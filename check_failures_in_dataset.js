var fs = require('fs');
var readline = require('readline');
var _ = require('lodash');
var processed = 1;
var goodAddressesCount = 0;
var wrongAddressesCount = 0;
var dir = './data/';
var outDir = './data/out/';
var commandOptions = [
  {name: "src", type: _.toString},
];
var options = require('command-line-args')(commandOptions);
var r = readline.createInterface({
  input: fs.createReadStream(dir + options.src, {encoding:'utf8'})
});

var outfilename = outDir +  options.src.slice(0, options.src.length - 4);
var time = Date.now();
var goodFilename = outfilename + '__good__' + time + '.txt';
var wrongFileName = outfilename + '__wrong__' +  time + '.txt';
var good = fs.createWriteStream(goodFilename);
var wrong = fs.createWriteStream(wrongFileName);

r.on('line', function(data) {
  console.log('lines processed: %d', processed++);
  try {
    validate(transform(data));
  }catch(e) {
    console.error(e);
  }
});

function transform(data) {
  return splitAndLabelAbbrevations(replaceNumberTag(upperCaseTags(data)));
}

function upperCaseTags(line) {
  return line.replace(/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)/gi, function(match) {return match.toUpperCase();});
}

function replaceNumberTag(line) {
  var checkRegExp = /(\s|^)(N|n|N°|n°|No|#|Nro|Numero|Number)(?=\|B_SN)/gi;

  if (checkRegExp.test(line)) {
    var match = /(\s|^)(N|n|N°|n°|No|#|Nro|Numero|Number)\|B_SN(\s+\S+)\|I_SN/g;
    line = line.replace(match, '$1$2|O$3|B_SN');
  }

  return line;
}

function splitAndLabelAbbrevations(line) {
  var words = line.split(" ");
  var newLine = words.map(function(word) {
    var newWord = word;
    if (/.+\.[^.\s]{3}[^.\s]*/.test(word)) {
      // ver que tipo de etiqueta tiene
      var tag = word.match(/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)/);
      var tmp = word.split(".");
      tag = tag ? tag[1] : null;

      if (tag && tmp.length > 0) {
        newWord = tmp[0]+'.|' + tag;

        if (tag[0] === 'B') {
          if (tmp.length > 1) {
            tmp.slice(1, tmp.length).forEach(function(w) {
              if (!/^\|/.test(w)) {
                newWord += ' ' + w.replace('|'+tag, '') + '|I' + tag.slice(1, tag.length);
              }
            });
          }

        }else if (tmp.length > 0) {
            if (tmp.length > 1) {
              tmp.slice(1, tmp.length).forEach(function(w) {
                if(!/^\|/.test(w)) {
                  newWord += ' ' + w.replace('|'+tag, '') + '|' + tag;
                }
              });
            }
        }
      }
    }

    return newWord;
  });
  return newLine.join(" ");
}

function validate(data) {
  // 1. check that all words have tags [X]
  // 2. check for invalid tags by format [X]
  // 3. check for nonexistent tags [X]
  // 4. check for incomplete NEs [X]

  if (allWordsHaveValidTags(data) && !isThereIncompleteTags(data) && !arePointsAfterTags(data)) {
    good.write(data + '\n');
    goodAddressesCount++;
  }else {
    wrong.write(data + '\n');
    wrongAddressesCount++;
  }

  printProgress(goodAddressesCount, wrongAddressesCount);
}

function allWordsHaveValidTags(line) {
  var words = line.split(' ');
  return !words.find(function(word) {
    return !/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)/g.test(word);
  });
}

function isThereIncompleteTags(line) {
  // I_<tag> without an B_<tag> before
  var invalidTagRegExp =  /\|((B_([SNCTDUROLZPFV]{1,3}))\s+\S*\|I_(?!\3)|O\s+\S*\|I_[SNCTDUROLZPFV]{1,3})/;
  return invalidTagRegExp.test(line);
}

function arePointsAfterTags(line) {
  return /\|([BI]_[SNCTDUROLZPFV]{1,3}|O)\./gi.test(line);
}

function printProgress(goodCount, wrongCount) {
  console.log('\n================================================');
  console.log('[GOOD]: %d       [BAD]: %d           total: %d', goodCount, wrongCount, goodCount + wrongCount);
}
