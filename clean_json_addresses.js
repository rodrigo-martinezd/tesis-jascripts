var fs = require('fs');
var _ = require('lodash');
var path = require('path');
var JSONStream  = require('JSONStream');
var Cleaner = require('./cleaner/cleaner').default;

var cleaner = new Cleaner('cl');
var processed = 0;
var dir = './data/';
var commandOptions = [
	{name: "src", type: _.toString},
	{name: "out", type: _.toString}
];
var options = require('command-line-args')(commandOptions);

var stream = fs.createReadStream(dir + options.src,{encoding:'utf8'});
var parser = JSONStream.parse('*');
var addressStream = JSONStream.stringify();
var outAddressStream = fs.createWriteStream(dir + options.out);

stream.pipe(parser);
addressStream.pipe(outAddressStream);

parser.on('data', function(data) {
	saveCleanAddress(data);
});

parser.on('end', function() {
	addressStream.end();
});

function saveCleanAddress(o) {
	cleaner.clean(o.address).then(function(cleanAddress) {
		o.clean_address = cleanAddress;
		addressStream.write(o);
		processed++;
		printAddresses(o.address, o.clean_address);
		printProgress();
	})
	.catch(function(originalAddress) {
		processed++;
		printProgress();
	});
}

function printAddresses(original, cleaned) {
	console.log("\noriginal: %s", original);
	console.log("cleaned: %s\n", cleaned);
}

function printProgress() {
	console.log('Addresses processed: %d', processed);
}
