import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import countries from 'i18n-iso-countries';

import defaultTransformations, {localConfigs} from './config';


class CleanerError extends Error{}
class CleanerInvalidParameter extends Error{}
class CleanerMissingValue extends Error{}

const RESOURCES_DIR = 'resources';
const STOPWORDS_FILENAME = 'stopwords.txt';
const AVAILABLE_COUNTRIES = ['cl'];

class Cleaner {
    constructor(countryCode, optionalPath=null){
        let cp;
        let cn;

        this.$promise = new Promise((resolve, reject) => {
            cp = resolve;
            cn = reject;
        });

        try {
            // Validate countryCode and optionalPath

            validateCountryCode(countryCode);
            validateOptionalPath(optionalPath);

            countryCode = countryCode.toLowerCase();

            // Check if config was previosly loaded

            if (this.constructor.configs.hasOwnProperty(countryCode) &&
            !_.isEmpty(this.constructor.configs[countryCode])) {

                _.merge(this, this.constructor.configs[countryCode]);

                // Set transformations
                this.overrideDefaultConfiguration = !!this.overrideDefaultConfiguration;

                if(!this.overrideDefaultConfiguration) {
                    this.transformations = _.clone(defaultTransformations);
                }else if (this.overrideDefaultConfiguration && _.isEmpty(this.transformations)) {
                    throw new CleanerMissingValue('transformations array is missing '+
                                                  'from local configuration');
                }

                // Set stopwords

                if (_.isEmpty(this.stopwords)) {
                    const basePath = optionalPath ? optionalPath : '.';
                    const filePath = `${basePath}/${countryCode}/${RESOURCES_DIR}/${STOPWORDS_FILENAME}`;
                    fs.readFile(path.join(__dirname, filePath), {encoding:'utf8'}, (err, content) => {
                        if(err) {
                            throw new CleanerError(err);
                        }

                        this.stopwords = content.toString().split('\n');
                        this.constructor.configs[countryCode].stopwords = _.clone(this.stopwords);
                        cp(this);
                    });
                }else {
                    cp(this);
                }

            }else {
                throw new CleanerError(`configuration module for country [${countryCode}] not found`);
            }
        }catch(e) {
            cn(e);
        }
    }

    clean(address){
        let newAddress = address;
        return this.$promise.then(() => {
            this.transformations.forEach(transformation => {
                newAddress = transformation(newAddress, this);
            });
            return newAddress;
        }).catch(() => {
            return newAddress;
        });
    }
}

Cleaner.globalTransformations = defaultTransformations;
Cleaner.configs = localConfigs || {};
Cleaner.errors = {
    'UNKNOWN': CleanerError,
    'INVALID_PARAMETER': CleanerInvalidParameter,
    'MISSING_VALUE': CleanerMissingValue
};

export default Cleaner;


///////////////////////////////////////////////////////
// Dynamic Validation
//////////////////////////////////////////////////////


function validateCountryCode(code) {
    if (code) {
        if (!(countries.getAlpha2Codes().hasOwnProperty(code.toUpperCase()) &&
        AVAILABLE_COUNTRIES.find(c => c === code.toLowerCase()))) {
            throw new CleanerInvalidParameter('Country code must be a valid ISO 3166-1 alpha 2');
        }
    }else {
        throw new CleanerMissingValue('Country code is required');
    }
}

function validateOptionalPath(optionalPath) {
    if(optionalPath && (typeof optionalPath !== 'string' || !/^(\.{1,2}\/|\/)?((\w+\/)+)?\w+(\.js)?$/.test(optionalPath))) {
        throw new CleanerInvalidParameter('optional path must be a relative or absolute system path');
    }
}

// TODO: VALIDATE TYPE OF ALL CONFIGS ATTRIBUTES
