/* jshint expr: true */
import chai from 'chai';
import sinon from 'sinon';
import chaiAsPromised from 'chai-as-promised';

import Cleaner, {__RewireAPI__ as CleanerRewireAPI} from './cleaner';

chai.use(chaiAsPromised);

let expect = chai.expect;
let sandbox = null;

chai.should();

describe('Cleaner', () => {
	let countries = {getAlpha2Codes(){}};
	let globalTransformations = [];

	beforeEach(() => {
		sandbox = sinon.createSandbox();
		sandbox.stub(countries, 'getAlpha2Codes');
		CleanerRewireAPI.__Rewire__('countries', countries);
		CleanerRewireAPI.__Rewire__('defaultTransformations', globalTransformations);
	});

	afterEach(() => {
		sandbox.restore();
		CleanerRewireAPI.__ResetDependency__('countries', countries);
		CleanerRewireAPI.__ResetDependency__('defaultTransformations', globalTransformations);
	});

	describe('when validate countryCode', () => {

		it('Should throw an error if countryCode is missing', (done) => {
			let cleaner = new Cleaner();
			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal('Country code is required');
			}).should.notify(done);
		});

		it('Should throw an error if countryCode isn\'t a valid ISO-3166 alpha 2', (done) => {
			countries.getAlpha2Codes.onFirstCall().returns({});

			let cleaner = new Cleaner('chile');

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal('Country code must be a valid ISO 3166-1 alpha 2');
			}).should.notify(done);
		});

		it('Should throw an error if countryCode isn\'t an available countryCode', (done) => {
			const AVAILABLE_COUNTRIES = ['br'];
			CleanerRewireAPI.__Rewire__('AVAILABLE_COUNTRIES', AVAILABLE_COUNTRIES);

			countries.getAlpha2Codes.onFirstCall().returns({'CL': 'cl'});

			let cleaner = new Cleaner('cl');

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal('Country code must be a valid ISO 3166-1 alpha 2');
				CleanerRewireAPI.__ResetDependency__('AVAILABLE_COUNTRIES', AVAILABLE_COUNTRIES);
			}).should.notify(done);
		});
	});

	describe('when validate optionalPath', () => {
		beforeEach(() => {
			countries.getAlpha2Codes.onFirstCall().returns({'CL':'cl'});
		});

		it('Should throw an error if optionalPath is defined and isn\'t a string', (done) => {

			let cleaner = new Cleaner('cl', 1234);

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal('optional path must be a relative or absolute system path');
			}).should.notify(done);
		});

		it('Should throw an error if optionalPath is defined and isn\'t a valid path', (done) => {

			let cleaner = new Cleaner('cl', '$&/$&%#&/#%/');

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal('optional path must be a relative or absolute system path');
			}).should.notify(done);
		});
	});


	describe('with valid arguments', () => {
		const validCountryCode = 'cl';
		let originalConfig = null;

		beforeEach(() => {
			countries.getAlpha2Codes.onFirstCall().returns({'CL':'cl'});
			originalConfig = Cleaner.configs;
		});

		afterEach(() => {
			Cleaner.configs = originalConfig;
		});

		it('Should throw an error if country submodule is missing', (done) => {

			Cleaner.configs = {};

			let cleaner = new Cleaner(validCountryCode);

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal(`configuration module for country [${validCountryCode}] not found`);
			}).should.notify(done);
		});

		it('Should set <instance> property as promise when finish instantiation', (done) => {
			Cleaner.configs = {};

			let cleaner = new Cleaner(validCountryCode);

			expect(cleaner.$promise).to.be.a('promise');
			cleaner.$promise.should.be.rejected.then(() => {
			}).should.notify(done);
		});

		it('Should merge local configuration if exist into the instance', (done) => {
			Cleaner.configs = {};
			Cleaner.configs[validCountryCode] = {
				countryName: 'Chile',
				overrideDefaultConfiguration: false,
				stopwords: ['ante'],
				keepCharacters: '=&/'
			};

			let cleaner = new Cleaner(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(() => {
				expect(cleaner).to.have.property('countryName', 'Chile');
				expect(cleaner).to.have.property('overrideDefaultConfiguration', false);
				expect(cleaner).to.have.property('stopwords');
				expect(cleaner.stopwords).to.be.an('array').that.includes('ante');
				expect(cleaner).to.have.property('keepCharacters','=&/');
			}).should.notify(done);

		});

		it('Should set the instance transformations property with a copy of global transformations by default', (done) => {
			let globalTransformations = ['global'];
			let localTransformations = ['local'];

			Cleaner.configs = {};
			Cleaner.configs[validCountryCode] = {
				overrideDefaultConfiguration: false,
				stopwords: ['test'],
				transformations: localTransformations
			};

			CleanerRewireAPI.__Rewire__('defaultTransformations', globalTransformations);

			let cleaner = new Cleaner(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(() => {
				expect(cleaner.transformations).to.be.an('array').and.to.have.lengthOf(1);
				expect(cleaner.transformations).to.not.include(localTransformations[0]);
				expect(cleaner.transformations).to.include(globalTransformations[0]);
				CleanerRewireAPI.__ResetDependency__('defaultTransformations', globalTransformations);
			}).should.notify(done);
		});

		it('Should set local transformations if option overrideDefaultConfiguration is true', (done) => {
			let globalTransformations = ['global'];
			let localTransformations = ['local'];

			Cleaner.configs = {};
			Cleaner.configs[validCountryCode] = {
				overrideDefaultConfiguration: true,
				stopwords: ['test'],
				transformations: localTransformations
			};

			CleanerRewireAPI.__Rewire__('defaultTransformations', globalTransformations);

			let cleaner = new Cleaner(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(() => {
				expect(cleaner.transformations).to.be.an('array').and.to.have.lengthOf(1);
				expect(cleaner.transformations).to.not.include(globalTransformations[0]);
				expect(cleaner.transformations).to.include(localTransformations[0]);
				CleanerRewireAPI.__ResetDependency__('defaultTransformations', globalTransformations);
			}).should.notify(done);
		});

		it('Should throw an error is overrideDefaultConfiguration is true and local transformations are empty', (done) => {
			Cleaner.configs = {};
			Cleaner.configs[validCountryCode] = {
				overrideDefaultConfiguration: true,
				stopwords: ['test'],
				transformations: []
			};

			let cleaner = new Cleaner(validCountryCode);

			cleaner.$promise.should.be.rejected.then((err) => {
				expect(err).to.be.an('error');
				expect(err.message).to.equal(
					'transformations array is missing from local configuration');
			}).should.notify(done);
		});

		describe('loading stopwords', () => {
			let fsMock = {readFile(){}};

			beforeEach(() => {
				sandbox.stub(fsMock, 'readFile');
				CleanerRewireAPI.__Rewire__('fs', fsMock);
			});

			afterEach(() => {
				CleanerRewireAPI.__ResetDependency__('fs', fsMock);
			});

			it('Should load stopwords from file using default path', (done) => {
				let content = {toString(){return 'ante\npara\npor';}};
				let expectedStopwords = ['ante', 'para', 'por'];
				let callback = (path, options, cb) => {
					cb(null, content);
				};

				let spy = sandbox.spy(callback);

				Cleaner.configs = {};
				Cleaner.configs[validCountryCode] = {
					overrideDefaultConfiguration: false,
				};

				fsMock.readFile.callsFake(spy);

				let cleaner = new Cleaner(validCountryCode);

				cleaner.$promise.should.be.fulfilled.then(()=> {
					expect(spy.calledWithMatch(`/${validCountryCode}/resources/stopwords.txt`,{encoding:'utf8'})).to.be.true;
					expect(cleaner.stopwords).to.be.an('array').and.to.have.lengthOf(3);
					expect(cleaner.stopwords).to.have.members(expectedStopwords);
					expect(Cleaner.configs[validCountryCode].stopwords).to.be.an('array').and.to.have.lengthOf(3);
					expect(Cleaner.configs[validCountryCode].stopwords).to.have.members(expectedStopwords);
				}).should.notify(done);
			});

			it('Should load stopwords from file using optionalPath when it\'s provided', (done) => {
				const optionalFolder = 'assets';
				const optionalPath = `./${optionalFolder}`;
				let content = {toString(){return 'ante\npara\npor';}};
				let expectedStopwords = ['ante', 'para', 'por'];
				let callback = (path, options, cb) => {
					cb(null, content);
				};

				let spy = sandbox.spy(callback);

				Cleaner.configs = {};
				Cleaner.configs[validCountryCode] = {
					overrideDefaultConfiguration: false,
				};

				fsMock.readFile.callsFake(spy);

				let cleaner = new Cleaner(validCountryCode, optionalPath);

				cleaner.$promise.should.be.fulfilled.then(()=> {
					expect(spy.calledWithMatch(`/${optionalFolder}/${validCountryCode}/resources/stopwords.txt`,{encoding:'utf8'})).to.be.true;
					expect(cleaner.stopwords).to.be.an('array').and.to.have.lengthOf(3);
					expect(cleaner.stopwords).to.have.members(expectedStopwords);
					expect(Cleaner.configs[validCountryCode].stopwords).to.be.an('array').and.to.have.lengthOf(3);
					expect(Cleaner.configs[validCountryCode].stopwords).to.have.members(expectedStopwords);
				}).should.notify(done);
			});

			it('Should reject instance if can\'t load stopwords', (done) => {
				let error = new Error('can\'t open file');
				let callback = (path, options, cb) => {
					cb(error, null);
				};

				Cleaner.configs = {};
				Cleaner.configs[validCountryCode] = {
					overrideDefaultConfiguration: false,
				};

				fsMock.readFile.callsFake(callback);

				let cleaner = new Cleaner(validCountryCode);

				cleaner.$promise.should.be.rejected.then((err)=> {
					expect(err).to.be.an('error');
					expect(err.message).to.include(error.message);
				}).should.notify(done);
			});
		});

	});

	describe('Clean method', () => {
		let cleaner;
		let originalConfig;

		beforeEach(() => {
			countries.getAlpha2Codes.returns({'CL':'cl'});
			originalConfig = Cleaner.configs;
			cleaner = new Cleaner('cl');

			Cleaner.configs = {
				'cl': {
					overrideDefaultConfiguration: false,
					stopwords: ['ante']
				}
			};
		});

		afterEach(() => {
			Cleaner.configs = originalConfig;
		});

		it('Should return a promise when the clean method is called', () => {
			let promise = cleaner.clean('Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba');

			expect(promise).to.be.a('promise');
		});

		it('Should start the cleaning process when $promise property are fulfilled', (done) => {
			let dummy = (a) => {return a;};
			let spy = sandbox.spy(dummy);
			let mockTransformations = [spy];
			let originalTransformations = cleaner.transformations;

			cleaner.transformations = mockTransformations;

			let promise = cleaner.clean('Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba');

			promise.should.be.fulfilled.then(() => {
				expect(spy.calledOnce).to.be.true;
				cleaner.transformations = originalTransformations;
			}).should.notify(done);
		});

		it('Should iterate over every transformation and pass the arguments address and instance', (done) => {
			let dummyA = (a) => {return a;};
			let dummyB = (b) => {return b;};
			let spyA = sandbox.spy(dummyA);
			let spyB = sandbox.spy(dummyB);
			let mockTransformations = [spyA, spyB];
			let address = 'Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba';
			let originalTransformations = cleaner.transformations;

			cleaner.transformations = mockTransformations;

			let promise = cleaner.clean(address);

			promise.should.be.fulfilled.then(() => {
				expect(spyA.calledOnce).to.be.true;
				expect(spyB.calledOnce).to.be.true;
				expect(spyA.calledWith(address, cleaner)).to.be.true;
				expect(spyB.calledWith(address, cleaner)).to.be.true;
				cleaner.transformations = originalTransformations;
			}).should.notify(done);
		});


		it('Should return a string if the promise returned are fulfilled', (done) => {

			let promise = cleaner.clean('address');

			promise.should.be.fulfilled.then((newAddress) => {
				expect(newAddress).to.be.an('string');
			}).should.notify(done);
		});

		it('Should return the original address if the promise returned are rejected', (done) => {
			let originalPromise = cleaner.$promise;
			cleaner.$promise = Promise.reject(new Error('invalid'));
			let promise = cleaner.clean('address');

			promise.should.be.fulfilled.then((originalAddress) => {
				expect(originalAddress).to.equal('address');
				cleaner.$promise = originalPromise;
			}).should.notify(done);
		});
	});
});
