/**
	reemplazo de abreviaturas (acronimos) // pais
	eliminacion de stopwords // pais

**/

export default  {
	countryCode: 'cl',
	allowOverrideConfiguration: false,
	countryName: "Chile",
	useSplitWords: true,
	separatorCharacter: ' ',
	alternativeSplittingCharacters: {
		rule: /([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])(\/|\\|\-|\_)+([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])/gi,
		replace: "$1 $3",
		exceptions: ['S/N', 's/n', 'n/a', 'N/A', 'S/n', 's/N', 'N/a', 'n/A']
	},
	validLetters: /[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]/i,
	validSingleLetters: /[a-zA-z]/,
	validSpecialCharacters: /(\'|\"|\*|\(|\)|\{|\}|\[|\]|\-|\\|\/|\.|\,|\:|\+|\°)/,
	specialCharactersReplacements: { '?': '', 'º': '°', 'ª': '°'},
	deleteOptions: {
		replace: true,
		replacementCharacter: ' ' // char
	},
	repeatOptions: {
		rule: /([^0-9\s])\1+/gi,
		excludeCharacters:  [
			{char: 'r', freq: 2},
			{char: 'l', freq: 2},
			{char: 'c', freq: 2},
			{char: 's', freq: 2},
			{char: 'g', freq: 2},
			{char: 'f', freq: 2},
			{char: 'n', freq: 2},
			{char: 'e', freq: 2},
			{char: 'm', freq: 2},
			{char: 't', freq: 2},
			{char: 'p', freq: 2}
		]
	},
	expressionsOptions: {
		rules: [
			/\+?(\(?(56(\-|\s)?)?\)?)?(\(?0?9\)?(\-|\s)?[0-9]{8}|\(?0?22\)?(\-|\s)?[0-9]{7})/gi, // phoneNumber
			/((\(|\[|\{)|(\"|\'|\*|\+|\%|\#)\3*).*((\)|\]|\})|(\"|\'|\*|\+|\%|\#)\6*)/gi, // all text between brackets or "'+*#%
			/\(.+\,/gi, // everything between a bracket and a comma
			/T?[012]?[0-9]\:[0-9]{2}(\:[0-9]{2})?(\.[0-9]{0,6})?(Z|\s?(\+|\-)?[012][0-9]\:[0-9]{2})?/gi, // time
			/(([012]?\d|3[01])(\/|\-)[01]?\d(\/|\-)([12]\d{3}|\d{2})|([12]\d{3}|\d{2})(\/|\-)[01]?\d(\/|\-)([012]?\d|3[01]))/gi, // date
			/[^0-9](\-|\+)?(90\.0{5,}|[0-8]?[0-9]\.[0-9]{5,})/g, //latitude
			/(\-|\+)?(180\.0{5,}|(1[0-7][0-9]|[0-9]{1,2})\.[0-9]{5,})/g // longitude
		]
	},
	cleaningRules: [cleanAsteriks, cleanPads, cleanTwoPoints, cleanSlashs,  cleanPoints, cleanHyphes, cleanCommas],
	algorithms: {
		split: splitWordsAlgorithm,
		tokenize: tokenizerAlgorithm,
		latinize: latinizeAlgorithm
	}
};

// CLEANING RULES

function cleanAsteriks(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			{regex: /(n|N)\*(?=(\s+)?\d+)/gi, replacement: 'N° '},
			{regex: /\*/gi, replacement: instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanPads(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// Agrega un espacio antes de #1234
			{regex: /(\#\d)/gi, replacement: instance.separatorCharacter + '$1'},
			// Reemplaza # al principio de una letra por un espacio
			{regex: /\#([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: instance.separatorCharacter + '$1'},
			// Reemplaza # al final de una letra por un espacio
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\#/gi, replacement: '$1' + instance.separatorCharacter},
			// Reemplaza # al final de un número por un espacio
			{regex: /(\d)\#/gi, replacement: '$1' + instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanTwoPoints(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// Eliminar d:d expresiones
			{regex: /\d+\:\d+/gi, replacement: instance.separatorCharacter},
			// Reemplaza A:21 por A-21
			{regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter + '$2-'},
			// Reemplaza 21:B por 21-B
			{regex: /(\s+|^)(\d+)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter+'$2-'},
			// Reemplaza A-:21 por A-21
			{regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]\-)\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter+'$2'},
			// Reemplaza 21-:B por 21-B
			{regex: /(\s+|^)(\d+\-)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter+'$2'},
			// Reemplaza palabra: por palabra_ para evitar que se borre :
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\:/g, replacement: '$1_'},
			// Reemplaza : por un espacio si no es la continuacion de una palabra
			{regex: /\:/gi, replacement: instance.separatorCharacter},
			// Reincorpora palabra_  como palabra:
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\_/g, replacement: '$1:' + instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			while(newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanSlashs(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// Reemplazar S/N con un guion en medio para que no sea eliminado
			{regex: /(\s+|^)(S|s)(\s{0,3})\/(\s{0,3})(N|n)/gi, replacement: instance.separatorCharacter + '$2_$5' +  instance.separatorCharacter},
			// Reemplazar N/A con un guin en medio para que no sea eliminado
			{regex: /(\s+|^)(N|n)(\s{0,3})\/(\s{0,3})(A|a)/gi, replacement: instance.separatorCharacter + '$2_$5' +  instance.separatorCharacter},
			// Remplazar d/d por d_d para que no sea eliminada
			{regex: /(\d)\/(\d)/gi, replacement: '$1_$2'},
			// Reemplazar todos los slash con espacios
			{regex: /(\\|\/)/gi, replacement: instance.separatorCharacter},
			// Restituir d_d a d/d
			{regex: /(\d)\_(\d)/g, replacement: '$1/$2' },
			// Restituir S_N a S/N
			{regex: /(\s+|^)(S|s)\_(N|n)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter},
			// Restituir N_A a N/A
			{regex: /(\s+|^)(N|n)\_(A|a)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter},
		];

		rules.forEach((rule) => {
			while(newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanPoints(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {

		let rules = [
			// numeros con multiples puntos ej: 11.1.1.1.1.1
			{regex: /\.*(\d+\.\d+|\d+\.){2,}\d+?\.*/gi, replacement: instance.separatorCharacter},
			// N.1000 -> N° 1000
			{regex: /(\s+|^)N\.(\d+)/g, replacement: instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2'},
			// reemplazar . por - si esta en medio de una letra y un numero ej: A.2 -> A-2
			{regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\.(?=\d+(\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-'},
			// reemplazar . por - si esta al entre un numero y una letra ej: 14.B -> 14-B
			{regex: /(\s+|^)(\d+)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-'},
			// agregar espacio a la derecha de . si . esta en medio de una palabra y un numero
			{regex: /([^\s\.\d]{2,}\.)(?=\d+)/g, replacement: '$1' + instance.separatorCharacter},
			// palabras con . al principio
			{regex: /(\s+|^)\.(?=[^\s\.\d]+)/g, replacement: instance.separatorCharacter},
			// numeros con . al principio
			{regex: /(\s+|^)\.(?=\d+)/g, replacement: instance.separatorCharacter},
			// unir numeros con coma si en medio a un punto
			{regex: /(\d+)\.\,/g, replacement: '$1,' + instance.separatorCharacter},
			// numeros con . al final
			{regex: /(\d+)\.(?=(\s+|(\s+)?$))/g, replacement: '$1' + instance.separatorCharacter},
			// separar av.|avda.|adva.|avnda.|adva.de palabras
			{regex: /(\s+|^)(av|avda|adva|avnda|adva)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)/gi, replacement: instance.separatorCharacter + '$2.' + instance.separatorCharacter},
			{regex: /([a-zA-Z]{2}[a-zA-Z]+)\.+([a-zA-Z]{2}[a-zA-Z]+)/gi,  replacement: '$1.' + instance.separatorCharacter + '$2'},
			{regex: /([a-zA-Z]+)\.+\,/gi, replacement: '$1.' + instance.separatorCharacter + ','},
			{regex: /([0-9]+)\.+([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü-]+)/gi, replacement: '$1' + instance.separatorCharacter + '$2'}
		];

		rules.forEach((rule) => {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanHyphes(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// L-10 -> L. 10
			{regex: /(\s+|^)L\-(?=\d+)/g, replacement: instance.separatorCharacter + 'L.' + instance.separatorCharacter},
			// unir letra con numero que tienen un guion en medio separado por espacios
			{regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ])\s{1,2}\-\s{1,2}(\d+)/g, replacement: instance.separatorCharacter + '$2-$3' + instance.separatorCharacter},
			// separar palabras de más de 1 caracter unidas por un - a un numero
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})\-(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter},
			// separar numeros unidos por un guion a palabras de más de 1 caracter
			{regex: /(\d+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})/gi, replacement: '$1' + instance.separatorCharacter},
			// separar palabras unidas por un -
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter},
			// reemplazar - al principio de una palabra o numero por un espacio
			{regex: /(\s+|^)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter},
			// reemplazar - al final de una palabra o numero por un espacio
			{regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=(\s+|(\s+)?$))/gi, replacement: '$1' + instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanCommas(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// remover espacio sobrante entre un caracter alfanumerico y una coma
			{regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\s+\,/g, replacement: '$1,' + instance.separatorCharacter},
			// reemplazar multiples comas separadas por espacios por solo una a la derecha del ultimo caracter alfanumerico
			{regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,?(\s+\,)+(\s+)?/g, replacement: '$1,' + instance.separatorCharacter},
			// agregar un espacio a la derecha de la coma cuando se encuentra entre un numero y una letra
			{regex: /(\d+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter},
			// agregar un espacio a la derecha de la coma cuando se encuentra entre una letra y un numero
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter},
			// agregar un espacio a la derecha de la coma cuando se encuentra entre 2 letras
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter},
			// eliminar coma al inicio de una palabra o numero
			{regex: /(\s+|^)\,(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter},
			// eliminar dos comas consecutivas al final de una palabra
			{regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,{2,}/gi, replacement: '$1,' + instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			while(newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}


// ALGORITHMS

function splitWordsAlgorithm(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length >  0) {
		let rules = [
			// separa N°1000 -> N° 1000
			{regex: /(.+|^)N\°(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2'},
			// separa KM68.5 -> KM 68.5
			{regex: /(.+|^)KM(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'KM' + instance.separatorCharacter + '$2'},
			// separa of-401
			{regex: /((of|OF|Of)\.?)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: '$1' + instance.separatorCharacter},
			// si una minuscula se encuentra con una mayuscula las separa
			{regex: /([a-záéíúóüàèìòùñ])(?=[A-ZÁÉÍÓÚÜÀÈÌÒÙÑ])/g, replacement: '$1' + instance.separatorCharacter},

			// si dos palabras de al menos 2 characteres se encuentran divididas por -, _, \ o / los separa
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})(\-|\_|\/|\\)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})/gi, replacement: '$1' + instance.separatorCharacter},

			// si una palabra se encuentra con un numero y es superior a 2 characteres los separa
			{regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})(\-|\_)?(?=[0-9])/gi, replacement: '$1' + instance.separatorCharacter},

			// si un numero se encuentra con una palabra superior a 2 caracteres los separa
			{regex: /([0-9])(\-|\_)?(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})/gi, replacement: '$1' + instance.separatorCharacter},

			// separar N°BCG y NH°123
			{regex: /(N.?\°)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü0-9])/gi, replacement: '$1' + instance.separatorCharacter}
		];

		rules.forEach((rule) => {
			while(newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});

	}

	return newAddress;
}


function tokenizerAlgorithm(address, instance) {
	let tokens = [];
	if (typeof address === "string" && address.length > 0) {
		let re = /[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/;
		tokens = address.split(instance.separatorCharacter);
		tokens = tokens.filter((token) =>  {
			return (token.length > 1 && re.test(token)) ||
			(token.length === 1 && (instance.validSingleLetters.test(token) || /^[0-9]$/.test(token)));
		});

		tokens = tokens.map((w,i,arr) => arr[i].trim());
	}
	return tokens;

}

function latinizeAlgorithm(token) {
	let tokenLatinized = token;
	if (typeof token === "string" && token.length > 0) {
		const isUpper = /[A-ZÁÉÍÓÚÀÈÌÒÙÜÑ]/;
		const replacementsMap = {
			'á': 'a',
			'é': 'e',
			'í': 'i',
			'ó': 'o',
			'ú': 'u',
			'à': 'a',
			'è': 'e',
			'ì': 'i',
			'ò': 'o',
			'ù': 'u',
			'ü': 'u'
		};

		let chars = tokenLatinized.split("");
		chars = chars.map(c => {
			let upper = isUpper.test(c);
			c = c.toLowerCase();
			c = replacementsMap[c] ? replacementsMap[c] : c;
			return upper ? c.toUpperCase() : c;
		});
		tokenLatinized = chars.join("");
	}
	return tokenLatinized;
}
