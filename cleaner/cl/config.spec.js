/* jshint expr: true */
import chai from 'chai';
import sinon from 'sinon';
import _ from 'lodash';

import config from './config';
import transformations from '../config';

let expect = chai.expect;
let sandbox = null;


describe('Cleaner - [CL] Configuration', () => {
	let instance = _.merge({}, config);
	let clean = (address, instance) => {
		transformations.forEach(transform => {
			address = transform(address, instance);
		});
		return address;
	};

	instance.stopwords = ['desde', 'hasta', 'por'];

	beforeEach(() => {
		sandbox = sinon.createSandbox();
	});

	afterEach(() => {
		sandbox.restore();
	});

	describe('deleting invalid characters', () => {
		let transform = transformations[0];

		it('Should replace letters distinct than a-zA-záéíóíÁÉÍÓÚñÑÜü with space', () => {
			expect(transform('áéíóúÁÉÍÓÚñÑÜü', instance)).to.equal('áéíóúÁÉÍÓÚñÑÜü');
			expect(transform('abcdefghijklmnñopqrstuvwxyz', instance)).to.equal('abcdefghijklmnñopqrstuvwxyz');
			expect(transform('ABCDEFGHIJKLMNÑOPQRSTUVWXYZ', instance)).to.equal('ABCDEFGHIJKLMNÑOPQRSTUVWXYZ');
			expect(transform('ÂÇăģěõ', instance)).to.equal('      ');
		});

		it('Should remove character ?', () => {
			expect(transform('calle pe?unias', instance)).to.equal('calle peunias');
		});

		it('Should replace special characters distinct than  \'"*(){}[]-\\/.,:+ª° with space', () => {
			expect(transform('#!&$~;', instance)).to.equal('      ');
			expect(transform('°ª\'"()[]{}-\\/.+:,', instance)).to.equal('°°\'"()[]{}-\\/.+:,');
		});

		it('Should replace character ª and º with °', () => {
			expect(transform('#!&$~;', instance)).to.equal('      ');
		});

		it('Should not remove or replace space character', () => {
			expect(transform('ªº', instance)).to.equal('°°');
		});
	});

	describe('deleting expressions', () => {
		let transform = transformations[1];

		it('Should remove phone numbers', () => {
			expect(transform('cel: 56949123580', instance)).to.equal('cel:  ');
			expect(transform('cel: +(56)(09)49123580', instance)).to.equal('cel:  ');
			expect(transform('cel: 9-49123580', instance)).to.equal('cel:  ');
			expect(transform('cel: 0228574894', instance)).to.equal('cel:  ');
			expect(transform('cel: (56)(22)8574894', instance)).to.equal('cel:  ');
			expect(transform('cel: 22-8574894', instance)).to.equal('cel:  ');
			expect(transform('cel: 56-22-8574894', instance)).to.equal('cel:  ');
		});

		it('Should remove everything surrounded with ()[]{}**""\'\'+', () => {
			expect(transform('asd(  asd   asd asdasd )asd', instance)).to.equal('asd asd');
			expect(transform('***fritz***', instance)).to.equal(' ');
			expect(transform('[extra info]', instance)).to.equal(' ');
			expect(transform('\'enfasis\'', instance)).to.equal(' ');
			expect(transform('"enfasis"', instance)).to.equal(' ');
			expect(transform('{asda}', instance)).to.equal(' ');
			expect(transform('+asd+', instance)).to.equal(' ');
		});

		it('Should remove everything between a ( and a ,', () => {
			expect(transform('  (asd asd asd asd,', instance)).to.equal('   ');
			expect(transform('  ( asd asd asd asd, ', instance)).to.equal('    ');
			expect(transform('(asd asd asd asd , ', instance)).to.equal('  ');
		});

		it('Should remove hours', () => {
			expect(transform('10:00', instance)).to.equal(' ');
			expect(transform('9:00', instance)).to.equal(' ');
			expect(transform('10:00:00', instance)).to.equal(' ');
			expect(transform('10:00:00.123123', instance)).to.equal(' ');
			expect(transform('10:00:00.123456Z', instance)).to.equal(' ');
			expect(transform('10:00:00.123456 +04:00', instance)).to.equal(' ');
			expect(transform('T10:00:00.123456 +04:00', instance)).to.equal(' ');
		});

		it('Should remove dates', () => {
			expect(transform('14/05/1992', instance)).to.equal(' ');
			expect(transform('14/05/92', instance)).to.equal(' ');
			expect(transform('14-05-1992', instance)).to.equal(' ');
			expect(transform('14-05-92', instance)).to.equal(' ');
			expect(transform('1992-05-14', instance)).to.equal(' ');
			expect(transform('1992/05/14', instance)).to.equal(' ');
			expect(transform('92-05-14', instance)).to.equal(' ');
			expect(transform('92/05/14', instance)).to.equal(' ');
		});

		it('Should remove latitude', () => {
			expect(transform('-90.000000', instance)).to.equal(' ');
			expect(transform('90.000000', instance)).to.equal(' ');
			expect(transform('+64.1234115', instance)).to.equal(' ');
		});

		it('Should remove longitude', () => {
			expect(transform('-180.000000', instance)).to.equal(' ');
			expect(transform('180.000000', instance)).to.equal(' ');
			expect(transform('90.1234115', instance)).to.equal(' ');
		});
	});


	describe('apply cleaning rules on', () => {
		let transform = transformations[2];

		describe('point character', () => {

			it('Should replace N.[0-9] for N° [0-9]', () => {
				expect(transform('N.1200', instance)).to.equal(' N° 1200');
				expect(transform(' N.1200 ', instance)).to.equal(' N° 1200 ');
			});

			it('Should replace point character with hyphen when point is between a number and a letter', () => {
				expect(transform('local 24.a', instance)).to.equal('local 24-a');
			});

			it('Should split words join by a point if subwords has length more than 2', () => {
				expect(transform('vic.mackenna', instance)).to.equal('vic. mackenna');
				expect(transform('rr.hh', instance)).to.equal('rr.hh');
			});

			it('Should replace point with hyphen when are between a letter and a number', () => {
				expect(transform('depto C.2', instance)).to.equal('depto C-2');
				expect(transform('depto A.301', instance)).to.equal('depto A-301');
			});

			it('Should add an space to the right of point when point is between a word and a number', () => {
				expect(transform('km.24', instance)).to.equal('km. 24');
			});

			it('Should replace point at begining with an space', () => {
				expect(transform('.asdasd', instance)).to.equal(' asdasd');
				expect(transform('.asdasd.', instance)).to.equal(' asdasd.');
				expect(transform(' .123', instance)).to.equal(' 123');
			});

			it('Should replace point with space when point is at the end of a number', () => {
				expect(transform('1400.', instance)).to.equal('1400 ');
			});

			it('Should remove point if the point is at the end of the number and later came a comma', () => {
				expect(transform('340.,', instance)).to.equal('340, ');
				expect(transform(' 340., ', instance)).to.equal(' 340,  ');
			});

			it('Should delete numbers with more than one point between', () => {
				expect(transform('11.11.1.1.1.1.1.', instance)).to.equal(' ');
				expect(transform(' ....11.11.1.1.1.1.1. ', instance)).to.equal('   ');
				expect(transform('11111111.1111', instance)).to.equal('11111111.1111');
			});

			it('Should split point from comma', () => {
				expect(transform('jose p.p.,', instance)).to.equal('jose p.p. ,');
			});

			it('Should split word from number if it\'s join by a point', () => {
				expect(transform('asd.123', instance)).to.equal('asd. 123');
				expect(transform('123..asd', instance)).to.equal('123 asd');
			});

      it('Should replace point at the end of a number if a word does not follow with space', () => {
        expect(transform('123.-', instance)).to.equal('123 -');
        expect(transform('123. a', instance)).to.equal('123  a');
      });

			it('Should split av|avnda|avda|adva if its join to a word by a point', () => {
				expect(transform('av.bla', instance)).to.equal(' av. bla');
				expect(transform('avnda.bla', instance)).to.equal(' avnda. bla');
				expect(transform('avda.bla', instance)).to.equal(' avda. bla');
				expect(transform('adva.bla', instance)).to.equal(' adva. bla');
				expect(transform('AV.bla', instance)).to.equal(' AV. bla');
				expect(transform('AVNDA.bla', instance)).to.equal(' AVNDA. bla');
				expect(transform('AVDA.bla', instance)).to.equal(' AVDA. bla');
				expect(transform('ADVA.bla', instance)).to.equal(' ADVA. bla');
				expect(transform(' AV.bla', instance)).to.equal(' AV. bla');
				expect(transform(' AVNDA.bla', instance)).to.equal(' AVNDA. bla');
				expect(transform(' AVDA.bla', instance)).to.equal(' AVDA. bla');
				expect(transform(' ADVA.bla', instance)).to.equal(' ADVA. bla');
			});
		});

		describe('two points character', () => {
			let transform = transformations[2];

			it('Should remove expresions like d:d', () => {
				expect(transform('13:124', instance)).to.equal(' ');
			});

			it('Should replace : for - when join a letter with a number', () => {
				expect(transform('a:124', instance)).to.equal(' a-124');
				expect(transform('a-:124', instance)).to.equal(' a-124');
				expect(transform('B:2', instance)).to.equal(' B-2');
			});

			it('Should replace : for - when join a number with a letter', () => {
				expect(transform('124-:D', instance)).to.equal(' 124-D');
				expect(transform('124:A', instance)).to.equal(' 124-A');
				expect(transform('2:C', instance)).to.equal(' 2-C');
			});

			it('Should replace all : characters for space except those at the end of a word', () => {
				expect(transform(':124', instance)).to.equal(' 124');
				expect(transform('cel: 124', instance)).to.equal('cel:  124');
				expect(transform('loc:12:of:33', instance)).to.equal('loc: 12 of: 33');
			});
		});

		describe('comma character', () => {
			let transform = transformations[2];

			it('Should add an space to the right between a number and a word', () => {
				expect(transform('1450,la florida', instance)).to.equal('1450, la florida');
			});

			it('Should remove comma between a word and a number', () => {
				expect(transform('depto,401', instance)).to.equal('depto 401');
			});

			it('Should remove excesive space between a character and a comma', () => {
				expect(transform('1450    ,la florida', instance)).to.equal('1450, la florida');
			});

			it('Should remove rest comma characters between spaces', () => {
				expect(transform('1450    ,        ,     la florida', instance)).to.equal('1450, la florida');
			});

			it('Should separate words join by a comma adding an space to the right of comma', () => {
				expect(transform('vic.mackenna,la florida', instance)).to.equal('vic. mackenna, la florida');
			});

			it('Should replace comma at the begining of an alphanumeric character with a space', () => {
				expect(transform(',aaa', instance)).to.equal(' aaa');
				expect(transform(',1000', instance)).to.equal(' 1000');
			});

			it('Should remove two commas consecutive', () => {
				expect(transform('aaa,,', instance)).to.equal('aaa, ');
			});

			it('')
		});

		describe('hyphen character', () => {
			let transform = transformations[2];

			it('Should replace replace hyphen between an L and a number for a point and a space to the right', () => {
				expect(transform('L-10', instance)).to.equal(' L. 10');
				expect(transform(' L-10 ', instance)).to.equal(' L. 10 ');
			});

			it('Should join a letter and a number with an hyphen in between separated by 1 or 2 spaces', () => {
				expect(transform('A - 1', instance)).to.equal(' A-1 ');
				expect(transform(' A  -  1 ', instance)).to.equal(' A-1  ');
				expect(transform('A -  1', instance)).to.equal(' A-1 ');
				expect(transform('A  - 1', instance)).to.equal(' A-1 ');
			});

			it('Should remove an hyphen in the begining of a word or number', () => {
				expect(transform(' -1450', instance)).to.equal(' 1450');
				expect(transform('-1450', instance)).to.equal(' 1450');
				expect(transform(' -oficina', instance)).to.equal(' oficina');
				expect(transform('-oficina', instance)).to.equal(' oficina');
			});

			it('Should remove an hyphen in the end of a word or number', () => {
				expect(transform('1450-', instance)).to.equal('1450 ');
				expect(transform('1450- ', instance)).to.equal('1450  ');
				expect(transform('oficina- ', instance)).to.equal('oficina  ');
				expect(transform('oficina-', instance)).to.equal('oficina ');
			});

			it('Should replace an hyphen between words with a space', () => {
				expect(transform('los-retamos', instance)).to.equal('los retamos');
				expect(transform(' los-retamos ', instance)).to.equal(' los retamos ');
			});

			it('Should replace an hyphen between a word of size at least of 2 and a number with a space', () => {
				expect(transform('of-86', instance)).to.equal('of 86');
				expect(transform(' calle-86 ', instance)).to.equal(' calle 86 ');
				expect(transform('local a-20', instance)).to.equal('local a-20');
				expect(transform(' local a-20 ', instance)).to.equal(' local a-20 ');
			});

			it('Should replace an hyphen between a number and a word of size at least of 2 with a space', () => {
				expect(transform('1088-ñuñoa', instance)).to.equal('1088 ñuñoa');
				expect(transform(' 1088-ñuñoa ', instance)).to.equal(' 1088 ñuñoa ');
				expect(transform('km-68', instance)).to.equal('km 68');
				expect(transform('of 42-a', instance)).to.equal('of 42-a');
				expect(transform(' of 42-a ', instance)).to.equal(' of 42-a ');
			});
		});

		describe('slash and backslash characters', () => {
			let transform = transformations[2];

			it('Should replace all slash and backslash characters for space', () => {
				expect(transform('loc / 13 / of /c4', instance)).to.equal('loc   13   of  c4');
				expect(transform('value/nombre', instance)).to.equal('value nombre');
				expect(transform('\\13\\feb\\33', instance)).to.equal(' 13 feb 33');
			});

			it('Should let fractions intact ([0-9])/[0-9])', () => {
				expect(transform('1/4', instance)).to.equal('1/4');
				expect(transform('3\\5', instance)).to.equal('3 5');
			});

			it('Should not split S/N or N/A and any combination of upper and lower forms', () => {
				expect(transform('S/N', instance)).to.equal(' S/N  ');
				expect(transform('s/N', instance)).to.equal(' s/N  ');
				expect(transform('S/n', instance)).to.equal(' S/n  ');
				expect(transform('s/n', instance)).to.equal(' s/n  ');
				expect(transform('N/A', instance)).to.equal(' N/A  ');
				expect(transform('N/a', instance)).to.equal(' N/a  ');
				expect(transform('n/A', instance)).to.equal(' n/A  ');
				expect(transform('n/a', instance)).to.equal(' n/a  ');
				expect(transform(' S   /  N ', instance)).to.equal(' S/N   ');
				expect(transform(' s   / N ', instance)).to.equal(' s/N   ');
				expect(transform(' S   /   n ', instance)).to.equal(' S/n   ');
				expect(transform(' s  /  n ', instance)).to.equal(' s/n   ');
				expect(transform(' N / A ', instance)).to.equal(' N/A   ');
				expect(transform(' N / a ', instance)).to.equal(' N/a   ');
				expect(transform(' n / A ', instance)).to.equal(' n/A   ');
				expect(transform(' n   /   a ', instance)).to.equal(' n/a   ');
				expect(transform(' S  /  N ', instance)).to.equal(' S/N   ');
				expect(transform(' s  /   N ', instance)).to.equal(' s/N   ');
				expect(transform(' S  / n ', instance)).to.equal(' S/n   ');
				expect(transform(' s /   n ', instance)).to.equal(' s/n   ');
				expect(transform(' N   / A ', instance)).to.equal(' N/A   ');
				expect(transform(' N /  a ', instance)).to.equal(' N/a   ');
				expect(transform(' n  /   A ', instance)).to.equal(' n/A   ');
				expect(transform(' n / a ', instance)).to.equal(' n/a   ');

			});
		});

		describe('asterik character', () => {
			let transform = transformations[2];

			it('Should replace n* or N* with N°', () => {
				expect(transform('n*', instance)).to.equal('n ');
				expect(transform('N*', instance)).to.equal('N ');
				expect(transform('N* 1410', instance)).to.equal('N°  1410');
				expect(transform('n*4567', instance)).to.equal('N° 4567');
			});

			it('Should replace all remaining * characters with space', () => {
				expect(transform('loc ** 13*of*c4 ***', instance)).to.equal('loc    13 of c4    ');
				expect(transform('value*nombre', instance)).to.equal('value nombre');
				expect(transform('*urgente', instance)).to.equal(' urgente');
			});
		});

		describe('pad character', () => {
			let transform = transformations[2];

			it('Should add an space before #d', () => {
				expect(transform('number#45', instance)).to.equal('number #45');
			});

			it('Should replace all remaining # characters with space', () => {
				expect(transform('A#', instance)).to.equal('A ');
				expect(transform('#A', instance)).to.equal(' A');
				expect(transform('1234#', instance)).to.equal('1234 ');
			});
		});
	});


	describe('alternative splitting characters', () => {
		let transform = transformations[3];

		it('Should replace \\ for spaces', () => {
			expect(transform('av\\josé\\miguel\\carrera', instance)).to.equal('av josé miguel carrera');
		});

		it('Should replace / for spaces', () => {
			expect(transform('calle/providencia', instance)).to.equal('calle providencia');
		});

		it('Should replace - for spaces', () => {
			expect(transform('psje-los-condores', instance)).to.equal('psje los condores');
		});

		it('Should replace _ for spaces', () => {
			expect(transform('of_B_las_condes', instance)).to.equal('of B las condes');
		});

		it('Should not split S/N or N/A and any combination of upper and lower forms', () => {
			expect(transform('S/N', instance)).to.equal('S/N');
			expect(transform('s/N', instance)).to.equal('s/N');
			expect(transform('S/n', instance)).to.equal('S/n');
			expect(transform('s/n', instance)).to.equal('s/n');
			expect(transform('N/A', instance)).to.equal('N/A');
			expect(transform('N/a', instance)).to.equal('N/a');
			expect(transform('n/A', instance)).to.equal('n/A');
			expect(transform('n/a', instance)).to.equal('n/a');
			expect(transform(' S/N ', instance)).to.equal(' S/N ');
			expect(transform(' s/N ', instance)).to.equal(' s/N ');
			expect(transform(' S/n ', instance)).to.equal(' S/n ');
			expect(transform(' s/n ', instance)).to.equal(' s/n ');
			expect(transform(' N/A ', instance)).to.equal(' N/A ');
			expect(transform(' N/a ', instance)).to.equal(' N/a ');
			expect(transform(' n/A ', instance)).to.equal(' n/A ');
			expect(transform(' n/a ', instance)).to.equal(' n/a ');

		});
	});

	describe('alternative splitting algorithm', () => {
		let transform = transformations[4];

		it('Should split (of|OF|Of)-[\w\d]', () => {
			expect(transform('of-401', instance)).to.equal('of 401');
			expect(transform(' OF-401 ', instance)).to.equal(' OF 401 ');
			expect(transform('Of.-401', instance)).to.equal('Of. 401');
		});

		it('Should split if a lowercase is continued by a uppercase letter', () => {
			expect(transform('granAvenida', instance)).to.equal('gran Avenida');
		});

		it('Should split if there an (-|_|\\|/) between 2 words of size 2 or greater', () => {
			expect(transform('lo-herrera', instance)).to.equal('lo herrera');
			expect(transform('lo_herrera', instance)).to.equal('lo herrera');
			expect(transform('lo\\herrera', instance)).to.equal('lo herrera');
			expect(transform('lo/herrera', instance)).to.equal('lo herrera');
			expect(transform('a-b', instance)).to.equal('a-b');
		});

		it('Should split if word at least of size 3 is continued by a number', () => {
			expect(transform('cel401', instance)).to.equal('cel 401');
			expect(transform('cel-4011010', instance)).to.equal('cel 4011010');
			expect(transform('a-2', instance)).to.equal('a-2');
		});

		it('Should split if a number is continued by a word of size greather or equal than 3', () => {
			expect(transform('1400loprado', instance)).to.equal('1400 loprado');
			expect(transform('1400_loprado', instance)).to.equal('1400 loprado');
			expect(transform('13b', instance)).to.equal('13b');
		});

		it('Should split N°[0-9] using a space between', () => {
			expect(transform('N°1000', instance)).to.equal(' N° 1000');
			expect(transform(' N°1000 ', instance)).to.equal('  N° 1000 ');
			expect(transform('ArturoN°1000 ', instance)).to.equal('Arturo N° 1000 ');
		});

		it('Should split KM[0-9] using a space between', () => {
			expect(transform('KM68.5', instance)).to.equal(' KM 68.5');
			expect(transform(' KM68.5 ', instance)).to.equal('  KM 68.5 ');
			expect(transform('asdKM12', instance)).to.equal('asd KM 12');
		});

		it('Should split N(.)° de numeros y letras', () => {
			expect(transform('NH°123', instance)).to.equal('NH° 123');
			expect(transform('N°BCG', instance)).to.equal('N° BCG');
		});

	});

	describe('remove duplicate characters', () => {
		let transform = transformations[5];

		it('Should compress a word of size equal or greater than 5 if has only one letter', () => {
			expect(transform('AAAAA', instance)).to.equal('A');
		});

		it('Should not compress a word of size less than 5 if has only one letter', () => {
			expect(transform('ss', instance)).to.equal('ss');
		});

		it('Should remove words with only duplicated special characters', () => {
			expect(transform('____', instance)).to.equal('');
			expect(transform('++++', instance)).to.equal('');
			expect(transform('????', instance)).to.equal('');
			expect(transform('------', instance)).to.equal('');
			expect(transform('/////', instance)).to.equal('');
			expect(transform('******', instance)).to.equal('');
		});

		it('Should compress duplicated characters on words', () => {
			expect(transform('local---2', instance)).to.equal('local-2');
			expect(transform('S//N', instance)).to.equal('S/N');
			expect(transform('vic..mackenna', instance)).to.equal('vic.mackenna');
			expect(transform('##123123', instance)).to.equal('#123123');
		});

		it('Should not remove acronyms', () => {
			expect(transform('FFAA', instance)).to.equal('FFAA');
			expect(transform('RRHH', instance)).to.equal('RRHH');
			expect(transform('CCRRTT', instance)).to.equal('CCRRTT');
		});

		it('Should not remove duplicate numbers', () => {
			expect(transform('222222', instance)).to.equal('222222');
		});

		it('Should remove duplicate spaces', () => {
			expect(transform('     calle', instance)).to.equal('calle');
			expect(transform('    calle    paul harris   ', instance)).to.equal('calle paul harris');
		});

		it('Should not modify string if don\'t have repeated characters', () => {
			expect(transform('    av    grecia   ', instance)).to.equal('    av    grecia   ');
		});

		it('Should limit letter r only to rr', () => {
			expect(transform('rrrrrrrrrotonda', instance)).to.equal('rrotonda');
		});

		it('Should limit letter l only to ll', () => {
			expect(transform('lllllllanquihue', instance)).to.equal('llanquihue');
		});

		it('Should limit letter s only to ss', () => {
			expect(transform('alesssandri', instance)).to.equal('alessandri');
		});

		it('Should limit letter c only to cc', () => {
			expect(transform('roccco', instance)).to.equal('rocco');
		});

		it('Should limit letter g only to gg', () => {
			expect(transform('ohiggggins', instance)).to.equal('ohiggins');
		});

		it('Should limit letter f only to ff', () => {
			expect(transform('beauchefff', instance)).to.equal('beaucheff');
		});

		it('Should limit letter n only to nn', () => {
			expect(transform('mackennna', instance)).to.equal('mackenna');
		});

		it('Should all replacements work on uppercase letters R,L,S,C,G,F', () => {
			expect(transform('BEAUCHEFFF', instance)).to.equal('BEAUCHEFF');
			expect(transform('OHIGGGINS', instance)).to.equal('OHIGGINS');
			expect(transform('ROCCCCCO', instance)).to.equal('ROCCO');
			expect(transform('ALESSSANDRI', instance)).to.equal('ALESSANDRI');
			expect(transform('LLLANQUIHUE', instance)).to.equal('LLANQUIHUE');
			expect(transform('RRRROTONDA', instance)).to.equal('RROTONDA');
			expect(transform('MACKENNNA', instance)).to.equal('MACKENNA');
		});
	});


	it('Should remove remaining special characters', () => {
		let transform = transformations[7];

		expect(transform('av. americo vespucio , 481 . . san - joaquin chile', instance)).to.equal('av. americo vespucio 481 san joaquin chile');
		expect(transform('av. quilin ## 1320 g a', instance)).to.equal('av. quilin 1320 g a');
		expect(transform(' aasd - -- asda $ #1123', instance)).to.equal('aasd asda #1123');
		expect(transform('ROMULO PE A 1321 (ESTADIO CANADELA ARICA', instance)).to.equal('ROMULO PE A 1321 ESTADIO CANADELA ARICA');
    expect(transform('ROMULO PE A 1321 ESTADIO CANADELA ARICA) )', instance)).to.equal('ROMULO PE A 1321 ESTADIO CANADELA ARICA');
	});

	it('Should not remove join characters', () => {
		let transform = transformations[7];

		expect(transform('av. santa maria 1200 L 2B', instance)).to.equal('av. santa maria 1200 L 2B');
		expect(transform('av. santa maria 1200 E 1B', instance)).to.equal('av. santa maria 1200 E 1B');
		expect(transform('Psj 1 puesto 3 y 5', instance)).to.equal('Psj 1 puesto 3 y 5');
		expect(transform('A salazar 15', instance)).to.equal('A salazar 15');
	});

	it('Should remove stopwords', () => {
		let transform = transformations[8];

		expect(transform('calle flores 1234, santiago, RM, Chile, entregar por psj rolando desde 10:00 hasta 13:00', instance)).to.equal('calle flores 1234, santiago, RM, Chile, entregar psj rolando 10:00 13:00');
	});

	it('Should remove excesive space characters between words (trim)', () => {
		let transform = transformations[6];
		expect(transform('     av vicuña        mackenna      3939     san    joaquín   chile', instance)).to.equal('av vicuña mackenna 3939 san joaquín chile');
	});

	describe('tokenizer algorithm', () => {
		it('Should filter words made only of special characters', () => {
			expect(instance.algorithms.tokenize('#$"# por la calle', instance)).to.have.members(['por', 'la', 'calle']);
		});

		it('Should filter words of size 1 if they aren\'t include on instance.validSingleLetters', () => {
			// validJoinCharacters aeouy
			expect(instance.algorithms.tokenize('calle t Ü garcía y ortega s á', instance)).to.have.members(['calle', 't', 'garcía', 'y', 'ortega', 's']);
		});

		it('Should let pass tokens of size 1 if they are numbers', () => {
			expect(instance.algorithms.tokenize('psj 2', instance)).to.have.members(['psj', '2']);
		});
	});

	describe('latinize algorithm', () => {
		it('Should remove accent on letters', () => {
			expect(instance.algorithms.latinize('ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ')).to.equal('AEIOUAEIOUUÑaeiouaeiouuñ');
		});
	});


	it('Should not remove N° expression after all transformations', () => {


		expect(clean('Luis Pasteur n° 6495 despto. 2 6495 Vitacura, Chile', instance)).to.equal('Luis Pasteur n° 6495 despto. 2 6495 Vitacura, Chile');
		expect(clean('CARDENAL CARO N° 021 MELIPILA, Chile', instance)).to.equal('CARDENAL CARO N° 021 MELIPILA, Chile');
		expect(clean('AV. CIRCUNVALACION Nº 0467, PUENTE ALTO', instance)).to.equal('AV. CIRCUNVALACION N° 0467, PUENTE ALTO');

	});

	it('Should not split S/N expression and any combination of upper and lower s or n', () => {
		expect(clean('EDUARDO RAGGIO S/N LOCAL 7\' \'CATEMU',instance)).to.equal('EDUARDO RAGGIO S/N LOCAL 7 CATEMU');
		expect(clean('GERMAN RIESCO S/N\' \'CHANGO',instance)).to.equal('GERMAN RIESCO S/N CHANGO');
		expect(clean('IGNACIO VALDEZ S/N CUNACO\' \'NANCAGUA',instance)).to.equal('IGNACIO VALDEZ S/N CUNACO NANCAGUA');
		expect(clean('RASTROJOS PARADERO 6 S/N, SAN VICENTE, SAGBARA', instance)).to.equal('RASTROJOS PARADERO 6 S/N, SAN VICENTE, SAGBARA');
	});
});
