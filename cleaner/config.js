import chileanConfig from './cl/config';

/**
    Reglas generales del cleaner


	[ X ] eliminacion de caracteres inválidos

	[ X ] eliminacion de expresiones (todo lo que este en llaves o destacado)

	[ X ] limpieza de palabras (aplicar reglas)

	[ X ] separar por otros caracteres alternativos reconocidos como separadores

	[ X ] dividir palabras enlazadas por algoritmo alternativo

	[ X ] eliminar characteres duplicados

	[  ] agregar pais si no lo tiene

	[ X ] eliminar multiples characteres separadores (TRIM)

	[ X ] eliminar caracteres especiales restantes

	[ X ] eliminar stopwords

**/

export default [
	deleteInvalidCharacters,
	deleteExpressions,
	applyCleaningRules,
	alternativeSplittingCharacters,
	alternativeSplitAlgorithm,
	deleteDuplicateCharacters,
	trim,
	deleteRemainingSpecialCharacters,
	removeStopWords
];


export let localConfigs = {
	'cl': chileanConfig
};


function deleteInvalidCharacters(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let replaceCharacter = !!instance.deleteOptions.delete ? '' :
		(!!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ');

		let characters = address.split("");
		newAddress = characters.map((c) => {
			if (!instance.validLetters.test(c) && !instance.validSpecialCharacters.test(c) &&
			!/[0-9]/.test(c) && c !== instance.separatorCharacter){
				return instance.specialCharactersReplacements.hasOwnProperty(c) ? instance.specialCharactersReplacements[c] :
				(replaceCharacter || " ");
			}else {
				return c;
			}
		});
		newAddress = newAddress.join("");
	}
	return newAddress;
}


function deleteExpressions(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let replaceCharacter = !!instance.deleteOptions.delete ? '' :
		(!!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ');

		instance.expressionsOptions.rules.forEach((rule) => {
			newAddress = newAddress.replace(rule, replaceCharacter);
		});
	}

	return newAddress;
}


function applyCleaningRules(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		instance.cleaningRules.forEach((transform) => {
			newAddress = transform(newAddress, instance);
		});
	}
	return newAddress;
}


function alternativeSplittingCharacters(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);

		let filterMatchs = (match) => {
			return !instance.alternativeSplittingCharacters.exceptions.find(w => w === match);
		};

		matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;

		while(matchs && matchs.length > 0) {
			newAddress = newAddress.replace(instance.alternativeSplittingCharacters.rule, instance.alternativeSplittingCharacters.replace);
			matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);
			matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;
		}
	}

	return newAddress;
}

function alternativeSplitAlgorithm(address, instance) {
	let newAddress = instance.useSplitWords ? instance.algorithms.split(address, instance) : address;
	return newAddress;
}

function escapeForRegex(str) {
	let escapingList = '+_~*=?¿\\/%$#!|°¬()[]{}^;:.,<>';
	str = str.split('');
	str = str.map(char => {
		return escapingList.includes(char) ? '\\'+ char : char;
	});

	return str.join('');
}

function deleteDuplicateCharacters(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0 && newAddress.match(instance.repeatOptions.rule)) {
		const special = /[^a-zA-Z0-9ÁÉÍÓÚÜÑÀÈÌÒÙáéíóúüñàèìòù\s]/i;
		let tokens = instance.algorithms.tokenize(newAddress, instance);
		let newTokens = tokens.map(token => {
			let newToken = token;
			let matchs = token.match(instance.repeatOptions.rule);
			let totalMatchs = Array.isArray(matchs) ? matchs.reduce((prev,curr) => prev + curr.length, 0) : 0;
			let containSpecialCharacters = Array.isArray(matchs) ?
			matchs.reduce((prev, curr) => !prev ? special.test(curr) : prev, false) : false;

			if ( containSpecialCharacters || (matchs && matchs.length === 1 && totalMatchs === token.length && token.length >= 5) ||
			(totalMatchs > 0 && totalMatchs < token.length && token.length >= 5)) {
				matchs.forEach(match => {
					let exclude = match[0] === instance.separatorCharacter;
					let freq = 1;
					let replacement = "";

					if (!exclude) {
						for(let i=0; i < instance.repeatOptions.excludeCharacters.length; i++) {
							let o = instance.repeatOptions.excludeCharacters[i];
							if (match[0].toLowerCase() === o.char) {
								freq = o.freq;
								break;
							}
						}

						for(let i = 0; i < freq; i++) {
							replacement += match[0];
						}
						let regex = new RegExp(escapeForRegex(match), 'gi');
						newToken = newToken.replace(regex, replacement);
					}

				});
			}

			return newToken;
		});

		newAddress = newTokens.join(instance.separatorCharacter);
	}

	return newAddress;
}

/*function addCountryName(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		const isNumber = /^\d+$/;
		const haveCountry = new RegExp(instance.countryName, "gi");
		let tokens = newAddress
			.split(instance.separatorCharacter)
			.filter(w => w !== instance.separatorCharacter);

		let containOnlyNumbers = tokens
			.reduce((prev,curr,i, arr) => prev ? isNumber.test(arr[i]) : prev, true );

		if (!haveCountry.test(newAddress) && !containOnlyNumbers) {
			newAddress = newAddress.replace(/(\S)\s*\,?$/gi, '$1, ' + instance.countryName);
		}
	}
	return newAddress;
}*/

function trim(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let words = instance.algorithms.tokenize(address, instance);
		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function removeStopWords(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let words = instance.algorithms.tokenize(address, instance);
		words = words.filter(w => {
			return !instance.stopwords.find(x => instance.algorithms.latinize(x) === w);
		});

		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function deleteRemainingSpecialCharacters(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		let re = new RegExp(/[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/);
		const validSpecialCharacter = new RegExp(instance.validSpecialCharacters);
		let characters = validSpecialCharacter.toString();
		characters = characters.replace(/\\\.|\\\,|\\\°/g, '');
		characters = characters.replace(/\|\|\||\|\|/g, '|');
		characters = characters.replace(/^\(\|/, '(');
		characters = characters.replace(/\|\)$/, ')');
		characters = new RegExp(characters.slice(1, characters.length - 1));
		const deleteAtBegin = new RegExp('(\\s|^)' + characters.source + '(?=\\S)', 'gi');
		const deleteAtEnd = new RegExp('(\\S)' + characters.source + '(?=(\\s|$))', 'gi' );
		const deleteIsolate = new RegExp('\\s+' + characters.source + '\\s+', 'gi');
		let words = instance.algorithms.tokenize(address, instance);
		words = words.filter(w => {
			return (w.length > 1 && re.test(w)) ||
			(w.length === 1 && (instance.validSingleLetters.test(w) || /^[0-9]$/.test(w)));
		});

		newAddress = words.join(instance.separatorCharacter);
		newAddress = newAddress.replace(deleteAtBegin, '$1');
    newAddress = newAddress.replace(deleteAtEnd, '$1');
    newAddress = newAddress.replace(deleteIsolate, '');
	}
	return newAddress;
}
