//jshint expr:true
import chai from 'chai';

import transformations from './config';

let expect = chai.expect;


describe('Cleaner - General Configuration: ', () => {
	let mockInstance = {};
	let address = '';

	it('Should transformations be an array not empty', () => {
		expect(transformations).to.be.an('array');
		expect(transformations).to.not.be.empty;
	});

	it('Should all transformations must return a string if receive a string as address', () => {
		transformations.forEach(f => {
			expect(f(address,mockInstance)).to.be.a('string');
		});
	});

	it('Should all transformations must return a null if receive a null as address', () =>  {
		transformations.forEach(f => {
			expect(f(null,mockInstance)).to.be.a('null');
		});
	});
});
