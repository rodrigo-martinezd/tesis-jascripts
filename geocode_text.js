var fs = require('fs');
var _ = require('lodash');
var readline = require('readline');
var Promise = require('bluebird');
var moment = require('moment');
var googleMaps = require('@google/maps');


var ARRAY_STEP_SIZE = 10;
var REGEX_LATITUDE = /(\+|-)?((90\.0{1,6}0*)|(([0-9]|[1-8][0-9])\.[0-9]{1,6}[0-9]*))/gi;
var REGEX_LONGITUDE = /(\+|-)?((180\.0{1,6}0*)|(([0-9]|[1-9][0-9]|1[0-7][0-9])\.[0-9]{1,6}[0-9]*))/gi;
var step = 0;
var buffer = [];
var addressesGeocoded = 0;
var addressesPending = 0;
var progress = 0;
var startTime = moment();
var intervalId = null;
var dir = './data/';
var outDir = './data/out/';
var commandOptions = [
  {name: "src", type: _.toString},
];
var options = require('command-line-args')(commandOptions);
var r = readline.createInterface({
  input: fs.createReadStream(dir + options.src, {encoding:'utf8'})
});

var timestamp = Date.now();
var geofilename = outDir +  options.src.slice(0, options.src.length - 4) + '__geocoded__' + timestamp + '.txt';
var notGeofilename = outDir +  options.src.slice(0, options.src.length - 4) + '__not_geocoded__' + timestamp + '.txt';
var pendingOutfilename = outDir +  options.src.slice(0, options.src.length - 4) + '__pending_geocode__' + timestamp + '.txt';
var geoStream = fs.createWriteStream(geofilename);
var notGeoStream = fs.createWriteStream(notGeofilename);
var penStream = fs.createWriteStream(pendingOutfilename);

var googleMapsClient = googleMaps.createClient({
  //key: 'AIzaSyAAAaS2KVMjqtDxajbI5rGke1aRmZTRJqg',
	//key: 'AIzaSyB8B04MTIk7abJDVESr6SUF6f3Hgt1DPAY',
	//key: 'AIzaSyB-rDoxcAy80OA5KkbzrXB5p71HKduq3Ng', // Free Key
	clientId: 'gme-simpliroute',
	clientSecret: 'kbMbVAwMOm73IBK9sJK9lUDw5E4=',
  Promise: Promise
});

r.on('line', function(data) {
  if (!(REGEX_LATITUDE.test(data) || REGEX_LONGITUDE.test(data))) {
  	buffer.push(data);
	}
	console.log(data);
});

r.on('close', function() {
	console.log('entro a end de lectura');
  // 10 request per second for 90000 registers implied 3 hrs
  intervalId = setInterval(function(){
    var lower = step*ARRAY_STEP_SIZE;
    var upper = (step+1)*ARRAY_STEP_SIZE;
    upper = upper > buffer.length ? buffer.length : upper;

    if (progress >= buffer.length || lower >= upper) {
      setTimeout(function() {
        close();
      }, 5000);
      return;
    }

    var rows = buffer.slice(lower,upper);
    console.log("\n---------------------------------------------------------\n");

    _.forEach(rows, function(row) {
      setExtendedInfo(row);
    });

    step++;
  }, 1100);
});

function setExtendedInfo(address) {
	try {
	googleMapsClient.geocode({address: address, region: 'CL'}).asPromise()
		.then(function(response) {
			console.log(response.json.status);
			if (response.json.status === "OK") {
				addressesGeocoded++;
				if (response.json.results.length > 0 && response.json.results[0].geometry.location.lat &&
					response.json.results[0].geometry.location.lng) {
					console.log(response.json.results[0].geometry.location.lat);
					console.log(response.json.results[0].geometry.location.lng);
          geoStream.write(address + "\n");
        }
			}else if (response.json.status === "ZERO_RESULTS"){
				addressesGeocoded++;
				notGeoStream.write(address + "\n");
			}else if (response.json.status !== "INVALID_REQUEST") {
				addressesGeocoded++;
				notGeoStream.write(address + "\n");
			}

			progress++;
			notify(progress);

		})
		.catch(function(error) {
			console.error(error);
			console.log(address);
			progress++;
			addressesPending++;
			penStream.write(address+ "\n");
			notify(progress);
		});
	}catch(e) {
		console.error(e);
		console.log(address);
		progress++;
		addressesPending++;
		penStream.write(address+ "\n");
		notify(progress);
	}
}

function notify(progress) {
	console.log('Adress processing: %d,  percent: %d/100', progress, Math.floor((progress/buffer.length)*100));
}

function close() {

	console.log('=========================================================');
	console.log('Total addresses process: %d    %s', buffer.length, startTime.fromNow(true));
	console.log('Addresses Geocoded: %d', addressesGeocoded);
	console.log('Addresses Pending: %d', addressesPending);

	geoStream.close();
	notGeoStream.close();
	penStream.close();
}
