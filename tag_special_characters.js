var fs = require('fs');
var readline = require('readline');
var _ = require('lodash');
var processed = 0;
var dir = './data/';
var outDir = './data/out/';
var commandOptions = [
  {name: "src", type: _.toString},
];
var options = require('command-line-args')(commandOptions);
var r = readline.createInterface({
  input: fs.createReadStream(dir + options.src, {encoding:'utf8'})
});

var outfilename = outDir +  options.src.slice(0, options.src.length - 4) + '__tagged__' + Date.now() + '.txt';
var w = fs.createWriteStream(outfilename);

r.on('line', function(data) {
  save(tagAnythingElse(tagPointAfterTag(splitAndTagCommas(data))));
});

function splitAndTagCommas(data) {
  var newWord;
  var words = data.split(" ");
  var newLine = words.map(function(word) {
    newWord = word;
    if (/,$/.test(word)) {
      newWord = word.slice(0, word.length - 1) + ' ,|O';
    }
    return newWord;
  });

  return newLine.join(" ");
}

function tagPointAfterTag(data) {
  var newWord;
  var words = data.split(" ");
  var newLine = words.map(function(word) {
    newWord = word;
    if (/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)\./.test(word)) {
      var tag = word.match(/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)/);
      if(tag && tag.length >= 2) {
        newWord = word.replace('|' + tag[1], '') + '|' + tag[1];
      }
    }
    return newWord;
  });

  return newLine.join(" ");
}

function tagAnythingElse(data) {
  var newWord;
  var words = data.split(" ");
  var newLine = words.map(function(word) {
    newWord = word;
    if(!/\|([BI]_[SNCTDUROLZPFV]{1,3}|O)/.test(word)) {
      newWord = word + '|O';
    }

    return newWord;
  });

  return newLine.join(" ");
}

function save(line) {
  w.write(line + '\n');
  processed++;
  printProgress();
}



function printProgress() {
  console.log('Addresses processed: %d', processed);
}
