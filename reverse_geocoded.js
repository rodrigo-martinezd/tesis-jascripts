var fs = require('fs');
var _ = require('lodash');
var Promise = require('bluebird');
var moment = require('moment');
var JSONStream  = require('JSONStream');
var googleMaps = require('@google/maps');
//var addresses = require('./data/addresses_geocoded.json');
var addresses = require('./data/dataset_standardization__geo.json');


var ARRAY_STEP_SIZE = 10;
var step = 0;
var addressesGeocoded = 0;
var addressesPending = 0;
var progress = 0;
var startTime = moment();

var googleMapsClient = googleMaps.createClient({
	//key: 'AIzaSyAAAaS2KVMjqtDxajbI5rGke1aRmZTRJqg',
	//key: 'AIzaSyB-rDoxcAy80OA5KkbzrXB5p71HKduq3Ng', // Free Key
  clientId: 'gme-simpliroute',
  clientSecret: 'kbMbVAwMOm73IBK9sJK9lUDw5E4=',
	Promise: Promise
});

var addressStream = JSONStream.stringify();
var pendingAddressStream = JSONStream.stringify();

//var outAddressStream = fs.createWriteStream(__dirname + '/data/out/addresses_reverse_geocoded.json');
//var outPendingAddressStream = fs.createWriteStream(__dirname + '/data/out/addresses_reverse_geocoded_pending.json');

var outAddressStream = fs.createWriteStream(__dirname + '/data/out/dataset_standardization__reverse_geo.json');
var outPendingAddressStream = fs.createWriteStream(__dirname + '/data/out/dataset_standardization__reverse_geo_pending.json');

addressStream.pipe(outAddressStream);
pendingAddressStream.pipe(outPendingAddressStream);

// 10 request per second for 90000 registers inplie 3 hrs
var intervalId = setInterval(function(){
	var lower = step*ARRAY_STEP_SIZE;
	var upper = (step+1)*ARRAY_STEP_SIZE;
	upper = upper > addresses.length ? addresses.length : upper;

	if (progress >= addresses.length || lower >= upper) {
		setTimeout(function() {
			close();
		}, 5000);
		return;
	}

	var rows = addresses.slice(lower,upper);

	console.log("\n---------------------------------------------------------\n");

	_.forEach(rows, function(row) {
		setExtendedInfo(row);
	});

	step++;
}, 1100);


function setExtendedInfo(o) {
	try {
	googleMapsClient.reverseGeocode({latlng:{lat: o.latitude, lng: o.longitude}, result_type:"street_address", language: "es"}).asPromise()
		.then(function(response) {
			console.log(o.address);
			if (response.json.status === "OK") {
				o.reverse_address = response.json.results[0].formatted_address;

				addressesGeocoded++;
				addressStream.write(o);
			}else if (response.json.status === "ZERO_RESULTS"){
				o.reverse_address = null;

				addressesGeocoded++;
				addressStream.write(o);
			}else if (response.json.status !== "INVALID_REQUEST") {
				addressesPending++;
				pendingAddressStream.write(o);
			}

			progress++;
			notify(progress);

		})
		.catch(function(response) {
			console.log(o.address);
			progress++;
			addressesPending++;
			pendingAddressStream.write(o);
			notify(progress);
		});
	}catch(e) {
		console.log(o.address);
		progress++;
		addressesPending++;
		pendingAddressStream.write(o);
		notify(progress);
	}
}

function notify(progress) {
	console.log('Adress processing: %d,  percent: %d/100', progress, Math.floor((progress/addresses.length)*100));
}

function close() {
	clearInterval(intervalId);

	console.log('=========================================================');
	console.log('Total addresses process: %d    %s', addresses.length, startTime.fromNow(true));
	console.log('Addresses Geocoded: %d', addressesGeocoded);
	console.log('Addresses Pending: %d', addressesPending);

	addressStream.end();
	pendingAddressStream.end();
}
