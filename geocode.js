var fs = require('fs');
var _ = require('lodash');
var Promise = require('bluebird');
var moment = require('moment');
var JSONStream  = require('JSONStream');
var googleMaps = require('@google/maps');
//var addresses = require('./data/addresses.json');
var addresses = require('./data/dataset_standardization.json');


var ARRAY_STEP_SIZE = 10;
var REGEX_LATITUDE = /(\+|-)?((90\.0{1,6}0*)|(([0-9]|[1-8][0-9])\.[0-9]{1,6}[0-9]*))/gi;
var REGEX_LONGITUDE = /(\+|-)?((180\.0{1,6}0*)|(([0-9]|[1-9][0-9]|1[0-7][0-9])\.[0-9]{1,6}[0-9]*))/gi;
var step = 0;
var addressesGeocoded = 0;
var addressesPending = 0;
var progress = 0;
var startTime = moment();

var googleMapsClient = googleMaps.createClient({
	//key: 'AIzaSyAAAaS2KVMjqtDxajbI5rGke1aRmZTRJqg',
	// key: 'AIzaSyB-rDoxcAy80OA5KkbzrXB5p71HKduq3Ng', // Free Key
  clientId: 'gme-simpliroute',
  clientSecret: 'kbMbVAwMOm73IBK9sJK9lUDw5E4=',
	Promise: Promise
});

var addressStream = JSONStream.stringify();
var pendingAddressStream = JSONStream.stringify();

//var outAddressStream = fs.createWriteStream(__dirname + '/data/address_extended.json');
//var outPendingAddressStream = fs.createWriteStream(__dirname + '/data/address_pending.json');

var outAddressStream = fs.createWriteStream(__dirname + '/data/out/dataset_standardization__geo.json');
var outPendingAddressStream = fs.createWriteStream(__dirname + '/data/out/dataset_standardizatino_pending.json');


addressStream.pipe(outAddressStream);
pendingAddressStream.pipe(outPendingAddressStream);

// 10 request per second for 90000 registers inplie 3 hrs
var intervalId = setInterval(function(){
	var lower = step*ARRAY_STEP_SIZE;
	var upper = (step+1)*ARRAY_STEP_SIZE;
	upper = upper > addresses.length ? addresses.length : upper;

	if (progress >= addresses.length || lower >= upper) {
		setTimeout(function() {
			close();
		}, 5000);
		return;
	}

	var rows = addresses.slice(lower,upper);

	rows = _.filter(rows, function(row) {
		return !(REGEX_LATITUDE.test(row.address) || REGEX_LONGITUDE.test(row.address));
	});

	console.log("\n---------------------------------------------------------\n");

	_.forEach(rows, function(row) {
		setExtendedInfo(row);
	});

	step++;
}, 1100);


function setExtendedInfo(o) {
	try {
		console.log(o.address);
	googleMapsClient.geocode({address: o.address, region: 'CL'}).asPromise()
		.then(function(response) {
			console.log(o.address);
			if (response.json.status === "OK") {
				o.latitude = response.json.results[0].geometry.location.lat;
				o.longitude = response.json.results[0].geometry.location.lng;
				o.isGeorreference = !!o.latitude && !!o.longitude;
				o.isValid = o.isGeorreference;

				addressesGeocoded++;
				addressStream.write(o);
			}else if (response.json.status === "ZERO_RESULTS"){
				o.latitude = null;
				o.longitude = null;
				o.isGeorreference = false;
				o.isValid = null;

				addressesGeocoded++;
				addressStream.write(o);
			}else if (response.json.status !== "INVALID_REQUEST") {
				addressesPending++;
				pendingAddressStream.write(o);
			}

			progress++;
			notify(progress);

		})
		.catch(function(response) {
			console.log(response);
			console.log(o.address);
			progress++;
			addressesPending++;
			pendingAddressStream.write(o);
			notify(progress);
		});
	}catch(e) {
		console.log(o.address);
		progress++;
		addressesPending++;
		pendingAddressStream.write(o);
		notify(progress);
	}
}

function notify(progress) {
	console.log('Adress processing: %d,  percent: %d/100', progress, Math.floor((progress/addresses.length)*100));
}

function close() {
	clearInterval(intervalId);

	console.log('=========================================================');
	console.log('Total addresses process: %d    %s', addresses.length, startTime.fromNow(true));
	console.log('Addresses Geocoded: %d', addressesGeocoded);
	console.log('Addresses Pending: %d', addressesPending);

	addressStream.end();
	pendingAddressStream.end();
}
