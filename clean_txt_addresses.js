var fs = require('fs');
var readline = require('readline');
var _ = require('lodash');
var Cleaner = require('./cleaner/cleaner').default;

var cleaner = new Cleaner('cl');
var processed = 0;
var dir = './data/';
var outDir = './data/out/';
var commandOptions = [
  {name: "src", type: _.toString},
];
var options = require('command-line-args')(commandOptions);
var r = readline.createInterface({
  input: fs.createReadStream(dir + options.src, {encoding:'utf8'})
});

var outfilename = outDir +  options.src.slice(0, options.src.length - 4) + '__cleaned__' + Date.now() + '.txt';
var w = fs.createWriteStream(outfilename);

r.on('line', function(data) {
  saveCleanAddress(data);
});

function saveCleanAddress(address) {
  cleaner.clean(address).then(function(cleanAddress) {
    w.write(cleanAddress + '\n');
    processed++;
    printAddresses(address, cleanAddress);
    printProgress();
  })
    .catch(function() {
      processed++;
      printProgress();
    });
}

function printAddresses(original, cleaned) {
  console.log("\noriginal: %s", original);
  console.log("cleaned: %s\n", cleaned);
}

function printProgress() {
  console.log('Addresses processed: %d', processed);
}
