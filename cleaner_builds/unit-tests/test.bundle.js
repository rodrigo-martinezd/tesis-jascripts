require("source-map-support").install();
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("chai");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.__RewireAPI__ = exports.__ResetDependency__ = exports.__set__ = exports.__Rewire__ = exports.__GetDependency__ = exports.__get__ = exports.localConfigs = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _config = __webpack_require__(4);

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
    Reglas generales del cleaner


	[ X ] eliminacion de caracteres inválidos

	[ X ] eliminacion de expresiones (todo lo que este en llaves o destacado)

	[ X ] limpieza de palabras (aplicar reglas)

	[ X ] separar por otros caracteres alternativos reconocidos como separadores

	[ X ] dividir palabras enlazadas por algoritmo alternativo

	[ X ] eliminar characteres duplicados

	[  ] agregar pais si no lo tiene

	[ X ] eliminar multiples characteres separadores (TRIM)

	[ X ] eliminar caracteres especiales restantes

	[ X ] eliminar stopwords

**/

var _DefaultExportValue = [_get__('deleteInvalidCharacters'), _get__('deleteExpressions'), _get__('applyCleaningRules'), _get__('alternativeSplittingCharacters'), _get__('alternativeSplitAlgorithm'), _get__('deleteDuplicateCharacters'), _get__('trim'), _get__('deleteRemainingSpecialCharacters'), _get__('removeStopWords')];
exports.default = _DefaultExportValue;
var localConfigs = exports.localConfigs = {
	'cl': _get__('chileanConfig')
};

function deleteInvalidCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var replaceCharacter = !!instance.deleteOptions.delete ? '' : !!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ';

		var characters = address.split("");
		newAddress = characters.map(function (c) {
			if (!instance.validLetters.test(c) && !instance.validSpecialCharacters.test(c) && !/[0-9]/.test(c) && c !== instance.separatorCharacter) {
				return instance.specialCharactersReplacements.hasOwnProperty(c) ? instance.specialCharactersReplacements[c] : replaceCharacter || " ";
			} else {
				return c;
			}
		});
		newAddress = newAddress.join("");
	}
	return newAddress;
}

function deleteExpressions(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var replaceCharacter = !!instance.deleteOptions.delete ? '' : !!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ';

		instance.expressionsOptions.rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule, replaceCharacter);
		});
	}

	return newAddress;
}

function applyCleaningRules(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		instance.cleaningRules.forEach(function (transform) {
			newAddress = transform(newAddress, instance);
		});
	}
	return newAddress;
}

function alternativeSplittingCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);

		var filterMatchs = function filterMatchs(match) {
			return !instance.alternativeSplittingCharacters.exceptions.find(function (w) {
				return w === match;
			});
		};

		matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;

		while (matchs && matchs.length > 0) {
			newAddress = newAddress.replace(instance.alternativeSplittingCharacters.rule, instance.alternativeSplittingCharacters.replace);
			matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);
			matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;
		}
	}

	return newAddress;
}

function alternativeSplitAlgorithm(address, instance) {
	var newAddress = instance.useSplitWords ? instance.algorithms.split(address, instance) : address;
	return newAddress;
}

function escapeForRegex(str) {
	var escapingList = '+_~*=?¿\\/%$#!|°¬()[]{}^;:.,<>';
	str = str.split('');
	str = str.map(function (char) {
		return escapingList.includes(char) ? '\\' + char : char;
	});

	return str.join('');
}

function deleteDuplicateCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0 && newAddress.match(instance.repeatOptions.rule)) {
		var special = /[^a-zA-Z0-9ÁÉÍÓÚÜÑÀÈÌÒÙáéíóúüñàèìòù\s]/i;
		var tokens = instance.algorithms.tokenize(newAddress, instance);
		var newTokens = tokens.map(function (token) {
			var newToken = token;
			var matchs = token.match(instance.repeatOptions.rule);
			var totalMatchs = Array.isArray(matchs) ? matchs.reduce(function (prev, curr) {
				return prev + curr.length;
			}, 0) : 0;
			var containSpecialCharacters = Array.isArray(matchs) ? matchs.reduce(function (prev, curr) {
				return !prev ? special.test(curr) : prev;
			}, false) : false;

			if (containSpecialCharacters || matchs && matchs.length === 1 && totalMatchs === token.length && token.length >= 5 || totalMatchs > 0 && totalMatchs < token.length && token.length >= 5) {
				matchs.forEach(function (match) {
					var exclude = match[0] === instance.separatorCharacter;
					var freq = 1;
					var replacement = "";

					if (!exclude) {
						for (var i = 0; i < instance.repeatOptions.excludeCharacters.length; i++) {
							var o = instance.repeatOptions.excludeCharacters[i];
							if (match[0].toLowerCase() === o.char) {
								freq = o.freq;
								break;
							}
						}

						for (var _i = 0; _i < freq; _i++) {
							replacement += match[0];
						}
						var regex = new RegExp(_get__('escapeForRegex')(match), 'gi');
						newToken = newToken.replace(regex, replacement);
					}
				});
			}

			return newToken;
		});

		newAddress = newTokens.join(instance.separatorCharacter);
	}

	return newAddress;
}

/*function addCountryName(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		const isNumber = /^\d+$/;
		const haveCountry = new RegExp(instance.countryName, "gi");
		let tokens = newAddress
			.split(instance.separatorCharacter)
			.filter(w => w !== instance.separatorCharacter);

		let containOnlyNumbers = tokens
			.reduce((prev,curr,i, arr) => prev ? isNumber.test(arr[i]) : prev, true );

		if (!haveCountry.test(newAddress) && !containOnlyNumbers) {
			newAddress = newAddress.replace(/(\S)\s*\,?$/gi, '$1, ' + instance.countryName);
		}
	}
	return newAddress;
}*/

function trim(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var words = instance.algorithms.tokenize(address, instance);
		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function removeStopWords(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var words = instance.algorithms.tokenize(address, instance);
		words = words.filter(function (w) {
			return !instance.stopwords.find(function (x) {
				return instance.algorithms.latinize(x) === w;
			});
		});

		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function deleteRemainingSpecialCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var re = new RegExp(/[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/);
		var validSpecialCharacter = new RegExp(instance.validSpecialCharacters);
		var characters = validSpecialCharacter.toString();
		characters = characters.replace(/\\\.|\\\,|\\\°/g, '');
		characters = characters.replace(/\|\|\||\|\|/g, '|');
		characters = characters.replace(/^\(\|/, '(');
		characters = characters.replace(/\|\)$/, ')');
		characters = new RegExp(characters.slice(1, characters.length - 1));
		var deleteAtBegin = new RegExp('(\\s|^)' + characters.source + '(?=\\S)', 'gi');
		var deleteAtEnd = new RegExp('(\\S)' + characters.source + '(?=(\\s|$))', 'gi');
		var deleteIsolate = new RegExp('\\s+' + characters.source + '\\s+', 'gi');
		var words = instance.algorithms.tokenize(address, instance);
		words = words.filter(function (w) {
			return w.length > 1 && re.test(w) || w.length === 1 && (instance.validSingleLetters.test(w) || /^[0-9]$/.test(w));
		});

		newAddress = words.join(instance.separatorCharacter);
		newAddress = newAddress.replace(deleteAtBegin, '$1');
		newAddress = newAddress.replace(deleteAtEnd, '$1');
		newAddress = newAddress.replace(deleteIsolate, '');
	}
	return newAddress;
}

function _getGlobalObject() {
	try {
		if (!!global) {
			return global;
		}
	} catch (e) {
		try {
			if (!!window) {
				return window;
			}
		} catch (e) {
			return this;
		}
	}
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
	if (_RewireModuleId__ === null) {
		var globalVariable = _getGlobalObject();

		if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
			globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
		}

		_RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
	}

	return _RewireModuleId__;
}

function _getRewireRegistry__() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
		theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
	}

	return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
	var moduleId = _getRewireModuleId__();

	var registry = _getRewireRegistry__();

	var rewireData = registry[moduleId];

	if (!rewireData) {
		registry[moduleId] = Object.create(null);
		rewireData = registry[moduleId];
	}

	return rewireData;
}

(function registerResetAll() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable['__rewire_reset_all__']) {
		theGlobalVariable['__rewire_reset_all__'] = function () {
			theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
		};
	}
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
	function addPropertyToAPIObject(name, value) {
		Object.defineProperty(_RewireAPI__, name, {
			value: value,
			enumerable: false,
			configurable: true
		});
	}

	addPropertyToAPIObject('__get__', _get__);
	addPropertyToAPIObject('__GetDependency__', _get__);
	addPropertyToAPIObject('__Rewire__', _set__);
	addPropertyToAPIObject('__set__', _set__);
	addPropertyToAPIObject('__reset__', _reset__);
	addPropertyToAPIObject('__ResetDependency__', _reset__);
	addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _get_original__(variableName);
	} else {
		var value = rewireData[variableName];

		if (value === INTENTIONAL_UNDEFINED) {
			return undefined;
		} else {
			return value;
		}
	}
}

function _get_original__(variableName) {
	switch (variableName) {
		case 'chileanConfig':
			return _config2.default;

		case 'escapeForRegex':
			return escapeForRegex;

		case 'deleteInvalidCharacters':
			return deleteInvalidCharacters;

		case 'deleteExpressions':
			return deleteExpressions;

		case 'applyCleaningRules':
			return applyCleaningRules;

		case 'alternativeSplittingCharacters':
			return alternativeSplittingCharacters;

		case 'alternativeSplitAlgorithm':
			return alternativeSplitAlgorithm;

		case 'deleteDuplicateCharacters':
			return deleteDuplicateCharacters;

		case 'trim':
			return trim;

		case 'deleteRemainingSpecialCharacters':
			return deleteRemainingSpecialCharacters;

		case 'removeStopWords':
			return removeStopWords;
	}

	return undefined;
}

function _assign__(variableName, value) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _set_original__(variableName, value);
	} else {
		return rewireData[variableName] = value;
	}
}

function _set_original__(variableName, _value) {
	switch (variableName) {}

	return undefined;
}

function _update_operation__(operation, variableName, prefix) {
	var oldValue = _get__(variableName);

	var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

	_assign__(variableName, newValue);

	return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
	var rewireData = _getRewiredData__();

	if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
		Object.keys(variableName).forEach(function (name) {
			rewireData[name] = variableName[name];
		});
	} else {
		if (value === undefined) {
			rewireData[variableName] = INTENTIONAL_UNDEFINED;
		} else {
			rewireData[variableName] = value;
		}

		return function () {
			_reset__(variableName);
		};
	}
}

function _reset__(variableName) {
	var rewireData = _getRewiredData__();

	delete rewireData[variableName];

	if (Object.keys(rewireData).length == 0) {
		delete _getRewireRegistry__()[_getRewireModuleId__];
	}

	;
}

function _with__(object) {
	var rewireData = _getRewiredData__();

	var rewiredVariableNames = Object.keys(object);
	var previousValues = {};

	function reset() {
		rewiredVariableNames.forEach(function (variableName) {
			rewireData[variableName] = previousValues[variableName];
		});
	}

	return function (callback) {
		rewiredVariableNames.forEach(function (variableName) {
			previousValues[variableName] = rewireData[variableName];
			rewireData[variableName] = object[variableName];
		});
		var result = callback();

		if (!!result && typeof result.then == 'function') {
			result.then(reset).catch(reset);
		} else {
			reset();
		}

		return result;
	};
}

var _typeOfOriginalExport = typeof _DefaultExportValue === 'undefined' ? 'undefined' : _typeof(_DefaultExportValue);

function addNonEnumerableProperty(name, value) {
	Object.defineProperty(_DefaultExportValue, name, {
		value: value,
		enumerable: false,
		configurable: true
	});
}

if ((_typeOfOriginalExport === 'object' || _typeOfOriginalExport === 'function') && Object.isExtensible(_DefaultExportValue)) {
	addNonEnumerableProperty('__get__', _get__);
	addNonEnumerableProperty('__GetDependency__', _get__);
	addNonEnumerableProperty('__Rewire__', _set__);
	addNonEnumerableProperty('__set__', _set__);
	addNonEnumerableProperty('__reset__', _reset__);
	addNonEnumerableProperty('__ResetDependency__', _reset__);
	addNonEnumerableProperty('__with__', _with__);
	addNonEnumerableProperty('__RewireAPI__', _RewireAPI__);
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("sinon");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
	reemplazo de abreviaturas (acronimos) // pais
	eliminacion de stopwords // pais

**/

var _DefaultExportValue = {
	countryCode: 'cl',
	allowOverrideConfiguration: false,
	countryName: "Chile",
	useSplitWords: true,
	separatorCharacter: ' ',
	alternativeSplittingCharacters: {
		rule: /([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])(\/|\\|\-|\_)+([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])/gi,
		replace: "$1 $3",
		exceptions: ['S/N', 's/n', 'n/a', 'N/A', 'S/n', 's/N', 'N/a', 'n/A']
	},
	validLetters: /[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]/i,
	validSingleLetters: /[a-zA-z]/,
	validSpecialCharacters: /(\'|\"|\*|\(|\)|\{|\}|\[|\]|\-|\\|\/|\.|\,|\:|\+|\°)/,
	specialCharactersReplacements: { '?': '', 'º': '°', 'ª': '°' },
	deleteOptions: {
		replace: true,
		replacementCharacter: ' ' // char
	},
	repeatOptions: {
		rule: /([^0-9\s])\1+/gi,
		excludeCharacters: [{ char: 'r', freq: 2 }, { char: 'l', freq: 2 }, { char: 'c', freq: 2 }, { char: 's', freq: 2 }, { char: 'g', freq: 2 }, { char: 'f', freq: 2 }, { char: 'n', freq: 2 }, { char: 'e', freq: 2 }, { char: 'm', freq: 2 }, { char: 't', freq: 2 }, { char: 'p', freq: 2 }]
	},
	expressionsOptions: {
		rules: [/\+?(\(?(56(\-|\s)?)?\)?)?(\(?0?9\)?(\-|\s)?[0-9]{8}|\(?0?22\)?(\-|\s)?[0-9]{7})/gi, // phoneNumber
		/((\(|\[|\{)|(\"|\'|\*|\+|\%|\#)\3*).*((\)|\]|\})|(\"|\'|\*|\+|\%|\#)\6*)/gi, // all text between brackets or "'+*#%
		/\(.+\,/gi, // everything between a bracket and a comma
		/T?[012]?[0-9]\:[0-9]{2}(\:[0-9]{2})?(\.[0-9]{0,6})?(Z|\s?(\+|\-)?[012][0-9]\:[0-9]{2})?/gi, // time
		/(([012]?\d|3[01])(\/|\-)[01]?\d(\/|\-)([12]\d{3}|\d{2})|([12]\d{3}|\d{2})(\/|\-)[01]?\d(\/|\-)([012]?\d|3[01]))/gi, // date
		/[^0-9](\-|\+)?(90\.0{5,}|[0-8]?[0-9]\.[0-9]{5,})/g, //latitude
		/(\-|\+)?(180\.0{5,}|(1[0-7][0-9]|[0-9]{1,2})\.[0-9]{5,})/g // longitude
		]
	},
	cleaningRules: [_get__('cleanAsteriks'), _get__('cleanPads'), _get__('cleanTwoPoints'), _get__('cleanSlashs'), _get__('cleanPoints'), _get__('cleanHyphes'), _get__('cleanCommas')],
	algorithms: {
		split: _get__('splitWordsAlgorithm'),
		tokenize: _get__('tokenizerAlgorithm'),
		latinize: _get__('latinizeAlgorithm')
	}
};
exports.default = _DefaultExportValue;

// CLEANING RULES

function cleanAsteriks(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [{ regex: /(n|N)\*(?=(\s+)?\d+)/gi, replacement: 'N° ' }, { regex: /\*/gi, replacement: instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanPads(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Agrega un espacio antes de #1234
		{ regex: /(\#\d)/gi, replacement: instance.separatorCharacter + '$1' },
		// Reemplaza # al principio de una letra por un espacio
		{ regex: /\#([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: instance.separatorCharacter + '$1' },
		// Reemplaza # al final de una letra por un espacio
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\#/gi, replacement: '$1' + instance.separatorCharacter },
		// Reemplaza # al final de un número por un espacio
		{ regex: /(\d)\#/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanTwoPoints(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Eliminar d:d expresiones
		{ regex: /\d+\:\d+/gi, replacement: instance.separatorCharacter },
		// Reemplaza A:21 por A-21
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter + '$2-' },
		// Reemplaza 21:B por 21-B
		{ regex: /(\s+|^)(\d+)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter + '$2-' },
		// Reemplaza A-:21 por A-21
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]\-)\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter + '$2' },
		// Reemplaza 21-:B por 21-B
		{ regex: /(\s+|^)(\d+\-)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter + '$2' },
		// Reemplaza palabra: por palabra_ para evitar que se borre :
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\:/g, replacement: '$1_' },
		// Reemplaza : por un espacio si no es la continuacion de una palabra
		{ regex: /\:/gi, replacement: instance.separatorCharacter },
		// Reincorpora palabra_  como palabra:
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\_/g, replacement: '$1:' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanSlashs(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Reemplazar S/N con un guion en medio para que no sea eliminado
		{ regex: /(\s+|^)(S|s)(\s{0,3})\/(\s{0,3})(N|n)/gi, replacement: instance.separatorCharacter + '$2_$5' + instance.separatorCharacter },
		// Reemplazar N/A con un guin en medio para que no sea eliminado
		{ regex: /(\s+|^)(N|n)(\s{0,3})\/(\s{0,3})(A|a)/gi, replacement: instance.separatorCharacter + '$2_$5' + instance.separatorCharacter },
		// Remplazar d/d por d_d para que no sea eliminada
		{ regex: /(\d)\/(\d)/gi, replacement: '$1_$2' },
		// Reemplazar todos los slash con espacios
		{ regex: /(\\|\/)/gi, replacement: instance.separatorCharacter },
		// Restituir d_d a d/d
		{ regex: /(\d)\_(\d)/g, replacement: '$1/$2' },
		// Restituir S_N a S/N
		{ regex: /(\s+|^)(S|s)\_(N|n)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter },
		// Restituir N_A a N/A
		{ regex: /(\s+|^)(N|n)\_(A|a)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanPoints(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {

		var rules = [
		// numeros con multiples puntos ej: 11.1.1.1.1.1
		{ regex: /\.*(\d+\.\d+|\d+\.){2,}\d+?\.*/gi, replacement: instance.separatorCharacter },
		// N.1000 -> N° 1000
		{ regex: /(\s+|^)N\.(\d+)/g, replacement: instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2' },
		// reemplazar . por - si esta en medio de una letra y un numero ej: A.2 -> A-2
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\.(?=\d+(\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-' },
		// reemplazar . por - si esta al entre un numero y una letra ej: 14.B -> 14-B
		{ regex: /(\s+|^)(\d+)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-' },
		// agregar espacio a la derecha de . si . esta en medio de una palabra y un numero
		{ regex: /([^\s\.\d]{2,}\.)(?=\d+)/g, replacement: '$1' + instance.separatorCharacter },
		// palabras con . al principio
		{ regex: /(\s+|^)\.(?=[^\s\.\d]+)/g, replacement: instance.separatorCharacter },
		// numeros con . al principio
		{ regex: /(\s+|^)\.(?=\d+)/g, replacement: instance.separatorCharacter },
		// unir numeros con coma si en medio a un punto
		{ regex: /(\d+)\.\,/g, replacement: '$1,' + instance.separatorCharacter },
		// numeros con . al final
		{ regex: /(\d+)\.(?=(\s+|(\s+)?$))/g, replacement: '$1' + instance.separatorCharacter },
		// separar av.|avda.|adva.|avnda.|adva.de palabras
		{ regex: /(\s+|^)(av|avda|adva|avnda|adva)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)/gi, replacement: instance.separatorCharacter + '$2.' + instance.separatorCharacter }, { regex: /([a-zA-Z]{2}[a-zA-Z]+)\.+([a-zA-Z]{2}[a-zA-Z]+)/gi, replacement: '$1.' + instance.separatorCharacter + '$2' }, { regex: /([a-zA-Z]+)\.+\,/gi, replacement: '$1.' + instance.separatorCharacter + ',' }, { regex: /([0-9]+)\.+([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü-]+)/gi, replacement: '$1' + instance.separatorCharacter + '$2' }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanHyphes(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// L-10 -> L. 10
		{ regex: /(\s+|^)L\-(?=\d+)/g, replacement: instance.separatorCharacter + 'L.' + instance.separatorCharacter },
		// unir letra con numero que tienen un guion en medio separado por espacios
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ])\s{1,2}\-\s{1,2}(\d+)/g, replacement: instance.separatorCharacter + '$2-$3' + instance.separatorCharacter },
		// separar palabras de más de 1 caracter unidas por un - a un numero
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})\-(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter },
		// separar numeros unidos por un guion a palabras de más de 1 caracter
		{ regex: /(\d+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})/gi, replacement: '$1' + instance.separatorCharacter },
		// separar palabras unidas por un -
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// reemplazar - al principio de una palabra o numero por un espacio
		{ regex: /(\s+|^)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter },
		// reemplazar - al final de una palabra o numero por un espacio
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=(\s+|(\s+)?$))/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanCommas(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// remover espacio sobrante entre un caracter alfanumerico y una coma
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\s+\,/g, replacement: '$1,' + instance.separatorCharacter },
		// reemplazar multiples comas separadas por espacios por solo una a la derecha del ultimo caracter alfanumerico
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,?(\s+\,)+(\s+)?/g, replacement: '$1,' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre un numero y una letra
		{ regex: /(\d+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre una letra y un numero
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre 2 letras
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// eliminar coma al inicio de una palabra o numero
		{ regex: /(\s+|^)\,(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter },
		// eliminar dos comas consecutivas al final de una palabra
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,{2,}/gi, replacement: '$1,' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

// ALGORITHMS

function splitWordsAlgorithm(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// separa N°1000 -> N° 1000
		{ regex: /(.+|^)N\°(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2' },
		// separa KM68.5 -> KM 68.5
		{ regex: /(.+|^)KM(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'KM' + instance.separatorCharacter + '$2' },
		// separa of-401
		{ regex: /((of|OF|Of)\.?)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: '$1' + instance.separatorCharacter },
		// si una minuscula se encuentra con una mayuscula las separa
		{ regex: /([a-záéíúóüàèìòùñ])(?=[A-ZÁÉÍÓÚÜÀÈÌÒÙÑ])/g, replacement: '$1' + instance.separatorCharacter },

		// si dos palabras de al menos 2 characteres se encuentran divididas por -, _, \ o / los separa
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})(\-|\_|\/|\\)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})/gi, replacement: '$1' + instance.separatorCharacter },

		// si una palabra se encuentra con un numero y es superior a 2 characteres los separa
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})(\-|\_)?(?=[0-9])/gi, replacement: '$1' + instance.separatorCharacter },

		// si un numero se encuentra con una palabra superior a 2 caracteres los separa
		{ regex: /([0-9])(\-|\_)?(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})/gi, replacement: '$1' + instance.separatorCharacter },

		// separar N°BCG y NH°123
		{ regex: /(N.?\°)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü0-9])/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function tokenizerAlgorithm(address, instance) {
	var tokens = [];
	if (typeof address === "string" && address.length > 0) {
		var re = /[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/;
		tokens = address.split(instance.separatorCharacter);
		tokens = tokens.filter(function (token) {
			return token.length > 1 && re.test(token) || token.length === 1 && (instance.validSingleLetters.test(token) || /^[0-9]$/.test(token));
		});

		tokens = tokens.map(function (w, i, arr) {
			return arr[i].trim();
		});
	}
	return tokens;
}

function latinizeAlgorithm(token) {
	var tokenLatinized = token;
	if (typeof token === "string" && token.length > 0) {
		var isUpper = /[A-ZÁÉÍÓÚÀÈÌÒÙÜÑ]/;
		var replacementsMap = {
			'á': 'a',
			'é': 'e',
			'í': 'i',
			'ó': 'o',
			'ú': 'u',
			'à': 'a',
			'è': 'e',
			'ì': 'i',
			'ò': 'o',
			'ù': 'u',
			'ü': 'u'
		};

		var chars = tokenLatinized.split("");
		chars = chars.map(function (c) {
			var upper = isUpper.test(c);
			c = c.toLowerCase();
			c = replacementsMap[c] ? replacementsMap[c] : c;
			return upper ? c.toUpperCase() : c;
		});
		tokenLatinized = chars.join("");
	}
	return tokenLatinized;
}

function _getGlobalObject() {
	try {
		if (!!global) {
			return global;
		}
	} catch (e) {
		try {
			if (!!window) {
				return window;
			}
		} catch (e) {
			return this;
		}
	}
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
	if (_RewireModuleId__ === null) {
		var globalVariable = _getGlobalObject();

		if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
			globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
		}

		_RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
	}

	return _RewireModuleId__;
}

function _getRewireRegistry__() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
		theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
	}

	return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
	var moduleId = _getRewireModuleId__();

	var registry = _getRewireRegistry__();

	var rewireData = registry[moduleId];

	if (!rewireData) {
		registry[moduleId] = Object.create(null);
		rewireData = registry[moduleId];
	}

	return rewireData;
}

(function registerResetAll() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable['__rewire_reset_all__']) {
		theGlobalVariable['__rewire_reset_all__'] = function () {
			theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
		};
	}
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
	function addPropertyToAPIObject(name, value) {
		Object.defineProperty(_RewireAPI__, name, {
			value: value,
			enumerable: false,
			configurable: true
		});
	}

	addPropertyToAPIObject('__get__', _get__);
	addPropertyToAPIObject('__GetDependency__', _get__);
	addPropertyToAPIObject('__Rewire__', _set__);
	addPropertyToAPIObject('__set__', _set__);
	addPropertyToAPIObject('__reset__', _reset__);
	addPropertyToAPIObject('__ResetDependency__', _reset__);
	addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _get_original__(variableName);
	} else {
		var value = rewireData[variableName];

		if (value === INTENTIONAL_UNDEFINED) {
			return undefined;
		} else {
			return value;
		}
	}
}

function _get_original__(variableName) {
	switch (variableName) {
		case 'cleanAsteriks':
			return cleanAsteriks;

		case 'cleanPads':
			return cleanPads;

		case 'cleanTwoPoints':
			return cleanTwoPoints;

		case 'cleanSlashs':
			return cleanSlashs;

		case 'cleanPoints':
			return cleanPoints;

		case 'cleanHyphes':
			return cleanHyphes;

		case 'cleanCommas':
			return cleanCommas;

		case 'splitWordsAlgorithm':
			return splitWordsAlgorithm;

		case 'tokenizerAlgorithm':
			return tokenizerAlgorithm;

		case 'latinizeAlgorithm':
			return latinizeAlgorithm;
	}

	return undefined;
}

function _assign__(variableName, value) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _set_original__(variableName, value);
	} else {
		return rewireData[variableName] = value;
	}
}

function _set_original__(variableName, _value) {
	switch (variableName) {}

	return undefined;
}

function _update_operation__(operation, variableName, prefix) {
	var oldValue = _get__(variableName);

	var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

	_assign__(variableName, newValue);

	return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
	var rewireData = _getRewiredData__();

	if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
		Object.keys(variableName).forEach(function (name) {
			rewireData[name] = variableName[name];
		});
	} else {
		if (value === undefined) {
			rewireData[variableName] = INTENTIONAL_UNDEFINED;
		} else {
			rewireData[variableName] = value;
		}

		return function () {
			_reset__(variableName);
		};
	}
}

function _reset__(variableName) {
	var rewireData = _getRewiredData__();

	delete rewireData[variableName];

	if (Object.keys(rewireData).length == 0) {
		delete _getRewireRegistry__()[_getRewireModuleId__];
	}

	;
}

function _with__(object) {
	var rewireData = _getRewiredData__();

	var rewiredVariableNames = Object.keys(object);
	var previousValues = {};

	function reset() {
		rewiredVariableNames.forEach(function (variableName) {
			rewireData[variableName] = previousValues[variableName];
		});
	}

	return function (callback) {
		rewiredVariableNames.forEach(function (variableName) {
			previousValues[variableName] = rewireData[variableName];
			rewireData[variableName] = object[variableName];
		});
		var result = callback();

		if (!!result && typeof result.then == 'function') {
			result.then(reset).catch(reset);
		} else {
			reset();
		}

		return result;
	};
}

var _typeOfOriginalExport = typeof _DefaultExportValue === 'undefined' ? 'undefined' : _typeof(_DefaultExportValue);

function addNonEnumerableProperty(name, value) {
	Object.defineProperty(_DefaultExportValue, name, {
		value: value,
		enumerable: false,
		configurable: true
	});
}

if ((_typeOfOriginalExport === 'object' || _typeOfOriginalExport === 'function') && Object.isExtensible(_DefaultExportValue)) {
	addNonEnumerableProperty('__get__', _get__);
	addNonEnumerableProperty('__GetDependency__', _get__);
	addNonEnumerableProperty('__Rewire__', _set__);
	addNonEnumerableProperty('__set__', _set__);
	addNonEnumerableProperty('__reset__', _reset__);
	addNonEnumerableProperty('__ResetDependency__', _reset__);
	addNonEnumerableProperty('__with__', _with__);
	addNonEnumerableProperty('__RewireAPI__', _RewireAPI__);
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(6);
module.exports = __webpack_require__(7);


/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var context = __webpack_require__(8);
_get__('context').keys().forEach(_get__('context'));
module.exports = _get__('context');

function _getGlobalObject() {
  try {
    if (!!global) {
      return global;
    }
  } catch (e) {
    try {
      if (!!window) {
        return window;
      }
    } catch (e) {
      return this;
    }
  }
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
  if (_RewireModuleId__ === null) {
    var globalVariable = _getGlobalObject();

    if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
      globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
    }

    _RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
  }

  return _RewireModuleId__;
}

function _getRewireRegistry__() {
  var theGlobalVariable = _getGlobalObject();

  if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
    theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
  }

  return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
  var moduleId = _getRewireModuleId__();

  var registry = _getRewireRegistry__();

  var rewireData = registry[moduleId];

  if (!rewireData) {
    registry[moduleId] = Object.create(null);
    rewireData = registry[moduleId];
  }

  return rewireData;
}

(function registerResetAll() {
  var theGlobalVariable = _getGlobalObject();

  if (!theGlobalVariable['__rewire_reset_all__']) {
    theGlobalVariable['__rewire_reset_all__'] = function () {
      theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
    };
  }
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
  function addPropertyToAPIObject(name, value) {
    Object.defineProperty(_RewireAPI__, name, {
      value: value,
      enumerable: false,
      configurable: true
    });
  }

  addPropertyToAPIObject('__get__', _get__);
  addPropertyToAPIObject('__GetDependency__', _get__);
  addPropertyToAPIObject('__Rewire__', _set__);
  addPropertyToAPIObject('__set__', _set__);
  addPropertyToAPIObject('__reset__', _reset__);
  addPropertyToAPIObject('__ResetDependency__', _reset__);
  addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
  var rewireData = _getRewiredData__();

  if (rewireData[variableName] === undefined) {
    return _get_original__(variableName);
  } else {
    var value = rewireData[variableName];

    if (value === INTENTIONAL_UNDEFINED) {
      return undefined;
    } else {
      return value;
    }
  }
}

function _get_original__(variableName) {
  switch (variableName) {
    case 'context':
      return context;
  }

  return undefined;
}

function _assign__(variableName, value) {
  var rewireData = _getRewiredData__();

  if (rewireData[variableName] === undefined) {
    return _set_original__(variableName, value);
  } else {
    return rewireData[variableName] = value;
  }
}

function _set_original__(variableName, _value) {
  switch (variableName) {}

  return undefined;
}

function _update_operation__(operation, variableName, prefix) {
  var oldValue = _get__(variableName);

  var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

  _assign__(variableName, newValue);

  return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
  var rewireData = _getRewiredData__();

  if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
    Object.keys(variableName).forEach(function (name) {
      rewireData[name] = variableName[name];
    });
  } else {
    if (value === undefined) {
      rewireData[variableName] = INTENTIONAL_UNDEFINED;
    } else {
      rewireData[variableName] = value;
    }

    return function () {
      _reset__(variableName);
    };
  }
}

function _reset__(variableName) {
  var rewireData = _getRewiredData__();

  delete rewireData[variableName];

  if (Object.keys(rewireData).length == 0) {
    delete _getRewireRegistry__()[_getRewireModuleId__];
  }

  ;
}

function _with__(object) {
  var rewireData = _getRewiredData__();

  var rewiredVariableNames = Object.keys(object);
  var previousValues = {};

  function reset() {
    rewiredVariableNames.forEach(function (variableName) {
      rewireData[variableName] = previousValues[variableName];
    });
  }

  return function (callback) {
    rewiredVariableNames.forEach(function (variableName) {
      previousValues[variableName] = rewireData[variableName];
      rewireData[variableName] = object[variableName];
    });
    var result = callback();

    if (!!result && typeof result.then == 'function') {
      result.then(reset).catch(reset);
    } else {
      reset();
    }

    return result;
  };
}

var _typeOfOriginalExport = _typeof(module.exports);

function addNonEnumerableProperty(name, value) {
  Object.defineProperty(module.exports, name, {
    value: value,
    enumerable: false,
    configurable: true
  });
}

if ((_typeOfOriginalExport === 'object' || _typeOfOriginalExport === 'function') && Object.isExtensible(module.exports)) {
  addNonEnumerableProperty('__get__', _get__);
  addNonEnumerableProperty('__GetDependency__', _get__);
  addNonEnumerableProperty('__Rewire__', _set__);
  addNonEnumerableProperty('__set__', _set__);
  addNonEnumerableProperty('__reset__', _reset__);
  addNonEnumerableProperty('__ResetDependency__', _reset__);
  addNonEnumerableProperty('__with__', _with__);
  addNonEnumerableProperty('__RewireAPI__', _RewireAPI__);
}

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./cl/config.spec.js": 9,
	"./cleaner.spec.js": 10,
	"./config.spec.js": 16
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 8;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.__RewireAPI__ = exports.__ResetDependency__ = exports.__set__ = exports.__Rewire__ = exports.__GetDependency__ = exports.__get__ = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /* jshint expr: true */


var _chai = __webpack_require__(0);

var _chai2 = _interopRequireDefault(_chai);

var _sinon = __webpack_require__(2);

var _sinon2 = _interopRequireDefault(_sinon);

var _lodash = __webpack_require__(3);

var _lodash2 = _interopRequireDefault(_lodash);

var _config = __webpack_require__(4);

var _config2 = _interopRequireDefault(_config);

var _config3 = __webpack_require__(1);

var _config4 = _interopRequireDefault(_config3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _get__('chai').expect;
var sandbox = null;

describe('Cleaner - [CL] Configuration', function () {
	var instance = _get__('_').merge({}, _get__('config'));
	var clean = function clean(address, instance) {
		_get__('transformations').forEach(function (transform) {
			address = transform(address, instance);
		});
		return address;
	};

	instance.stopwords = ['desde', 'hasta', 'por'];

	beforeEach(function () {
		_assign__('sandbox', _get__('sinon').createSandbox());
	});

	afterEach(function () {
		_get__('sandbox').restore();
	});

	describe('deleting invalid characters', function () {
		var transform = _get__('transformations')[0];

		it('Should replace letters distinct than a-zA-záéíóíÁÉÍÓÚñÑÜü with space', function () {
			_get__('expect')(transform('áéíóúÁÉÍÓÚñÑÜü', instance)).to.equal('áéíóúÁÉÍÓÚñÑÜü');
			_get__('expect')(transform('abcdefghijklmnñopqrstuvwxyz', instance)).to.equal('abcdefghijklmnñopqrstuvwxyz');
			_get__('expect')(transform('ABCDEFGHIJKLMNÑOPQRSTUVWXYZ', instance)).to.equal('ABCDEFGHIJKLMNÑOPQRSTUVWXYZ');
			_get__('expect')(transform('ÂÇăģěõ', instance)).to.equal('      ');
		});

		it('Should remove character ?', function () {
			_get__('expect')(transform('calle pe?unias', instance)).to.equal('calle peunias');
		});

		it('Should replace special characters distinct than  \'"*(){}[]-\\/.,:+ª° with space', function () {
			_get__('expect')(transform('#!&$~;', instance)).to.equal('      ');
			_get__('expect')(transform('°ª\'"()[]{}-\\/.+:,', instance)).to.equal('°°\'"()[]{}-\\/.+:,');
		});

		it('Should replace character ª and º with °', function () {
			_get__('expect')(transform('#!&$~;', instance)).to.equal('      ');
		});

		it('Should not remove or replace space character', function () {
			_get__('expect')(transform('ªº', instance)).to.equal('°°');
		});
	});

	describe('deleting expressions', function () {
		var transform = _get__('transformations')[1];

		it('Should remove phone numbers', function () {
			_get__('expect')(transform('cel: 56949123580', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: +(56)(09)49123580', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: 9-49123580', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: 0228574894', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: (56)(22)8574894', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: 22-8574894', instance)).to.equal('cel:  ');
			_get__('expect')(transform('cel: 56-22-8574894', instance)).to.equal('cel:  ');
		});

		it('Should remove everything surrounded with ()[]{}**""\'\'+', function () {
			_get__('expect')(transform('asd(  asd   asd asdasd )asd', instance)).to.equal('asd asd');
			_get__('expect')(transform('***fritz***', instance)).to.equal(' ');
			_get__('expect')(transform('[extra info]', instance)).to.equal(' ');
			_get__('expect')(transform('\'enfasis\'', instance)).to.equal(' ');
			_get__('expect')(transform('"enfasis"', instance)).to.equal(' ');
			_get__('expect')(transform('{asda}', instance)).to.equal(' ');
			_get__('expect')(transform('+asd+', instance)).to.equal(' ');
		});

		it('Should remove everything between a ( and a ,', function () {
			_get__('expect')(transform('  (asd asd asd asd,', instance)).to.equal('   ');
			_get__('expect')(transform('  ( asd asd asd asd, ', instance)).to.equal('    ');
			_get__('expect')(transform('(asd asd asd asd , ', instance)).to.equal('  ');
		});

		it('Should remove hours', function () {
			_get__('expect')(transform('10:00', instance)).to.equal(' ');
			_get__('expect')(transform('9:00', instance)).to.equal(' ');
			_get__('expect')(transform('10:00:00', instance)).to.equal(' ');
			_get__('expect')(transform('10:00:00.123123', instance)).to.equal(' ');
			_get__('expect')(transform('10:00:00.123456Z', instance)).to.equal(' ');
			_get__('expect')(transform('10:00:00.123456 +04:00', instance)).to.equal(' ');
			_get__('expect')(transform('T10:00:00.123456 +04:00', instance)).to.equal(' ');
		});

		it('Should remove dates', function () {
			_get__('expect')(transform('14/05/1992', instance)).to.equal(' ');
			_get__('expect')(transform('14/05/92', instance)).to.equal(' ');
			_get__('expect')(transform('14-05-1992', instance)).to.equal(' ');
			_get__('expect')(transform('14-05-92', instance)).to.equal(' ');
			_get__('expect')(transform('1992-05-14', instance)).to.equal(' ');
			_get__('expect')(transform('1992/05/14', instance)).to.equal(' ');
			_get__('expect')(transform('92-05-14', instance)).to.equal(' ');
			_get__('expect')(transform('92/05/14', instance)).to.equal(' ');
		});

		it('Should remove latitude', function () {
			_get__('expect')(transform('-90.000000', instance)).to.equal(' ');
			_get__('expect')(transform('90.000000', instance)).to.equal(' ');
			_get__('expect')(transform('+64.1234115', instance)).to.equal(' ');
		});

		it('Should remove longitude', function () {
			_get__('expect')(transform('-180.000000', instance)).to.equal(' ');
			_get__('expect')(transform('180.000000', instance)).to.equal(' ');
			_get__('expect')(transform('90.1234115', instance)).to.equal(' ');
		});
	});

	describe('apply cleaning rules on', function () {
		var transform = _get__('transformations')[2];

		describe('point character', function () {

			it('Should replace N.[0-9] for N° [0-9]', function () {
				_get__('expect')(transform('N.1200', instance)).to.equal(' N° 1200');
				_get__('expect')(transform(' N.1200 ', instance)).to.equal(' N° 1200 ');
			});

			it('Should replace point character with hyphen when point is between a number and a letter', function () {
				_get__('expect')(transform('local 24.a', instance)).to.equal('local 24-a');
			});

			it('Should split words join by a point if subwords has length more than 2', function () {
				_get__('expect')(transform('vic.mackenna', instance)).to.equal('vic. mackenna');
				_get__('expect')(transform('rr.hh', instance)).to.equal('rr.hh');
			});

			it('Should replace point with hyphen when are between a letter and a number', function () {
				_get__('expect')(transform('depto C.2', instance)).to.equal('depto C-2');
				_get__('expect')(transform('depto A.301', instance)).to.equal('depto A-301');
			});

			it('Should add an space to the right of point when point is between a word and a number', function () {
				_get__('expect')(transform('km.24', instance)).to.equal('km. 24');
			});

			it('Should replace point at begining with an space', function () {
				_get__('expect')(transform('.asdasd', instance)).to.equal(' asdasd');
				_get__('expect')(transform('.asdasd.', instance)).to.equal(' asdasd.');
				_get__('expect')(transform(' .123', instance)).to.equal(' 123');
			});

			it('Should replace point with space when point is at the end of a number', function () {
				_get__('expect')(transform('1400.', instance)).to.equal('1400 ');
			});

			it('Should remove point if the point is at the end of the number and later came a comma', function () {
				_get__('expect')(transform('340.,', instance)).to.equal('340, ');
				_get__('expect')(transform(' 340., ', instance)).to.equal(' 340,  ');
			});

			it('Should delete numbers with more than one point between', function () {
				_get__('expect')(transform('11.11.1.1.1.1.1.', instance)).to.equal(' ');
				_get__('expect')(transform(' ....11.11.1.1.1.1.1. ', instance)).to.equal('   ');
				_get__('expect')(transform('11111111.1111', instance)).to.equal('11111111.1111');
			});

			it('Should split point from comma', function () {
				_get__('expect')(transform('jose p.p.,', instance)).to.equal('jose p.p. ,');
			});

			it('Should split word from number if it\'s join by a point', function () {
				_get__('expect')(transform('asd.123', instance)).to.equal('asd. 123');
				_get__('expect')(transform('123..asd', instance)).to.equal('123 asd');
			});

			it('Should replace point at the end of a number if a word does not follow with space', function () {
				_get__('expect')(transform('123.-', instance)).to.equal('123 -');
				_get__('expect')(transform('123. a', instance)).to.equal('123  a');
			});

			it('Should split av|avnda|avda|adva if its join to a word by a point', function () {
				_get__('expect')(transform('av.bla', instance)).to.equal(' av. bla');
				_get__('expect')(transform('avnda.bla', instance)).to.equal(' avnda. bla');
				_get__('expect')(transform('avda.bla', instance)).to.equal(' avda. bla');
				_get__('expect')(transform('adva.bla', instance)).to.equal(' adva. bla');
				_get__('expect')(transform('AV.bla', instance)).to.equal(' AV. bla');
				_get__('expect')(transform('AVNDA.bla', instance)).to.equal(' AVNDA. bla');
				_get__('expect')(transform('AVDA.bla', instance)).to.equal(' AVDA. bla');
				_get__('expect')(transform('ADVA.bla', instance)).to.equal(' ADVA. bla');
				_get__('expect')(transform(' AV.bla', instance)).to.equal(' AV. bla');
				_get__('expect')(transform(' AVNDA.bla', instance)).to.equal(' AVNDA. bla');
				_get__('expect')(transform(' AVDA.bla', instance)).to.equal(' AVDA. bla');
				_get__('expect')(transform(' ADVA.bla', instance)).to.equal(' ADVA. bla');
			});
		});

		describe('two points character', function () {
			var transform = _get__('transformations')[2];

			it('Should remove expresions like d:d', function () {
				_get__('expect')(transform('13:124', instance)).to.equal(' ');
			});

			it('Should replace : for - when join a letter with a number', function () {
				_get__('expect')(transform('a:124', instance)).to.equal(' a-124');
				_get__('expect')(transform('a-:124', instance)).to.equal(' a-124');
				_get__('expect')(transform('B:2', instance)).to.equal(' B-2');
			});

			it('Should replace : for - when join a number with a letter', function () {
				_get__('expect')(transform('124-:D', instance)).to.equal(' 124-D');
				_get__('expect')(transform('124:A', instance)).to.equal(' 124-A');
				_get__('expect')(transform('2:C', instance)).to.equal(' 2-C');
			});

			it('Should replace all : characters for space except those at the end of a word', function () {
				_get__('expect')(transform(':124', instance)).to.equal(' 124');
				_get__('expect')(transform('cel: 124', instance)).to.equal('cel:  124');
				_get__('expect')(transform('loc:12:of:33', instance)).to.equal('loc: 12 of: 33');
			});
		});

		describe('comma character', function () {
			var transform = _get__('transformations')[2];

			it('Should add an space to the right between a number and a word', function () {
				_get__('expect')(transform('1450,la florida', instance)).to.equal('1450, la florida');
			});

			it('Should remove comma between a word and a number', function () {
				_get__('expect')(transform('depto,401', instance)).to.equal('depto 401');
			});

			it('Should remove excesive space between a character and a comma', function () {
				_get__('expect')(transform('1450    ,la florida', instance)).to.equal('1450, la florida');
			});

			it('Should remove rest comma characters between spaces', function () {
				_get__('expect')(transform('1450    ,        ,     la florida', instance)).to.equal('1450, la florida');
			});

			it('Should separate words join by a comma adding an space to the right of comma', function () {
				_get__('expect')(transform('vic.mackenna,la florida', instance)).to.equal('vic. mackenna, la florida');
			});

			it('Should replace comma at the begining of an alphanumeric character with a space', function () {
				_get__('expect')(transform(',aaa', instance)).to.equal(' aaa');
				_get__('expect')(transform(',1000', instance)).to.equal(' 1000');
			});

			it('Should remove two commas consecutive', function () {
				_get__('expect')(transform('aaa,,', instance)).to.equal('aaa, ');
			});

			it('');
		});

		describe('hyphen character', function () {
			var transform = _get__('transformations')[2];

			it('Should replace replace hyphen between an L and a number for a point and a space to the right', function () {
				_get__('expect')(transform('L-10', instance)).to.equal(' L. 10');
				_get__('expect')(transform(' L-10 ', instance)).to.equal(' L. 10 ');
			});

			it('Should join a letter and a number with an hyphen in between separated by 1 or 2 spaces', function () {
				_get__('expect')(transform('A - 1', instance)).to.equal(' A-1 ');
				_get__('expect')(transform(' A  -  1 ', instance)).to.equal(' A-1  ');
				_get__('expect')(transform('A -  1', instance)).to.equal(' A-1 ');
				_get__('expect')(transform('A  - 1', instance)).to.equal(' A-1 ');
			});

			it('Should remove an hyphen in the begining of a word or number', function () {
				_get__('expect')(transform(' -1450', instance)).to.equal(' 1450');
				_get__('expect')(transform('-1450', instance)).to.equal(' 1450');
				_get__('expect')(transform(' -oficina', instance)).to.equal(' oficina');
				_get__('expect')(transform('-oficina', instance)).to.equal(' oficina');
			});

			it('Should remove an hyphen in the end of a word or number', function () {
				_get__('expect')(transform('1450-', instance)).to.equal('1450 ');
				_get__('expect')(transform('1450- ', instance)).to.equal('1450  ');
				_get__('expect')(transform('oficina- ', instance)).to.equal('oficina  ');
				_get__('expect')(transform('oficina-', instance)).to.equal('oficina ');
			});

			it('Should replace an hyphen between words with a space', function () {
				_get__('expect')(transform('los-retamos', instance)).to.equal('los retamos');
				_get__('expect')(transform(' los-retamos ', instance)).to.equal(' los retamos ');
			});

			it('Should replace an hyphen between a word of size at least of 2 and a number with a space', function () {
				_get__('expect')(transform('of-86', instance)).to.equal('of 86');
				_get__('expect')(transform(' calle-86 ', instance)).to.equal(' calle 86 ');
				_get__('expect')(transform('local a-20', instance)).to.equal('local a-20');
				_get__('expect')(transform(' local a-20 ', instance)).to.equal(' local a-20 ');
			});

			it('Should replace an hyphen between a number and a word of size at least of 2 with a space', function () {
				_get__('expect')(transform('1088-ñuñoa', instance)).to.equal('1088 ñuñoa');
				_get__('expect')(transform(' 1088-ñuñoa ', instance)).to.equal(' 1088 ñuñoa ');
				_get__('expect')(transform('km-68', instance)).to.equal('km 68');
				_get__('expect')(transform('of 42-a', instance)).to.equal('of 42-a');
				_get__('expect')(transform(' of 42-a ', instance)).to.equal(' of 42-a ');
			});
		});

		describe('slash and backslash characters', function () {
			var transform = _get__('transformations')[2];

			it('Should replace all slash and backslash characters for space', function () {
				_get__('expect')(transform('loc / 13 / of /c4', instance)).to.equal('loc   13   of  c4');
				_get__('expect')(transform('value/nombre', instance)).to.equal('value nombre');
				_get__('expect')(transform('\\13\\feb\\33', instance)).to.equal(' 13 feb 33');
			});

			it('Should let fractions intact ([0-9])/[0-9])', function () {
				_get__('expect')(transform('1/4', instance)).to.equal('1/4');
				_get__('expect')(transform('3\\5', instance)).to.equal('3 5');
			});

			it('Should not split S/N or N/A and any combination of upper and lower forms', function () {
				_get__('expect')(transform('S/N', instance)).to.equal(' S/N  ');
				_get__('expect')(transform('s/N', instance)).to.equal(' s/N  ');
				_get__('expect')(transform('S/n', instance)).to.equal(' S/n  ');
				_get__('expect')(transform('s/n', instance)).to.equal(' s/n  ');
				_get__('expect')(transform('N/A', instance)).to.equal(' N/A  ');
				_get__('expect')(transform('N/a', instance)).to.equal(' N/a  ');
				_get__('expect')(transform('n/A', instance)).to.equal(' n/A  ');
				_get__('expect')(transform('n/a', instance)).to.equal(' n/a  ');
				_get__('expect')(transform(' S   /  N ', instance)).to.equal(' S/N   ');
				_get__('expect')(transform(' s   / N ', instance)).to.equal(' s/N   ');
				_get__('expect')(transform(' S   /   n ', instance)).to.equal(' S/n   ');
				_get__('expect')(transform(' s  /  n ', instance)).to.equal(' s/n   ');
				_get__('expect')(transform(' N / A ', instance)).to.equal(' N/A   ');
				_get__('expect')(transform(' N / a ', instance)).to.equal(' N/a   ');
				_get__('expect')(transform(' n / A ', instance)).to.equal(' n/A   ');
				_get__('expect')(transform(' n   /   a ', instance)).to.equal(' n/a   ');
				_get__('expect')(transform(' S  /  N ', instance)).to.equal(' S/N   ');
				_get__('expect')(transform(' s  /   N ', instance)).to.equal(' s/N   ');
				_get__('expect')(transform(' S  / n ', instance)).to.equal(' S/n   ');
				_get__('expect')(transform(' s /   n ', instance)).to.equal(' s/n   ');
				_get__('expect')(transform(' N   / A ', instance)).to.equal(' N/A   ');
				_get__('expect')(transform(' N /  a ', instance)).to.equal(' N/a   ');
				_get__('expect')(transform(' n  /   A ', instance)).to.equal(' n/A   ');
				_get__('expect')(transform(' n / a ', instance)).to.equal(' n/a   ');
			});
		});

		describe('asterik character', function () {
			var transform = _get__('transformations')[2];

			it('Should replace n* or N* with N°', function () {
				_get__('expect')(transform('n*', instance)).to.equal('n ');
				_get__('expect')(transform('N*', instance)).to.equal('N ');
				_get__('expect')(transform('N* 1410', instance)).to.equal('N°  1410');
				_get__('expect')(transform('n*4567', instance)).to.equal('N° 4567');
			});

			it('Should replace all remaining * characters with space', function () {
				_get__('expect')(transform('loc ** 13*of*c4 ***', instance)).to.equal('loc    13 of c4    ');
				_get__('expect')(transform('value*nombre', instance)).to.equal('value nombre');
				_get__('expect')(transform('*urgente', instance)).to.equal(' urgente');
			});
		});

		describe('pad character', function () {
			var transform = _get__('transformations')[2];

			it('Should add an space before #d', function () {
				_get__('expect')(transform('number#45', instance)).to.equal('number #45');
			});

			it('Should replace all remaining # characters with space', function () {
				_get__('expect')(transform('A#', instance)).to.equal('A ');
				_get__('expect')(transform('#A', instance)).to.equal(' A');
				_get__('expect')(transform('1234#', instance)).to.equal('1234 ');
			});
		});
	});

	describe('alternative splitting characters', function () {
		var transform = _get__('transformations')[3];

		it('Should replace \\ for spaces', function () {
			_get__('expect')(transform('av\\josé\\miguel\\carrera', instance)).to.equal('av josé miguel carrera');
		});

		it('Should replace / for spaces', function () {
			_get__('expect')(transform('calle/providencia', instance)).to.equal('calle providencia');
		});

		it('Should replace - for spaces', function () {
			_get__('expect')(transform('psje-los-condores', instance)).to.equal('psje los condores');
		});

		it('Should replace _ for spaces', function () {
			_get__('expect')(transform('of_B_las_condes', instance)).to.equal('of B las condes');
		});

		it('Should not split S/N or N/A and any combination of upper and lower forms', function () {
			_get__('expect')(transform('S/N', instance)).to.equal('S/N');
			_get__('expect')(transform('s/N', instance)).to.equal('s/N');
			_get__('expect')(transform('S/n', instance)).to.equal('S/n');
			_get__('expect')(transform('s/n', instance)).to.equal('s/n');
			_get__('expect')(transform('N/A', instance)).to.equal('N/A');
			_get__('expect')(transform('N/a', instance)).to.equal('N/a');
			_get__('expect')(transform('n/A', instance)).to.equal('n/A');
			_get__('expect')(transform('n/a', instance)).to.equal('n/a');
			_get__('expect')(transform(' S/N ', instance)).to.equal(' S/N ');
			_get__('expect')(transform(' s/N ', instance)).to.equal(' s/N ');
			_get__('expect')(transform(' S/n ', instance)).to.equal(' S/n ');
			_get__('expect')(transform(' s/n ', instance)).to.equal(' s/n ');
			_get__('expect')(transform(' N/A ', instance)).to.equal(' N/A ');
			_get__('expect')(transform(' N/a ', instance)).to.equal(' N/a ');
			_get__('expect')(transform(' n/A ', instance)).to.equal(' n/A ');
			_get__('expect')(transform(' n/a ', instance)).to.equal(' n/a ');
		});
	});

	describe('alternative splitting algorithm', function () {
		var transform = _get__('transformations')[4];

		it('Should split (of|OF|Of)-[\w\d]', function () {
			_get__('expect')(transform('of-401', instance)).to.equal('of 401');
			_get__('expect')(transform(' OF-401 ', instance)).to.equal(' OF 401 ');
			_get__('expect')(transform('Of.-401', instance)).to.equal('Of. 401');
		});

		it('Should split if a lowercase is continued by a uppercase letter', function () {
			_get__('expect')(transform('granAvenida', instance)).to.equal('gran Avenida');
		});

		it('Should split if there an (-|_|\\|/) between 2 words of size 2 or greater', function () {
			_get__('expect')(transform('lo-herrera', instance)).to.equal('lo herrera');
			_get__('expect')(transform('lo_herrera', instance)).to.equal('lo herrera');
			_get__('expect')(transform('lo\\herrera', instance)).to.equal('lo herrera');
			_get__('expect')(transform('lo/herrera', instance)).to.equal('lo herrera');
			_get__('expect')(transform('a-b', instance)).to.equal('a-b');
		});

		it('Should split if word at least of size 3 is continued by a number', function () {
			_get__('expect')(transform('cel401', instance)).to.equal('cel 401');
			_get__('expect')(transform('cel-4011010', instance)).to.equal('cel 4011010');
			_get__('expect')(transform('a-2', instance)).to.equal('a-2');
		});

		it('Should split if a number is continued by a word of size greather or equal than 3', function () {
			_get__('expect')(transform('1400loprado', instance)).to.equal('1400 loprado');
			_get__('expect')(transform('1400_loprado', instance)).to.equal('1400 loprado');
			_get__('expect')(transform('13b', instance)).to.equal('13b');
		});

		it('Should split N°[0-9] using a space between', function () {
			_get__('expect')(transform('N°1000', instance)).to.equal(' N° 1000');
			_get__('expect')(transform(' N°1000 ', instance)).to.equal('  N° 1000 ');
			_get__('expect')(transform('ArturoN°1000 ', instance)).to.equal('Arturo N° 1000 ');
		});

		it('Should split KM[0-9] using a space between', function () {
			_get__('expect')(transform('KM68.5', instance)).to.equal(' KM 68.5');
			_get__('expect')(transform(' KM68.5 ', instance)).to.equal('  KM 68.5 ');
			_get__('expect')(transform('asdKM12', instance)).to.equal('asd KM 12');
		});

		it('Should split N(.)° de numeros y letras', function () {
			_get__('expect')(transform('NH°123', instance)).to.equal('NH° 123');
			_get__('expect')(transform('N°BCG', instance)).to.equal('N° BCG');
		});
	});

	describe('remove duplicate characters', function () {
		var transform = _get__('transformations')[5];

		it('Should compress a word of size equal or greater than 5 if has only one letter', function () {
			_get__('expect')(transform('AAAAA', instance)).to.equal('A');
		});

		it('Should not compress a word of size less than 5 if has only one letter', function () {
			_get__('expect')(transform('ss', instance)).to.equal('ss');
		});

		it('Should remove words with only duplicated special characters', function () {
			_get__('expect')(transform('____', instance)).to.equal('');
			_get__('expect')(transform('++++', instance)).to.equal('');
			_get__('expect')(transform('????', instance)).to.equal('');
			_get__('expect')(transform('------', instance)).to.equal('');
			_get__('expect')(transform('/////', instance)).to.equal('');
			_get__('expect')(transform('******', instance)).to.equal('');
		});

		it('Should compress duplicated characters on words', function () {
			_get__('expect')(transform('local---2', instance)).to.equal('local-2');
			_get__('expect')(transform('S//N', instance)).to.equal('S/N');
			_get__('expect')(transform('vic..mackenna', instance)).to.equal('vic.mackenna');
			_get__('expect')(transform('##123123', instance)).to.equal('#123123');
		});

		it('Should not remove acronyms', function () {
			_get__('expect')(transform('FFAA', instance)).to.equal('FFAA');
			_get__('expect')(transform('RRHH', instance)).to.equal('RRHH');
			_get__('expect')(transform('CCRRTT', instance)).to.equal('CCRRTT');
		});

		it('Should not remove duplicate numbers', function () {
			_get__('expect')(transform('222222', instance)).to.equal('222222');
		});

		it('Should remove duplicate spaces', function () {
			_get__('expect')(transform('     calle', instance)).to.equal('calle');
			_get__('expect')(transform('    calle    paul harris   ', instance)).to.equal('calle paul harris');
		});

		it('Should not modify string if don\'t have repeated characters', function () {
			_get__('expect')(transform('    av    grecia   ', instance)).to.equal('    av    grecia   ');
		});

		it('Should limit letter r only to rr', function () {
			_get__('expect')(transform('rrrrrrrrrotonda', instance)).to.equal('rrotonda');
		});

		it('Should limit letter l only to ll', function () {
			_get__('expect')(transform('lllllllanquihue', instance)).to.equal('llanquihue');
		});

		it('Should limit letter s only to ss', function () {
			_get__('expect')(transform('alesssandri', instance)).to.equal('alessandri');
		});

		it('Should limit letter c only to cc', function () {
			_get__('expect')(transform('roccco', instance)).to.equal('rocco');
		});

		it('Should limit letter g only to gg', function () {
			_get__('expect')(transform('ohiggggins', instance)).to.equal('ohiggins');
		});

		it('Should limit letter f only to ff', function () {
			_get__('expect')(transform('beauchefff', instance)).to.equal('beaucheff');
		});

		it('Should limit letter n only to nn', function () {
			_get__('expect')(transform('mackennna', instance)).to.equal('mackenna');
		});

		it('Should all replacements work on uppercase letters R,L,S,C,G,F', function () {
			_get__('expect')(transform('BEAUCHEFFF', instance)).to.equal('BEAUCHEFF');
			_get__('expect')(transform('OHIGGGINS', instance)).to.equal('OHIGGINS');
			_get__('expect')(transform('ROCCCCCO', instance)).to.equal('ROCCO');
			_get__('expect')(transform('ALESSSANDRI', instance)).to.equal('ALESSANDRI');
			_get__('expect')(transform('LLLANQUIHUE', instance)).to.equal('LLANQUIHUE');
			_get__('expect')(transform('RRRROTONDA', instance)).to.equal('RROTONDA');
			_get__('expect')(transform('MACKENNNA', instance)).to.equal('MACKENNA');
		});
	});

	it('Should remove remaining special characters', function () {
		var transform = _get__('transformations')[7];

		_get__('expect')(transform('av. americo vespucio , 481 . . san - joaquin chile', instance)).to.equal('av. americo vespucio 481 san joaquin chile');
		_get__('expect')(transform('av. quilin ## 1320 g a', instance)).to.equal('av. quilin 1320 g a');
		_get__('expect')(transform(' aasd - -- asda $ #1123', instance)).to.equal('aasd asda #1123');
		_get__('expect')(transform('ROMULO PE A 1321 (ESTADIO CANADELA ARICA', instance)).to.equal('ROMULO PE A 1321 ESTADIO CANADELA ARICA');
		_get__('expect')(transform('ROMULO PE A 1321 ESTADIO CANADELA ARICA) )', instance)).to.equal('ROMULO PE A 1321 ESTADIO CANADELA ARICA');
	});

	it('Should not remove join characters', function () {
		var transform = _get__('transformations')[7];

		_get__('expect')(transform('av. santa maria 1200 L 2B', instance)).to.equal('av. santa maria 1200 L 2B');
		_get__('expect')(transform('av. santa maria 1200 E 1B', instance)).to.equal('av. santa maria 1200 E 1B');
		_get__('expect')(transform('Psj 1 puesto 3 y 5', instance)).to.equal('Psj 1 puesto 3 y 5');
		_get__('expect')(transform('A salazar 15', instance)).to.equal('A salazar 15');
	});

	it('Should remove stopwords', function () {
		var transform = _get__('transformations')[8];

		_get__('expect')(transform('calle flores 1234, santiago, RM, Chile, entregar por psj rolando desde 10:00 hasta 13:00', instance)).to.equal('calle flores 1234, santiago, RM, Chile, entregar psj rolando 10:00 13:00');
	});

	it('Should remove excesive space characters between words (trim)', function () {
		var transform = _get__('transformations')[6];
		_get__('expect')(transform('     av vicuña        mackenna      3939     san    joaquín   chile', instance)).to.equal('av vicuña mackenna 3939 san joaquín chile');
	});

	describe('tokenizer algorithm', function () {
		it('Should filter words made only of special characters', function () {
			_get__('expect')(instance.algorithms.tokenize('#$"# por la calle', instance)).to.have.members(['por', 'la', 'calle']);
		});

		it('Should filter words of size 1 if they aren\'t include on instance.validSingleLetters', function () {
			// validJoinCharacters aeouy
			_get__('expect')(instance.algorithms.tokenize('calle t Ü garcía y ortega s á', instance)).to.have.members(['calle', 't', 'garcía', 'y', 'ortega', 's']);
		});

		it('Should let pass tokens of size 1 if they are numbers', function () {
			_get__('expect')(instance.algorithms.tokenize('psj 2', instance)).to.have.members(['psj', '2']);
		});
	});

	describe('latinize algorithm', function () {
		it('Should remove accent on letters', function () {
			_get__('expect')(instance.algorithms.latinize('ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ')).to.equal('AEIOUAEIOUUÑaeiouaeiouuñ');
		});
	});

	it('Should not remove N° expression after all transformations', function () {

		_get__('expect')(clean('Luis Pasteur n° 6495 despto. 2 6495 Vitacura, Chile', instance)).to.equal('Luis Pasteur n° 6495 despto. 2 6495 Vitacura, Chile');
		_get__('expect')(clean('CARDENAL CARO N° 021 MELIPILA, Chile', instance)).to.equal('CARDENAL CARO N° 021 MELIPILA, Chile');
		_get__('expect')(clean('AV. CIRCUNVALACION Nº 0467, PUENTE ALTO', instance)).to.equal('AV. CIRCUNVALACION N° 0467, PUENTE ALTO');
	});

	it('Should not split S/N expression and any combination of upper and lower s or n', function () {
		_get__('expect')(clean('EDUARDO RAGGIO S/N LOCAL 7\' \'CATEMU', instance)).to.equal('EDUARDO RAGGIO S/N LOCAL 7 CATEMU');
		_get__('expect')(clean('GERMAN RIESCO S/N\' \'CHANGO', instance)).to.equal('GERMAN RIESCO S/N CHANGO');
		_get__('expect')(clean('IGNACIO VALDEZ S/N CUNACO\' \'NANCAGUA', instance)).to.equal('IGNACIO VALDEZ S/N CUNACO NANCAGUA');
		_get__('expect')(clean('RASTROJOS PARADERO 6 S/N, SAN VICENTE, SAGBARA', instance)).to.equal('RASTROJOS PARADERO 6 S/N, SAN VICENTE, SAGBARA');
	});
});

function _getGlobalObject() {
	try {
		if (!!global) {
			return global;
		}
	} catch (e) {
		try {
			if (!!window) {
				return window;
			}
		} catch (e) {
			return this;
		}
	}
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
	if (_RewireModuleId__ === null) {
		var globalVariable = _getGlobalObject();

		if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
			globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
		}

		_RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
	}

	return _RewireModuleId__;
}

function _getRewireRegistry__() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
		theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
	}

	return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
	var moduleId = _getRewireModuleId__();

	var registry = _getRewireRegistry__();

	var rewireData = registry[moduleId];

	if (!rewireData) {
		registry[moduleId] = Object.create(null);
		rewireData = registry[moduleId];
	}

	return rewireData;
}

(function registerResetAll() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable['__rewire_reset_all__']) {
		theGlobalVariable['__rewire_reset_all__'] = function () {
			theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
		};
	}
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
	function addPropertyToAPIObject(name, value) {
		Object.defineProperty(_RewireAPI__, name, {
			value: value,
			enumerable: false,
			configurable: true
		});
	}

	addPropertyToAPIObject('__get__', _get__);
	addPropertyToAPIObject('__GetDependency__', _get__);
	addPropertyToAPIObject('__Rewire__', _set__);
	addPropertyToAPIObject('__set__', _set__);
	addPropertyToAPIObject('__reset__', _reset__);
	addPropertyToAPIObject('__ResetDependency__', _reset__);
	addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _get_original__(variableName);
	} else {
		var value = rewireData[variableName];

		if (value === INTENTIONAL_UNDEFINED) {
			return undefined;
		} else {
			return value;
		}
	}
}

function _get_original__(variableName) {
	switch (variableName) {
		case 'chai':
			return _chai2.default;

		case '_':
			return _lodash2.default;

		case 'config':
			return _config2.default;

		case 'transformations':
			return _config4.default;

		case 'sandbox':
			return sandbox;

		case 'sinon':
			return _sinon2.default;

		case 'expect':
			return expect;
	}

	return undefined;
}

function _assign__(variableName, value) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _set_original__(variableName, value);
	} else {
		return rewireData[variableName] = value;
	}
}

function _set_original__(variableName, _value) {
	switch (variableName) {
		case 'sandbox':
			return sandbox = _value;
	}

	return undefined;
}

function _update_operation__(operation, variableName, prefix) {
	var oldValue = _get__(variableName);

	var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

	_assign__(variableName, newValue);

	return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
	var rewireData = _getRewiredData__();

	if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
		Object.keys(variableName).forEach(function (name) {
			rewireData[name] = variableName[name];
		});
	} else {
		if (value === undefined) {
			rewireData[variableName] = INTENTIONAL_UNDEFINED;
		} else {
			rewireData[variableName] = value;
		}

		return function () {
			_reset__(variableName);
		};
	}
}

function _reset__(variableName) {
	var rewireData = _getRewiredData__();

	delete rewireData[variableName];

	if (Object.keys(rewireData).length == 0) {
		delete _getRewireRegistry__()[_getRewireModuleId__];
	}

	;
}

function _with__(object) {
	var rewireData = _getRewiredData__();

	var rewiredVariableNames = Object.keys(object);
	var previousValues = {};

	function reset() {
		rewiredVariableNames.forEach(function (variableName) {
			rewireData[variableName] = previousValues[variableName];
		});
	}

	return function (callback) {
		rewiredVariableNames.forEach(function (variableName) {
			previousValues[variableName] = rewireData[variableName];
			rewireData[variableName] = object[variableName];
		});
		var result = callback();

		if (!!result && typeof result.then == 'function') {
			result.then(reset).catch(reset);
		} else {
			reset();
		}

		return result;
	};
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;
exports.default = _RewireAPI__;

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.__RewireAPI__ = exports.__ResetDependency__ = exports.__set__ = exports.__Rewire__ = exports.__GetDependency__ = exports.__get__ = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /* jshint expr: true */


var _chai = __webpack_require__(0);

var _chai2 = _interopRequireDefault(_chai);

var _sinon = __webpack_require__(2);

var _sinon2 = _interopRequireDefault(_sinon);

var _chaiAsPromised = __webpack_require__(11);

var _chaiAsPromised2 = _interopRequireDefault(_chaiAsPromised);

var _cleaner = __webpack_require__(12);

var _cleaner2 = _interopRequireDefault(_cleaner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_get__('chai').use(_get__('chaiAsPromised'));

var expect = _get__('chai').expect;
var sandbox = null;

_get__('chai').should();

describe('Cleaner', function () {
	var countries = {
		getAlpha2Codes: function getAlpha2Codes() {}
	};
	var globalTransformations = [];

	beforeEach(function () {
		_assign__('sandbox', _get__('sinon').createSandbox());
		_get__('sandbox').stub(countries, 'getAlpha2Codes');
		_get__('CleanerRewireAPI').__Rewire__('countries', countries);
		_get__('CleanerRewireAPI').__Rewire__('defaultTransformations', globalTransformations);
	});

	afterEach(function () {
		_get__('sandbox').restore();
		_get__('CleanerRewireAPI').__ResetDependency__('countries', countries);
		_get__('CleanerRewireAPI').__ResetDependency__('defaultTransformations', globalTransformations);
	});

	describe('when validate countryCode', function () {

		it('Should throw an error if countryCode is missing', function (done) {
			var cleaner = new (_get__('Cleaner'))();
			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('Country code is required');
			}).should.notify(done);
		});

		it('Should throw an error if countryCode isn\'t a valid ISO-3166 alpha 2', function (done) {
			countries.getAlpha2Codes.onFirstCall().returns({});

			var cleaner = new (_get__('Cleaner'))('chile');

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('Country code must be a valid ISO 3166-1 alpha 2');
			}).should.notify(done);
		});

		it('Should throw an error if countryCode isn\'t an available countryCode', function (done) {
			var AVAILABLE_COUNTRIES = ['br'];
			_get__('CleanerRewireAPI').__Rewire__('AVAILABLE_COUNTRIES', AVAILABLE_COUNTRIES);

			countries.getAlpha2Codes.onFirstCall().returns({ 'CL': 'cl' });

			var cleaner = new (_get__('Cleaner'))('cl');

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('Country code must be a valid ISO 3166-1 alpha 2');
				_get__('CleanerRewireAPI').__ResetDependency__('AVAILABLE_COUNTRIES', AVAILABLE_COUNTRIES);
			}).should.notify(done);
		});
	});

	describe('when validate optionalPath', function () {
		beforeEach(function () {
			countries.getAlpha2Codes.onFirstCall().returns({ 'CL': 'cl' });
		});

		it('Should throw an error if optionalPath is defined and isn\'t a string', function (done) {

			var cleaner = new (_get__('Cleaner'))('cl', 1234);

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('optional path must be a relative or absolute system path');
			}).should.notify(done);
		});

		it('Should throw an error if optionalPath is defined and isn\'t a valid path', function (done) {

			var cleaner = new (_get__('Cleaner'))('cl', '$&/$&%#&/#%/');

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('optional path must be a relative or absolute system path');
			}).should.notify(done);
		});
	});

	describe('with valid arguments', function () {
		var validCountryCode = 'cl';
		var originalConfig = null;

		beforeEach(function () {
			countries.getAlpha2Codes.onFirstCall().returns({ 'CL': 'cl' });
			originalConfig = _get__('Cleaner').configs;
		});

		afterEach(function () {
			_get__('Cleaner').configs = originalConfig;
		});

		it('Should throw an error if country submodule is missing', function (done) {

			_get__('Cleaner').configs = {};

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('configuration module for country [' + validCountryCode + '] not found');
			}).should.notify(done);
		});

		it('Should set <instance> property as promise when finish instantiation', function (done) {
			_get__('Cleaner').configs = {};

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			_get__('expect')(cleaner.$promise).to.be.a('promise');
			cleaner.$promise.should.be.rejected.then(function () {}).should.notify(done);
		});

		it('Should merge local configuration if exist into the instance', function (done) {
			_get__('Cleaner').configs = {};
			_get__('Cleaner').configs[validCountryCode] = {
				countryName: 'Chile',
				overrideDefaultConfiguration: false,
				stopwords: ['ante'],
				keepCharacters: '=&/'
			};

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(function () {
				_get__('expect')(cleaner).to.have.property('countryName', 'Chile');
				_get__('expect')(cleaner).to.have.property('overrideDefaultConfiguration', false);
				_get__('expect')(cleaner).to.have.property('stopwords');
				_get__('expect')(cleaner.stopwords).to.be.an('array').that.includes('ante');
				_get__('expect')(cleaner).to.have.property('keepCharacters', '=&/');
			}).should.notify(done);
		});

		it('Should set the instance transformations property with a copy of global transformations by default', function (done) {
			var globalTransformations = ['global'];
			var localTransformations = ['local'];

			_get__('Cleaner').configs = {};
			_get__('Cleaner').configs[validCountryCode] = {
				overrideDefaultConfiguration: false,
				stopwords: ['test'],
				transformations: localTransformations
			};

			_get__('CleanerRewireAPI').__Rewire__('defaultTransformations', globalTransformations);

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(function () {
				_get__('expect')(cleaner.transformations).to.be.an('array').and.to.have.lengthOf(1);
				_get__('expect')(cleaner.transformations).to.not.include(localTransformations[0]);
				_get__('expect')(cleaner.transformations).to.include(globalTransformations[0]);
				_get__('CleanerRewireAPI').__ResetDependency__('defaultTransformations', globalTransformations);
			}).should.notify(done);
		});

		it('Should set local transformations if option overrideDefaultConfiguration is true', function (done) {
			var globalTransformations = ['global'];
			var localTransformations = ['local'];

			_get__('Cleaner').configs = {};
			_get__('Cleaner').configs[validCountryCode] = {
				overrideDefaultConfiguration: true,
				stopwords: ['test'],
				transformations: localTransformations
			};

			_get__('CleanerRewireAPI').__Rewire__('defaultTransformations', globalTransformations);

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			cleaner.$promise.should.be.fulfilled.then(function () {
				_get__('expect')(cleaner.transformations).to.be.an('array').and.to.have.lengthOf(1);
				_get__('expect')(cleaner.transformations).to.not.include(globalTransformations[0]);
				_get__('expect')(cleaner.transformations).to.include(localTransformations[0]);
				_get__('CleanerRewireAPI').__ResetDependency__('defaultTransformations', globalTransformations);
			}).should.notify(done);
		});

		it('Should throw an error is overrideDefaultConfiguration is true and local transformations are empty', function (done) {
			_get__('Cleaner').configs = {};
			_get__('Cleaner').configs[validCountryCode] = {
				overrideDefaultConfiguration: true,
				stopwords: ['test'],
				transformations: []
			};

			var cleaner = new (_get__('Cleaner'))(validCountryCode);

			cleaner.$promise.should.be.rejected.then(function (err) {
				_get__('expect')(err).to.be.an('error');
				_get__('expect')(err.message).to.equal('transformations array is missing from local configuration');
			}).should.notify(done);
		});

		describe('loading stopwords', function () {
			var fsMock = {
				readFile: function readFile() {}
			};

			beforeEach(function () {
				_get__('sandbox').stub(fsMock, 'readFile');
				_get__('CleanerRewireAPI').__Rewire__('fs', fsMock);
			});

			afterEach(function () {
				_get__('CleanerRewireAPI').__ResetDependency__('fs', fsMock);
			});

			it('Should load stopwords from file using default path', function (done) {
				var content = {
					toString: function toString() {
						return 'ante\npara\npor';
					}
				};
				var expectedStopwords = ['ante', 'para', 'por'];
				var callback = function callback(path, options, cb) {
					cb(null, content);
				};

				var spy = _get__('sandbox').spy(callback);

				_get__('Cleaner').configs = {};
				_get__('Cleaner').configs[validCountryCode] = {
					overrideDefaultConfiguration: false
				};

				fsMock.readFile.callsFake(spy);

				var cleaner = new (_get__('Cleaner'))(validCountryCode);

				cleaner.$promise.should.be.fulfilled.then(function () {
					_get__('expect')(spy.calledWithMatch('/' + validCountryCode + '/resources/stopwords.txt', { encoding: 'utf8' })).to.be.true;
					_get__('expect')(cleaner.stopwords).to.be.an('array').and.to.have.lengthOf(3);
					_get__('expect')(cleaner.stopwords).to.have.members(expectedStopwords);
					_get__('expect')(_get__('Cleaner').configs[validCountryCode].stopwords).to.be.an('array').and.to.have.lengthOf(3);
					_get__('expect')(_get__('Cleaner').configs[validCountryCode].stopwords).to.have.members(expectedStopwords);
				}).should.notify(done);
			});

			it('Should load stopwords from file using optionalPath when it\'s provided', function (done) {
				var optionalFolder = 'assets';
				var optionalPath = './' + optionalFolder;
				var content = {
					toString: function toString() {
						return 'ante\npara\npor';
					}
				};
				var expectedStopwords = ['ante', 'para', 'por'];
				var callback = function callback(path, options, cb) {
					cb(null, content);
				};

				var spy = _get__('sandbox').spy(callback);

				_get__('Cleaner').configs = {};
				_get__('Cleaner').configs[validCountryCode] = {
					overrideDefaultConfiguration: false
				};

				fsMock.readFile.callsFake(spy);

				var cleaner = new (_get__('Cleaner'))(validCountryCode, optionalPath);

				cleaner.$promise.should.be.fulfilled.then(function () {
					_get__('expect')(spy.calledWithMatch('/' + optionalFolder + '/' + validCountryCode + '/resources/stopwords.txt', { encoding: 'utf8' })).to.be.true;
					_get__('expect')(cleaner.stopwords).to.be.an('array').and.to.have.lengthOf(3);
					_get__('expect')(cleaner.stopwords).to.have.members(expectedStopwords);
					_get__('expect')(_get__('Cleaner').configs[validCountryCode].stopwords).to.be.an('array').and.to.have.lengthOf(3);
					_get__('expect')(_get__('Cleaner').configs[validCountryCode].stopwords).to.have.members(expectedStopwords);
				}).should.notify(done);
			});

			it('Should reject instance if can\'t load stopwords', function (done) {
				var error = new Error('can\'t open file');
				var callback = function callback(path, options, cb) {
					cb(error, null);
				};

				_get__('Cleaner').configs = {};
				_get__('Cleaner').configs[validCountryCode] = {
					overrideDefaultConfiguration: false
				};

				fsMock.readFile.callsFake(callback);

				var cleaner = new (_get__('Cleaner'))(validCountryCode);

				cleaner.$promise.should.be.rejected.then(function (err) {
					_get__('expect')(err).to.be.an('error');
					_get__('expect')(err.message).to.include(error.message);
				}).should.notify(done);
			});
		});
	});

	describe('Clean method', function () {
		var cleaner = void 0;
		var originalConfig = void 0;

		beforeEach(function () {
			countries.getAlpha2Codes.returns({ 'CL': 'cl' });
			originalConfig = _get__('Cleaner').configs;
			cleaner = new (_get__('Cleaner'))('cl');

			_get__('Cleaner').configs = {
				'cl': {
					overrideDefaultConfiguration: false,
					stopwords: ['ante']
				}
			};
		});

		afterEach(function () {
			_get__('Cleaner').configs = originalConfig;
		});

		it('Should return a promise when the clean method is called', function () {
			var promise = cleaner.clean('Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba');

			_get__('expect')(promise).to.be.a('promise');
		});

		it('Should start the cleaning process when $promise property are fulfilled', function (done) {
			var dummy = function dummy(a) {
				return a;
			};
			var spy = _get__('sandbox').spy(dummy);
			var mockTransformations = [spy];
			var originalTransformations = cleaner.transformations;

			cleaner.transformations = mockTransformations;

			var promise = cleaner.clean('Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba');

			promise.should.be.fulfilled.then(function () {
				_get__('expect')(spy.calledOnce).to.be.true;
				cleaner.transformations = originalTransformations;
			}).should.notify(done);
		});

		it('Should iterate over every transformation and pass the arguments address and instance', function (done) {
			var dummyA = function dummyA(a) {
				return a;
			};
			var dummyB = function dummyB(b) {
				return b;
			};
			var spyA = _get__('sandbox').spy(dummyA);
			var spyB = _get__('sandbox').spy(dummyB);
			var mockTransformations = [spyA, spyB];
			var address = 'Av. A. Vespucio Nº 1737 Local 1012-1017 Huechuraba';
			var originalTransformations = cleaner.transformations;

			cleaner.transformations = mockTransformations;

			var promise = cleaner.clean(address);

			promise.should.be.fulfilled.then(function () {
				_get__('expect')(spyA.calledOnce).to.be.true;
				_get__('expect')(spyB.calledOnce).to.be.true;
				_get__('expect')(spyA.calledWith(address, cleaner)).to.be.true;
				_get__('expect')(spyB.calledWith(address, cleaner)).to.be.true;
				cleaner.transformations = originalTransformations;
			}).should.notify(done);
		});

		it('Should return a string if the promise returned are fulfilled', function (done) {

			var promise = cleaner.clean('address');

			promise.should.be.fulfilled.then(function (newAddress) {
				_get__('expect')(newAddress).to.be.an('string');
			}).should.notify(done);
		});

		it('Should return the original address if the promise returned are rejected', function (done) {
			var originalPromise = cleaner.$promise;
			cleaner.$promise = Promise.reject(new Error('invalid'));
			var promise = cleaner.clean('address');

			promise.should.be.fulfilled.then(function (originalAddress) {
				_get__('expect')(originalAddress).to.equal('address');
				cleaner.$promise = originalPromise;
			}).should.notify(done);
		});
	});
});

function _getGlobalObject() {
	try {
		if (!!global) {
			return global;
		}
	} catch (e) {
		try {
			if (!!window) {
				return window;
			}
		} catch (e) {
			return this;
		}
	}
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
	if (_RewireModuleId__ === null) {
		var globalVariable = _getGlobalObject();

		if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
			globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
		}

		_RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
	}

	return _RewireModuleId__;
}

function _getRewireRegistry__() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
		theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
	}

	return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
	var moduleId = _getRewireModuleId__();

	var registry = _getRewireRegistry__();

	var rewireData = registry[moduleId];

	if (!rewireData) {
		registry[moduleId] = Object.create(null);
		rewireData = registry[moduleId];
	}

	return rewireData;
}

(function registerResetAll() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable['__rewire_reset_all__']) {
		theGlobalVariable['__rewire_reset_all__'] = function () {
			theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
		};
	}
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
	function addPropertyToAPIObject(name, value) {
		Object.defineProperty(_RewireAPI__, name, {
			value: value,
			enumerable: false,
			configurable: true
		});
	}

	addPropertyToAPIObject('__get__', _get__);
	addPropertyToAPIObject('__GetDependency__', _get__);
	addPropertyToAPIObject('__Rewire__', _set__);
	addPropertyToAPIObject('__set__', _set__);
	addPropertyToAPIObject('__reset__', _reset__);
	addPropertyToAPIObject('__ResetDependency__', _reset__);
	addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _get_original__(variableName);
	} else {
		var value = rewireData[variableName];

		if (value === INTENTIONAL_UNDEFINED) {
			return undefined;
		} else {
			return value;
		}
	}
}

function _get_original__(variableName) {
	switch (variableName) {
		case 'chai':
			return _chai2.default;

		case 'chaiAsPromised':
			return _chaiAsPromised2.default;

		case 'sandbox':
			return sandbox;

		case 'sinon':
			return _sinon2.default;

		case 'CleanerRewireAPI':
			return _cleaner.__RewireAPI__;

		case 'Cleaner':
			return _cleaner2.default;

		case 'expect':
			return expect;
	}

	return undefined;
}

function _assign__(variableName, value) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _set_original__(variableName, value);
	} else {
		return rewireData[variableName] = value;
	}
}

function _set_original__(variableName, _value) {
	switch (variableName) {
		case 'sandbox':
			return sandbox = _value;
	}

	return undefined;
}

function _update_operation__(operation, variableName, prefix) {
	var oldValue = _get__(variableName);

	var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

	_assign__(variableName, newValue);

	return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
	var rewireData = _getRewiredData__();

	if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
		Object.keys(variableName).forEach(function (name) {
			rewireData[name] = variableName[name];
		});
	} else {
		if (value === undefined) {
			rewireData[variableName] = INTENTIONAL_UNDEFINED;
		} else {
			rewireData[variableName] = value;
		}

		return function () {
			_reset__(variableName);
		};
	}
}

function _reset__(variableName) {
	var rewireData = _getRewiredData__();

	delete rewireData[variableName];

	if (Object.keys(rewireData).length == 0) {
		delete _getRewireRegistry__()[_getRewireModuleId__];
	}

	;
}

function _with__(object) {
	var rewireData = _getRewiredData__();

	var rewiredVariableNames = Object.keys(object);
	var previousValues = {};

	function reset() {
		rewiredVariableNames.forEach(function (variableName) {
			rewireData[variableName] = previousValues[variableName];
		});
	}

	return function (callback) {
		rewiredVariableNames.forEach(function (variableName) {
			previousValues[variableName] = rewireData[variableName];
			rewireData[variableName] = object[variableName];
		});
		var result = callback();

		if (!!result && typeof result.then == 'function') {
			result.then(reset).catch(reset);
		} else {
			reset();
		}

		return result;
	};
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;
exports.default = _RewireAPI__;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("chai-as-promised");

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.__RewireAPI__ = exports.__ResetDependency__ = exports.__set__ = exports.__Rewire__ = exports.__GetDependency__ = exports.__get__ = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = __webpack_require__(13);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(14);

var _path2 = _interopRequireDefault(_path);

var _lodash = __webpack_require__(3);

var _lodash2 = _interopRequireDefault(_lodash);

var _i18nIsoCountries = __webpack_require__(15);

var _i18nIsoCountries2 = _interopRequireDefault(_i18nIsoCountries);

var _config = __webpack_require__(1);

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CleanerError = function (_Error) {
    _inherits(CleanerError, _Error);

    function CleanerError() {
        _classCallCheck(this, CleanerError);

        return _possibleConstructorReturn(this, (CleanerError.__proto__ || Object.getPrototypeOf(CleanerError)).apply(this, arguments));
    }

    return CleanerError;
}(Error);

var CleanerInvalidParameter = function (_Error2) {
    _inherits(CleanerInvalidParameter, _Error2);

    function CleanerInvalidParameter() {
        _classCallCheck(this, CleanerInvalidParameter);

        return _possibleConstructorReturn(this, (CleanerInvalidParameter.__proto__ || Object.getPrototypeOf(CleanerInvalidParameter)).apply(this, arguments));
    }

    return CleanerInvalidParameter;
}(Error);

var CleanerMissingValue = function (_Error3) {
    _inherits(CleanerMissingValue, _Error3);

    function CleanerMissingValue() {
        _classCallCheck(this, CleanerMissingValue);

        return _possibleConstructorReturn(this, (CleanerMissingValue.__proto__ || Object.getPrototypeOf(CleanerMissingValue)).apply(this, arguments));
    }

    return CleanerMissingValue;
}(Error);

var RESOURCES_DIR = 'resources';
var STOPWORDS_FILENAME = 'stopwords.txt';
var AVAILABLE_COUNTRIES = ['cl'];

var Cleaner = function () {
    function Cleaner(countryCode) {
        var _this4 = this;

        var optionalPath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        _classCallCheck(this, Cleaner);

        var cp = void 0;
        var cn = void 0;

        this.$promise = new Promise(function (resolve, reject) {
            cp = resolve;
            cn = reject;
        });

        try {
            // Validate countryCode and optionalPath

            _get__('validateCountryCode')(countryCode);
            _get__('validateOptionalPath')(optionalPath);

            countryCode = countryCode.toLowerCase();

            // Check if config was previosly loaded

            if (this.constructor.configs.hasOwnProperty(countryCode) && !_get__('_').isEmpty(this.constructor.configs[countryCode])) {

                _get__('_').merge(this, this.constructor.configs[countryCode]);

                // Set transformations
                this.overrideDefaultConfiguration = !!this.overrideDefaultConfiguration;

                if (!this.overrideDefaultConfiguration) {
                    this.transformations = _get__('_').clone(_get__('defaultTransformations'));
                } else if (this.overrideDefaultConfiguration && _get__('_').isEmpty(this.transformations)) {
                    throw new (_get__('CleanerMissingValue'))('transformations array is missing ' + 'from local configuration');
                }

                // Set stopwords

                if (_get__('_').isEmpty(this.stopwords)) {
                    var basePath = optionalPath ? optionalPath : '.';
                    var filePath = basePath + '/' + countryCode + '/' + _get__('RESOURCES_DIR') + '/' + _get__('STOPWORDS_FILENAME');
                    _get__('fs').readFile(_get__('path').join(__dirname, filePath), { encoding: 'utf8' }, function (err, content) {
                        if (err) {
                            throw new (_get__('CleanerError'))(err);
                        }

                        _this4.stopwords = content.toString().split('\n');
                        _this4.constructor.configs[countryCode].stopwords = _get__('_').clone(_this4.stopwords);
                        cp(_this4);
                    });
                } else {
                    cp(this);
                }
            } else {
                throw new (_get__('CleanerError'))('configuration module for country [' + countryCode + '] not found');
            }
        } catch (e) {
            cn(e);
        }
    }

    _createClass(Cleaner, [{
        key: 'clean',
        value: function clean(address) {
            var _this5 = this;

            var newAddress = address;
            return this.$promise.then(function () {
                _this5.transformations.forEach(function (transformation) {
                    newAddress = transformation(newAddress, _this5);
                });
                return newAddress;
            }).catch(function () {
                return newAddress;
            });
        }
    }]);

    return Cleaner;
}();

_get__('Cleaner').globalTransformations = _get__('defaultTransformations');
_get__('Cleaner').configs = _get__('localConfigs') || {};
_get__('Cleaner').errors = {
    'UNKNOWN': _get__('CleanerError'),
    'INVALID_PARAMETER': _get__('CleanerInvalidParameter'),
    'MISSING_VALUE': _get__('CleanerMissingValue')
};

exports.default = _get__('Cleaner');

///////////////////////////////////////////////////////
// Dynamic Validation
//////////////////////////////////////////////////////


function validateCountryCode(code) {
    if (code) {
        if (!(_get__('countries').getAlpha2Codes().hasOwnProperty(code.toUpperCase()) && _get__('AVAILABLE_COUNTRIES').find(function (c) {
            return c === code.toLowerCase();
        }))) {
            throw new (_get__('CleanerInvalidParameter'))('Country code must be a valid ISO 3166-1 alpha 2');
        }
    } else {
        throw new (_get__('CleanerMissingValue'))('Country code is required');
    }
}

function validateOptionalPath(optionalPath) {
    if (optionalPath && (typeof optionalPath !== 'string' || !/^(\.{1,2}\/|\/)?((\w+\/)+)?\w+(\.js)?$/.test(optionalPath))) {
        throw new (_get__('CleanerInvalidParameter'))('optional path must be a relative or absolute system path');
    }
}

// TODO: VALIDATE TYPE OF ALL CONFIGS ATTRIBUTES

function _getGlobalObject() {
    try {
        if (!!global) {
            return global;
        }
    } catch (e) {
        try {
            if (!!window) {
                return window;
            }
        } catch (e) {
            return this;
        }
    }
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
    if (_RewireModuleId__ === null) {
        var globalVariable = _getGlobalObject();

        if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
            globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
        }

        _RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
    }

    return _RewireModuleId__;
}

function _getRewireRegistry__() {
    var theGlobalVariable = _getGlobalObject();

    if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
        theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
    }

    return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
    var moduleId = _getRewireModuleId__();

    var registry = _getRewireRegistry__();

    var rewireData = registry[moduleId];

    if (!rewireData) {
        registry[moduleId] = Object.create(null);
        rewireData = registry[moduleId];
    }

    return rewireData;
}

(function registerResetAll() {
    var theGlobalVariable = _getGlobalObject();

    if (!theGlobalVariable['__rewire_reset_all__']) {
        theGlobalVariable['__rewire_reset_all__'] = function () {
            theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
        };
    }
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
    function addPropertyToAPIObject(name, value) {
        Object.defineProperty(_RewireAPI__, name, {
            value: value,
            enumerable: false,
            configurable: true
        });
    }

    addPropertyToAPIObject('__get__', _get__);
    addPropertyToAPIObject('__GetDependency__', _get__);
    addPropertyToAPIObject('__Rewire__', _set__);
    addPropertyToAPIObject('__set__', _set__);
    addPropertyToAPIObject('__reset__', _reset__);
    addPropertyToAPIObject('__ResetDependency__', _reset__);
    addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
    var rewireData = _getRewiredData__();

    if (rewireData[variableName] === undefined) {
        return _get_original__(variableName);
    } else {
        var value = rewireData[variableName];

        if (value === INTENTIONAL_UNDEFINED) {
            return undefined;
        } else {
            return value;
        }
    }
}

function _get_original__(variableName) {
    switch (variableName) {
        case 'validateCountryCode':
            return validateCountryCode;

        case 'validateOptionalPath':
            return validateOptionalPath;

        case '_':
            return _lodash2.default;

        case 'defaultTransformations':
            return _config2.default;

        case 'CleanerMissingValue':
            return CleanerMissingValue;

        case 'RESOURCES_DIR':
            return RESOURCES_DIR;

        case 'STOPWORDS_FILENAME':
            return STOPWORDS_FILENAME;

        case 'fs':
            return _fs2.default;

        case 'path':
            return _path2.default;

        case 'CleanerError':
            return CleanerError;

        case 'Cleaner':
            return Cleaner;

        case 'localConfigs':
            return _config.localConfigs;

        case 'CleanerInvalidParameter':
            return CleanerInvalidParameter;

        case 'countries':
            return _i18nIsoCountries2.default;

        case 'AVAILABLE_COUNTRIES':
            return AVAILABLE_COUNTRIES;
    }

    return undefined;
}

function _assign__(variableName, value) {
    var rewireData = _getRewiredData__();

    if (rewireData[variableName] === undefined) {
        return _set_original__(variableName, value);
    } else {
        return rewireData[variableName] = value;
    }
}

function _set_original__(variableName, _value) {
    switch (variableName) {}

    return undefined;
}

function _update_operation__(operation, variableName, prefix) {
    var oldValue = _get__(variableName);

    var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

    _assign__(variableName, newValue);

    return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
    var rewireData = _getRewiredData__();

    if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
        Object.keys(variableName).forEach(function (name) {
            rewireData[name] = variableName[name];
        });
    } else {
        if (value === undefined) {
            rewireData[variableName] = INTENTIONAL_UNDEFINED;
        } else {
            rewireData[variableName] = value;
        }

        return function () {
            _reset__(variableName);
        };
    }
}

function _reset__(variableName) {
    var rewireData = _getRewiredData__();

    delete rewireData[variableName];

    if (Object.keys(rewireData).length == 0) {
        delete _getRewireRegistry__()[_getRewireModuleId__];
    }

    ;
}

function _with__(object) {
    var rewireData = _getRewiredData__();

    var rewiredVariableNames = Object.keys(object);
    var previousValues = {};

    function reset() {
        rewiredVariableNames.forEach(function (variableName) {
            rewireData[variableName] = previousValues[variableName];
        });
    }

    return function (callback) {
        rewiredVariableNames.forEach(function (variableName) {
            previousValues[variableName] = rewireData[variableName];
            rewireData[variableName] = object[variableName];
        });
        var result = callback();

        if (!!result && typeof result.then == 'function') {
            result.then(reset).catch(reset);
        } else {
            reset();
        }

        return result;
    };
}

var _typeOfOriginalExport = typeof Cleaner === 'undefined' ? 'undefined' : _typeof(Cleaner);

function addNonEnumerableProperty(name, value) {
    Object.defineProperty(Cleaner, name, {
        value: value,
        enumerable: false,
        configurable: true
    });
}

if ((_typeOfOriginalExport === 'object' || _typeOfOriginalExport === 'function') && Object.isExtensible(Cleaner)) {
    addNonEnumerableProperty('__get__', _get__);
    addNonEnumerableProperty('__GetDependency__', _get__);
    addNonEnumerableProperty('__Rewire__', _set__);
    addNonEnumerableProperty('__set__', _set__);
    addNonEnumerableProperty('__reset__', _reset__);
    addNonEnumerableProperty('__ResetDependency__', _reset__);
    addNonEnumerableProperty('__with__', _with__);
    addNonEnumerableProperty('__RewireAPI__', _RewireAPI__);
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;
/* WEBPACK VAR INJECTION */}.call(exports, "cleaner"))

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 14 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("i18n-iso-countries");

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.__RewireAPI__ = exports.__ResetDependency__ = exports.__set__ = exports.__Rewire__ = exports.__GetDependency__ = exports.__get__ = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; //jshint expr:true


var _chai = __webpack_require__(0);

var _chai2 = _interopRequireDefault(_chai);

var _config = __webpack_require__(1);

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var expect = _get__('chai').expect;

describe('Cleaner - General Configuration: ', function () {
	var mockInstance = {};
	var address = '';

	it('Should transformations be an array not empty', function () {
		_get__('expect')(_get__('transformations')).to.be.an('array');
		_get__('expect')(_get__('transformations')).to.not.be.empty;
	});

	it('Should all transformations must return a string if receive a string as address', function () {
		_get__('transformations').forEach(function (f) {
			_get__('expect')(f(address, mockInstance)).to.be.a('string');
		});
	});

	it('Should all transformations must return a null if receive a null as address', function () {
		_get__('transformations').forEach(function (f) {
			_get__('expect')(f(null, mockInstance)).to.be.a('null');
		});
	});
});

function _getGlobalObject() {
	try {
		if (!!global) {
			return global;
		}
	} catch (e) {
		try {
			if (!!window) {
				return window;
			}
		} catch (e) {
			return this;
		}
	}
}

;
var _RewireModuleId__ = null;

function _getRewireModuleId__() {
	if (_RewireModuleId__ === null) {
		var globalVariable = _getGlobalObject();

		if (!globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__) {
			globalVariable.__$$GLOBAL_REWIRE_NEXT_MODULE_ID__ = 0;
		}

		_RewireModuleId__ = __$$GLOBAL_REWIRE_NEXT_MODULE_ID__++;
	}

	return _RewireModuleId__;
}

function _getRewireRegistry__() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__) {
		theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
	}

	return __$$GLOBAL_REWIRE_REGISTRY__;
}

function _getRewiredData__() {
	var moduleId = _getRewireModuleId__();

	var registry = _getRewireRegistry__();

	var rewireData = registry[moduleId];

	if (!rewireData) {
		registry[moduleId] = Object.create(null);
		rewireData = registry[moduleId];
	}

	return rewireData;
}

(function registerResetAll() {
	var theGlobalVariable = _getGlobalObject();

	if (!theGlobalVariable['__rewire_reset_all__']) {
		theGlobalVariable['__rewire_reset_all__'] = function () {
			theGlobalVariable.__$$GLOBAL_REWIRE_REGISTRY__ = Object.create(null);
		};
	}
})();

var INTENTIONAL_UNDEFINED = '__INTENTIONAL_UNDEFINED__';
var _RewireAPI__ = {};

(function () {
	function addPropertyToAPIObject(name, value) {
		Object.defineProperty(_RewireAPI__, name, {
			value: value,
			enumerable: false,
			configurable: true
		});
	}

	addPropertyToAPIObject('__get__', _get__);
	addPropertyToAPIObject('__GetDependency__', _get__);
	addPropertyToAPIObject('__Rewire__', _set__);
	addPropertyToAPIObject('__set__', _set__);
	addPropertyToAPIObject('__reset__', _reset__);
	addPropertyToAPIObject('__ResetDependency__', _reset__);
	addPropertyToAPIObject('__with__', _with__);
})();

function _get__(variableName) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _get_original__(variableName);
	} else {
		var value = rewireData[variableName];

		if (value === INTENTIONAL_UNDEFINED) {
			return undefined;
		} else {
			return value;
		}
	}
}

function _get_original__(variableName) {
	switch (variableName) {
		case 'chai':
			return _chai2.default;

		case 'expect':
			return expect;

		case 'transformations':
			return _config2.default;
	}

	return undefined;
}

function _assign__(variableName, value) {
	var rewireData = _getRewiredData__();

	if (rewireData[variableName] === undefined) {
		return _set_original__(variableName, value);
	} else {
		return rewireData[variableName] = value;
	}
}

function _set_original__(variableName, _value) {
	switch (variableName) {}

	return undefined;
}

function _update_operation__(operation, variableName, prefix) {
	var oldValue = _get__(variableName);

	var newValue = operation === '++' ? oldValue + 1 : oldValue - 1;

	_assign__(variableName, newValue);

	return prefix ? newValue : oldValue;
}

function _set__(variableName, value) {
	var rewireData = _getRewiredData__();

	if ((typeof variableName === 'undefined' ? 'undefined' : _typeof(variableName)) === 'object') {
		Object.keys(variableName).forEach(function (name) {
			rewireData[name] = variableName[name];
		});
	} else {
		if (value === undefined) {
			rewireData[variableName] = INTENTIONAL_UNDEFINED;
		} else {
			rewireData[variableName] = value;
		}

		return function () {
			_reset__(variableName);
		};
	}
}

function _reset__(variableName) {
	var rewireData = _getRewiredData__();

	delete rewireData[variableName];

	if (Object.keys(rewireData).length == 0) {
		delete _getRewireRegistry__()[_getRewireModuleId__];
	}

	;
}

function _with__(object) {
	var rewireData = _getRewiredData__();

	var rewiredVariableNames = Object.keys(object);
	var previousValues = {};

	function reset() {
		rewiredVariableNames.forEach(function (variableName) {
			rewireData[variableName] = previousValues[variableName];
		});
	}

	return function (callback) {
		rewiredVariableNames.forEach(function (variableName) {
			previousValues[variableName] = rewireData[variableName];
			rewireData[variableName] = object[variableName];
		});
		var result = callback();

		if (!!result && typeof result.then == 'function') {
			result.then(reset).catch(reset);
		} else {
			reset();
		}

		return result;
	};
}

exports.__get__ = _get__;
exports.__GetDependency__ = _get__;
exports.__Rewire__ = _set__;
exports.__set__ = _set__;
exports.__ResetDependency__ = _reset__;
exports.__RewireAPI__ = _RewireAPI__;
exports.default = _RewireAPI__;

/***/ })
/******/ ]);
//# sourceMappingURL=test.bundle.js.map