require("source-map-support").install();
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(4);
module.exports = __webpack_require__(5);


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var fs = __webpack_require__(0);
var _ = __webpack_require__(1);
var path = __webpack_require__(2);
var JSONStream = __webpack_require__(6);
var Cleaner = __webpack_require__(7).default;

var cleaner = new Cleaner('cl');
var processed = 0;
var dir = './data/';
var commandOptions = [{ name: "src", type: _.toString }, { name: "out", type: _.toString }];
var options = __webpack_require__(11)(commandOptions);

var stream = fs.createReadStream(dir + options.src, { encoding: 'utf8' });
var parser = JSONStream.parse('*');
var addressStream = JSONStream.stringify();
var outAddressStream = fs.createWriteStream(dir + options.out);

stream.pipe(parser);
addressStream.pipe(outAddressStream);

parser.on('data', function (data) {
	saveCleanAddress(data);
});

parser.on('end', function () {
	addressStream.end();
});

function saveCleanAddress(o) {
	cleaner.clean(o.address).then(function (cleanAddress) {
		o.clean_address = cleanAddress;
		addressStream.write(o);
		processed++;
		printAddresses(o.address, o.clean_address);
		printProgress();
	}).catch(function (originalAddress) {
		processed++;
		printProgress();
	});
}

function printAddresses(original, cleaned) {
	console.log("\noriginal: %s", original);
	console.log("cleaned: %s\n", cleaned);
}

function printProgress() {
	console.log('Addresses processed: %d', processed);
}

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("JSONStream");

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(__dirname) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = __webpack_require__(0);

var _fs2 = _interopRequireDefault(_fs);

var _path = __webpack_require__(2);

var _path2 = _interopRequireDefault(_path);

var _lodash = __webpack_require__(1);

var _lodash2 = _interopRequireDefault(_lodash);

var _i18nIsoCountries = __webpack_require__(8);

var _i18nIsoCountries2 = _interopRequireDefault(_i18nIsoCountries);

var _config = __webpack_require__(9);

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CleanerError = function (_Error) {
    _inherits(CleanerError, _Error);

    function CleanerError() {
        _classCallCheck(this, CleanerError);

        return _possibleConstructorReturn(this, (CleanerError.__proto__ || Object.getPrototypeOf(CleanerError)).apply(this, arguments));
    }

    return CleanerError;
}(Error);

var CleanerInvalidParameter = function (_Error2) {
    _inherits(CleanerInvalidParameter, _Error2);

    function CleanerInvalidParameter() {
        _classCallCheck(this, CleanerInvalidParameter);

        return _possibleConstructorReturn(this, (CleanerInvalidParameter.__proto__ || Object.getPrototypeOf(CleanerInvalidParameter)).apply(this, arguments));
    }

    return CleanerInvalidParameter;
}(Error);

var CleanerMissingValue = function (_Error3) {
    _inherits(CleanerMissingValue, _Error3);

    function CleanerMissingValue() {
        _classCallCheck(this, CleanerMissingValue);

        return _possibleConstructorReturn(this, (CleanerMissingValue.__proto__ || Object.getPrototypeOf(CleanerMissingValue)).apply(this, arguments));
    }

    return CleanerMissingValue;
}(Error);

var RESOURCES_DIR = 'resources';
var STOPWORDS_FILENAME = 'stopwords.txt';
var AVAILABLE_COUNTRIES = ['cl'];

var Cleaner = function () {
    function Cleaner(countryCode) {
        var _this4 = this;

        var optionalPath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        _classCallCheck(this, Cleaner);

        var cp = void 0;
        var cn = void 0;

        this.$promise = new Promise(function (resolve, reject) {
            cp = resolve;
            cn = reject;
        });

        try {
            // Validate countryCode and optionalPath

            validateCountryCode(countryCode);
            validateOptionalPath(optionalPath);

            countryCode = countryCode.toLowerCase();

            // Check if config was previosly loaded

            if (this.constructor.configs.hasOwnProperty(countryCode) && !_lodash2.default.isEmpty(this.constructor.configs[countryCode])) {

                _lodash2.default.merge(this, this.constructor.configs[countryCode]);

                // Set transformations
                this.overrideDefaultConfiguration = !!this.overrideDefaultConfiguration;

                if (!this.overrideDefaultConfiguration) {
                    this.transformations = _lodash2.default.clone(_config2.default);
                } else if (this.overrideDefaultConfiguration && _lodash2.default.isEmpty(this.transformations)) {
                    throw new CleanerMissingValue('transformations array is missing ' + 'from local configuration');
                }

                // Set stopwords

                if (_lodash2.default.isEmpty(this.stopwords)) {
                    var basePath = optionalPath ? optionalPath : '.';
                    var filePath = basePath + '/' + countryCode + '/' + RESOURCES_DIR + '/' + STOPWORDS_FILENAME;
                    _fs2.default.readFile(_path2.default.join(__dirname, filePath), { encoding: 'utf8' }, function (err, content) {
                        if (err) {
                            throw new CleanerError(err);
                        }

                        _this4.stopwords = content.toString().split('\n');
                        _this4.constructor.configs[countryCode].stopwords = _lodash2.default.clone(_this4.stopwords);
                        cp(_this4);
                    });
                } else {
                    cp(this);
                }
            } else {
                throw new CleanerError('configuration module for country [' + countryCode + '] not found');
            }
        } catch (e) {
            cn(e);
        }
    }

    _createClass(Cleaner, [{
        key: 'clean',
        value: function clean(address) {
            var _this5 = this;

            var newAddress = address;
            return this.$promise.then(function () {
                _this5.transformations.forEach(function (transformation) {
                    newAddress = transformation(newAddress, _this5);
                });
                return newAddress;
            }).catch(function () {
                return newAddress;
            });
        }
    }]);

    return Cleaner;
}();

Cleaner.globalTransformations = _config2.default;
Cleaner.configs = _config.localConfigs || {};
Cleaner.errors = {
    'UNKNOWN': CleanerError,
    'INVALID_PARAMETER': CleanerInvalidParameter,
    'MISSING_VALUE': CleanerMissingValue
};

exports.default = Cleaner;

///////////////////////////////////////////////////////
// Dynamic Validation
//////////////////////////////////////////////////////


function validateCountryCode(code) {
    if (code) {
        if (!(_i18nIsoCountries2.default.getAlpha2Codes().hasOwnProperty(code.toUpperCase()) && AVAILABLE_COUNTRIES.find(function (c) {
            return c === code.toLowerCase();
        }))) {
            throw new CleanerInvalidParameter('Country code must be a valid ISO 3166-1 alpha 2');
        }
    } else {
        throw new CleanerMissingValue('Country code is required');
    }
}

function validateOptionalPath(optionalPath) {
    if (optionalPath && (typeof optionalPath !== 'string' || !/^(\.{1,2}\/|\/)?((\w+\/)+)?\w+(\.js)?$/.test(optionalPath))) {
        throw new CleanerInvalidParameter('optional path must be a relative or absolute system path');
    }
}

// TODO: VALIDATE TYPE OF ALL CONFIGS ATTRIBUTES
/* WEBPACK VAR INJECTION */}.call(exports, "cleaner"))

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("i18n-iso-countries");

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.localConfigs = undefined;

var _config = __webpack_require__(10);

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
    Reglas generales del cleaner


	[ X ] eliminacion de caracteres inválidos

	[ X ] eliminacion de expresiones (todo lo que este en llaves o destacado)

	[ X ] limpieza de palabras (aplicar reglas)

	[ X ] separar por otros caracteres alternativos reconocidos como separadores

	[ X ] dividir palabras enlazadas por algoritmo alternativo

	[ X ] eliminar characteres duplicados

	[  ] agregar pais si no lo tiene

	[ X ] eliminar multiples characteres separadores (TRIM)

	[ X ] eliminar caracteres especiales restantes

	[ X ] eliminar stopwords

**/

exports.default = [deleteInvalidCharacters, deleteExpressions, applyCleaningRules, alternativeSplittingCharacters, alternativeSplitAlgorithm, deleteDuplicateCharacters, trim, deleteRemainingSpecialCharacters, removeStopWords];
var localConfigs = exports.localConfigs = {
	'cl': _config2.default
};

function deleteInvalidCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var replaceCharacter = !!instance.deleteOptions.delete ? '' : !!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ';

		var characters = address.split("");
		newAddress = characters.map(function (c) {
			if (!instance.validLetters.test(c) && !instance.validSpecialCharacters.test(c) && !/[0-9]/.test(c) && c !== instance.separatorCharacter) {
				return instance.specialCharactersReplacements.hasOwnProperty(c) ? instance.specialCharactersReplacements[c] : replaceCharacter || " ";
			} else {
				return c;
			}
		});
		newAddress = newAddress.join("");
	}
	return newAddress;
}

function deleteExpressions(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var replaceCharacter = !!instance.deleteOptions.delete ? '' : !!instance.deleteOptions.replace ? instance.deleteOptions.replacementCharacter : ' ';

		instance.expressionsOptions.rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule, replaceCharacter);
		});
	}

	return newAddress;
}

function applyCleaningRules(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		instance.cleaningRules.forEach(function (transform) {
			newAddress = transform(newAddress, instance);
		});
	}
	return newAddress;
}

function alternativeSplittingCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);

		var filterMatchs = function filterMatchs(match) {
			return !instance.alternativeSplittingCharacters.exceptions.find(function (w) {
				return w === match;
			});
		};

		matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;

		while (matchs && matchs.length > 0) {
			newAddress = newAddress.replace(instance.alternativeSplittingCharacters.rule, instance.alternativeSplittingCharacters.replace);
			matchs = newAddress.match(instance.alternativeSplittingCharacters.rule);
			matchs = Array.isArray(matchs) ? matchs.filter(filterMatchs) : null;
		}
	}

	return newAddress;
}

function alternativeSplitAlgorithm(address, instance) {
	var newAddress = instance.useSplitWords ? instance.algorithms.split(address, instance) : address;
	return newAddress;
}

function escapeForRegex(str) {
	var escapingList = '+_~*=?¿\\/%$#!|°¬()[]{}^;:.,<>';
	str = str.split('');
	str = str.map(function (char) {
		return escapingList.includes(char) ? '\\' + char : char;
	});

	return str.join('');
}

function deleteDuplicateCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0 && newAddress.match(instance.repeatOptions.rule)) {
		var special = /[^a-zA-Z0-9ÁÉÍÓÚÜÑÀÈÌÒÙáéíóúüñàèìòù\s]/i;
		var tokens = instance.algorithms.tokenize(newAddress, instance);
		var newTokens = tokens.map(function (token) {
			var newToken = token;
			var matchs = token.match(instance.repeatOptions.rule);
			var totalMatchs = Array.isArray(matchs) ? matchs.reduce(function (prev, curr) {
				return prev + curr.length;
			}, 0) : 0;
			var containSpecialCharacters = Array.isArray(matchs) ? matchs.reduce(function (prev, curr) {
				return !prev ? special.test(curr) : prev;
			}, false) : false;

			if (containSpecialCharacters || matchs && matchs.length === 1 && totalMatchs === token.length && token.length >= 5 || totalMatchs > 0 && totalMatchs < token.length && token.length >= 5) {
				matchs.forEach(function (match) {
					var exclude = match[0] === instance.separatorCharacter;
					var freq = 1;
					var replacement = "";

					if (!exclude) {
						for (var i = 0; i < instance.repeatOptions.excludeCharacters.length; i++) {
							var o = instance.repeatOptions.excludeCharacters[i];
							if (match[0].toLowerCase() === o.char) {
								freq = o.freq;
								break;
							}
						}

						for (var _i = 0; _i < freq; _i++) {
							replacement += match[0];
						}
						var regex = new RegExp(escapeForRegex(match), 'gi');
						newToken = newToken.replace(regex, replacement);
					}
				});
			}

			return newToken;
		});

		newAddress = newTokens.join(instance.separatorCharacter);
	}

	return newAddress;
}

/*function addCountryName(address, instance) {
	let newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		const isNumber = /^\d+$/;
		const haveCountry = new RegExp(instance.countryName, "gi");
		let tokens = newAddress
			.split(instance.separatorCharacter)
			.filter(w => w !== instance.separatorCharacter);

		let containOnlyNumbers = tokens
			.reduce((prev,curr,i, arr) => prev ? isNumber.test(arr[i]) : prev, true );

		if (!haveCountry.test(newAddress) && !containOnlyNumbers) {
			newAddress = newAddress.replace(/(\S)\s*\,?$/gi, '$1, ' + instance.countryName);
		}
	}
	return newAddress;
}*/

function trim(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var words = instance.algorithms.tokenize(address, instance);
		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function removeStopWords(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var words = instance.algorithms.tokenize(address, instance);
		words = words.filter(function (w) {
			return !instance.stopwords.find(function (x) {
				return instance.algorithms.latinize(x) === w;
			});
		});

		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

function deleteRemainingSpecialCharacters(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var re = /[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/;
		var words = instance.algorithms.tokenize(address, instance);
		words = words.filter(function (w) {
			return w.length > 1 && re.test(w) || w.length === 1 && (instance.validSingleLetters.test(w) || /^[0-9]$/.test(w));
		});

		newAddress = words.join(instance.separatorCharacter);
	}
	return newAddress;
}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});
/**
	reemplazo de abreviaturas (acronimos) // pais
	eliminacion de stopwords // pais

**/

exports.default = {
	countryCode: 'cl',
	allowOverrideConfiguration: false,
	countryName: "Chile",
	useSplitWords: true,
	separatorCharacter: ' ',
	alternativeSplittingCharacters: {
		rule: /([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])(\/|\\|\-|\_)+([a-zA-ZáéíúóÁÉÍÓÚñÑüÜ])/gi,
		replace: "$1 $3",
		exceptions: ['S/N', 's/n', 'n/a', 'N/A', 'S/n', 's/N', 'N/a', 'n/A']
	},
	validLetters: /[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]/i,
	validSingleLetters: /[a-zA-z]/,
	validSpecialCharacters: /(\'|\"|\*|\(|\)|\{|\}|\[|\]|\-|\\|\/|\.|\,|\:|\+|\°)/,
	specialCharactersReplacements: { '?': '', 'º': '°', 'ª': '°' },
	deleteOptions: {
		replace: true,
		replacementCharacter: ' ' // char
	},
	repeatOptions: {
		rule: /([^0-9\s])\1+/gi,
		excludeCharacters: [{ char: 'r', freq: 2 }, { char: 'l', freq: 2 }, { char: 'c', freq: 2 }, { char: 's', freq: 2 }, { char: 'g', freq: 2 }, { char: 'f', freq: 2 }, { char: 'n', freq: 2 }, { char: 'e', freq: 2 }, { char: 'm', freq: 2 }, { char: 't', freq: 2 }, { char: 'p', freq: 2 }]
	},
	expressionsOptions: {
		rules: [/\+?(\(?(56(\-|\s)?)?\)?)?(\(?0?9\)?(\-|\s)?[0-9]{8}|\(?0?22\)?(\-|\s)?[0-9]{7})/gi, // phoneNumber
		/((\(|\[|\{)|(\"|\'|\*|\+|\%|\#)\3*).*((\)|\]|\})|(\"|\'|\*|\+|\%|\#)\6*)/gi, // all text between brackets or "'+*#%
		/\(.+\,/gi, // everything between a bracket and a comma
		/T?[012]?[0-9]\:[0-9]{2}(\:[0-9]{2})?(\.[0-9]{0,6})?(Z|\s?(\+|\-)?[012][0-9]\:[0-9]{2})?/gi, // time
		/(([012]?\d|3[01])(\/|\-)[01]?\d(\/|\-)([12]\d{3}|\d{2})|([12]\d{3}|\d{2})(\/|\-)[01]?\d(\/|\-)([012]?\d|3[01]))/gi, // date
		/[^0-9](\-|\+)?(90\.0{5,}|[0-8]?[0-9]\.[0-9]{5,})/g, //latitude
		/(\-|\+)?(180\.0{5,}|(1[0-7][0-9]|[0-9]{1,2})\.[0-9]{5,})/g // longitude
		]
	},
	cleaningRules: [cleanAsteriks, cleanPads, cleanTwoPoints, cleanSlashs, cleanPoints, cleanHyphes, cleanCommas],
	algorithms: {
		split: splitWordsAlgorithm,
		tokenize: tokenizerAlgorithm,
		latinize: latinizeAlgorithm
	}
};

// CLEANING RULES

function cleanAsteriks(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [{ regex: /(n|N)\*(?=(\s+)?\d+)/gi, replacement: 'N° ' }, { regex: /\*/gi, replacement: instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanPads(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Agrega un espacio antes de #1234
		{ regex: /(\#\d)/gi, replacement: instance.separatorCharacter + '$1' },
		// Reemplaza # al principio de una letra por un espacio
		{ regex: /\#([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: instance.separatorCharacter + '$1' },
		// Reemplaza # al final de una letra por un espacio
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\#/gi, replacement: '$1' + instance.separatorCharacter },
		// Reemplaza # al final de un número por un espacio
		{ regex: /(\d)\#/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanTwoPoints(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Eliminar d:d expresiones
		{ regex: /\d+\:\d+/gi, replacement: instance.separatorCharacter },
		// Reemplaza A:21 por A-21
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter + '$2-' },
		// Reemplaza 21:B por 21-B
		{ regex: /(\s+|^)(\d+)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter + '$2-' },
		// Reemplaza A-:21 por A-21
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]\-)\:(?=\d+(\s+|$))/gi, replacement: instance.separatorCharacter + '$2' },
		// Reemplaza 21-:B por 21-B
		{ regex: /(\s+|^)(\d+\-)\:(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|$))/gi, replacement: instance.separatorCharacter + '$2' },
		// Reemplaza palabra: por palabra_ para evitar que se borre :
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\:/g, replacement: '$1_' },
		// Reemplaza : por un espacio si no es la continuacion de una palabra
		{ regex: /\:/gi, replacement: instance.separatorCharacter },
		// Reincorpora palabra_  como palabra:
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)\_/g, replacement: '$1:' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanSlashs(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// Reemplazar S/N con un guion en medio para que no sea eliminado
		{ regex: /(\s+|^)(S|s)(\s{0,3})\/(\s{0,3})(N|n)/gi, replacement: instance.separatorCharacter + '$2_$5' + instance.separatorCharacter },
		// Reemplazar N/A con un guin en medio para que no sea eliminado
		{ regex: /(\s+|^)(N|n)(\s{0,3})\/(\s{0,3})(A|a)/gi, replacement: instance.separatorCharacter + '$2_$5' + instance.separatorCharacter },
		// Remplazar d/d por d_d para que no sea eliminada
		{ regex: /(\d)\/(\d)/gi, replacement: '$1_$2' },
		// Reemplazar todos los slash con espacios
		{ regex: /(\\|\/)/gi, replacement: instance.separatorCharacter },
		// Restituir d_d a d/d
		{ regex: /(\d)\_(\d)/g, replacement: '$1/$2' },
		// Restituir S_N a S/N
		{ regex: /(\s+|^)(S|s)\_(N|n)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter },
		// Restituir N_A a N/A
		{ regex: /(\s+|^)(N|n)\_(A|a)/gi, replacement: instance.separatorCharacter + '$2/$3' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function cleanPoints(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {

		var rules = [
		// numeros con multiples puntos ej: 11.1.1.1.1.1
		{ regex: /\.*(\d+\.\d+|\d+\.){2,}\d+?\.*/gi, replacement: instance.separatorCharacter },
		// N.1000 -> N° 1000
		{ regex: /(\s+|^)N\.(\d+)/g, replacement: instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2' },
		// reemplazar . por - si esta en medio de una letra y un numero ej: A.2 -> A-2
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])\.(?=\d+(\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-' },
		// reemplazar . por - si esta al entre un numero y una letra ej: 14.B -> 14-B
		{ regex: /(\s+|^)(\d+)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü](\s+|(\s+)?$))/g, replacement: instance.separatorCharacter + '$2-' },
		// agregar espacio a la derecha de . si . esta en medio de una palabra y un numero
		{ regex: /([^\s\.\d]{2,}\.)(?=\d+)/g, replacement: '$1' + instance.separatorCharacter },
		// palabras con . al principio
		{ regex: /(\s+|^)\.(?=[^\s\.\d]+)/g, replacement: instance.separatorCharacter },
		// numeros con . al principio
		{ regex: /(\s+|^)\.(?=\d+)/g, replacement: instance.separatorCharacter },
		// unir numeros con coma si en medio a un punto
		{ regex: /(\d+)\.\,/g, replacement: '$1,' + instance.separatorCharacter },
		// numeros con . al final
		{ regex: /(\d+)\.(?=(\s+|(\s+)?$))/g, replacement: '$1' + instance.separatorCharacter },
		// separar av.|avda.|adva.|avnda.|adva.de palabras
		{ regex: /(\s+|^)(av|avda|adva|avnda|adva)\.(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]+)/gi, replacement: instance.separatorCharacter + '$2.' + instance.separatorCharacter }, { regex: /([a-zA-Z]{2}[a-zA-Z]+)\.+([a-zA-Z]{2}[a-zA-Z]+)/gi, replacement: '$1.' + instance.separatorCharacter + '$2' }, { regex: /([a-zA-Z]+)\.+\,/gi, replacement: '$1.' + instance.separatorCharacter + ',' }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanHyphes(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// L-10 -> L. 10
		{ regex: /(\s+|^)L\-(?=\d+)/g, replacement: instance.separatorCharacter + 'L.' + instance.separatorCharacter },
		// unir letra con numero que tienen un guion en medio separado por espacios
		{ regex: /(\s+|^)([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ])\s{1,2}\-\s{1,2}(\d+)/g, replacement: instance.separatorCharacter + '$2-$3' + instance.separatorCharacter },
		// separar palabras de más de 1 caracter unidas por un - a un numero
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})\-(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter },
		// separar numeros unidos por un guion a palabras de más de 1 caracter
		{ regex: /(\d+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]{2,})/gi, replacement: '$1' + instance.separatorCharacter },
		// separar palabras unidas por un -
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// reemplazar - al principio de una palabra o numero por un espacio
		{ regex: /(\s+|^)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter },
		// reemplazar - al final de una palabra o numero por un espacio
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\-(?=(\s+|(\s+)?$))/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			newAddress = newAddress.replace(rule.regex, rule.replacement);
		});
	}

	return newAddress;
}

function cleanCommas(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// remover espacio sobrante entre un caracter alfanumerico y una coma
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\s+\,/g, replacement: '$1,' + instance.separatorCharacter },
		// reemplazar multiples comas separadas por espacios por solo una a la derecha del ultimo caracter alfanumerico
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,?(\s+\,)+(\s+)?/g, replacement: '$1,' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre un numero y una letra
		{ regex: /(\d+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre una letra y un numero
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,(?=\d+)/gi, replacement: '$1' + instance.separatorCharacter },
		// agregar un espacio a la derecha de la coma cuando se encuentra entre 2 letras
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+\,)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: '$1' + instance.separatorCharacter },
		// eliminar coma al inicio de una palabra o numero
		{ regex: /(\s+|^)\,(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)/gi, replacement: instance.separatorCharacter },
		// eliminar dos comas consecutivas al final de una palabra
		{ regex: /([a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÜÑáéíóúàèìòùüñ]+)\,{2,}/gi, replacement: '$1,' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

// ALGORITHMS

function splitWordsAlgorithm(address, instance) {
	var newAddress = address;
	if (typeof address === "string" && address.length > 0) {
		var rules = [
		// separa N°1000 -> N° 1000
		{ regex: /(.+|^)N\°(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'N°' + instance.separatorCharacter + '$2' },
		// separa KM68.5 -> KM 68.5
		{ regex: /(.+|^)KM(\d+)/g, replacement: '$1' + instance.separatorCharacter + 'KM' + instance.separatorCharacter + '$2' },
		// separa of-401
		{ regex: /((of|OF|Of)\.?)\-(?=[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü])/gi, replacement: '$1' + instance.separatorCharacter },
		// si una minuscula se encuentra con una mayuscula las separa
		{ regex: /([a-záéíúóüàèìòùñ])(?=[A-ZÁÉÍÓÚÜÀÈÌÒÙÑ])/g, replacement: '$1' + instance.separatorCharacter },

		// si dos palabras de al menos 2 characteres se encuentran divididas por -, _, \ o / los separa
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})(\-|\_|\/|\\)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{2,})/gi, replacement: '$1' + instance.separatorCharacter },

		// si una palabra se encuentra con un numero y es superior a 2 characteres los separa
		{ regex: /([a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})(\-|\_)?(?=[0-9])/gi, replacement: '$1' + instance.separatorCharacter },

		// si un numero se encuentra con una palabra superior a 2 caracteres los separa
		{ regex: /([0-9])(\-|\_)?(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü]{3,})/gi, replacement: '$1' + instance.separatorCharacter },

		// separar N°BCG y NH°123
		{ regex: /(N.?\°)(?=[a-zA-ZÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìùñü0-9])/gi, replacement: '$1' + instance.separatorCharacter }];

		rules.forEach(function (rule) {
			while (newAddress.search(rule.regex) !== -1) {
				newAddress = newAddress.replace(rule.regex, rule.replacement);
			}
		});
	}

	return newAddress;
}

function tokenizerAlgorithm(address, instance) {
	var tokens = [];
	if (typeof address === "string" && address.length > 0) {
		var re = /[a-zA-Z0-9ÁÉÍÓÚÀÈÌÒÙÑÜáéíóúàèìòùñü]/;
		tokens = address.split(instance.separatorCharacter);
		tokens = tokens.filter(function (token) {
			return token.length > 1 && re.test(token) || token.length === 1 && (instance.validSingleLetters.test(token) || /^[0-9]$/.test(token));
		});

		tokens = tokens.map(function (w, i, arr) {
			return arr[i].trim();
		});
	}
	return tokens;
}

function latinizeAlgorithm(token) {
	var tokenLatinized = token;
	if (typeof token === "string" && token.length > 0) {
		var isUpper = /[A-ZÁÉÍÓÚÀÈÌÒÙÜÑ]/;
		var replacementsMap = {
			'á': 'a',
			'é': 'e',
			'í': 'i',
			'ó': 'o',
			'ú': 'u',
			'à': 'a',
			'è': 'e',
			'ì': 'i',
			'ò': 'o',
			'ù': 'u',
			'ü': 'u'
		};

		var chars = tokenLatinized.split("");
		chars = chars.map(function (c) {
			var upper = isUpper.test(c);
			c = c.toLowerCase();
			c = replacementsMap[c] ? replacementsMap[c] : c;
			return upper ? c.toUpperCase() : c;
		});
		tokenLatinized = chars.join("");
	}
	return tokenLatinized;
}

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("command-line-args");

/***/ })
/******/ ]);