var webpack = require('webpack');
var path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = [{
	/* https://babeljs.io/docs/usage/polyfill */
	entry: ['babel-polyfill', './clean_json_addresses.js'],
	target: 'node',
	/* https://github.com/webpack-contrib/css-loader/issues/447 */
	node: {
		fs: "empty",
		__dirname: true,
		__filename: true,
	},
	output: {
		path: path.resolve(__dirname, 'cleaner_builds'),
		filename: 'clean_json_addresses.bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				options: {
					cacheDirectory: false,
					presets: ['es2015', 'stage-2'],
				}
			}
		]
	},
	plugins: [
		new webpack.BannerPlugin({ banner: 'require("source-map-support").install();',
                              raw: true, entryOnly: false })
	],
	externals: [nodeExternals()],
	stats: {
		colors: true
	}
},{
  /* https://babeljs.io/docs/usage/polyfill */
  entry: ['babel-polyfill', './clean_txt_addresses.js'],
  target: 'node',
  /* https://github.com/webpack-contrib/css-loader/issues/447 */
  node: {
    fs: "empty",
    __dirname: true,
    __filename: true,
  },
  output: {
    path: path.resolve(__dirname, 'cleaner_builds'),
    filename: 'clean_txt_addresses.bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          cacheDirectory: false,
          presets: ['es2015', 'stage-2'],
        }
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin({ banner: 'require("source-map-support").install();',
      raw: true, entryOnly: false })
  ],
  externals: [nodeExternals()],
  stats: {
    colors: true
  }
}];
